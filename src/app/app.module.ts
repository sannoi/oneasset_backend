import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { NgxUiLoaderModule, NgxUiLoaderConfig, SPINNER, } from 'ngx-ui-loader';
import { FlatpickrModule } from 'angularx-flatpickr';

import { TokenInterceptorService } from './services/token-interceptor.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guard/auth.guard';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { NewsComponent } from './components/news/news.component';
import { FundsComponent } from './components/funds/funds.component';

import { UserLoginService } from './services/user-login.service';
import { NewsService } from './services/news.service';
import { AddNewsComponent } from './components/add-news/add-news.component';


import { NgxSummernoteModule } from 'ngx-summernote';
import { EditNewsComponent } from './components/edit-news/edit-news.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';

import { PagerService } from './_services/index';
import { AnnounceComponent } from './components/announce/announce.component';
import { AddAnnounceComponent } from './components/add-announce/add-announce.component';
import { EditAnnounceComponent } from './components/edit-announce/edit-announce.component';
import { EarthupdateComponent } from './components/earthupdate/earthupdate.component';
import { AddEarthupdateComponent } from './components/add-earthupdate/add-earthupdate.component';
import { EditEarthupdateComponent } from './components/edit-earthupdate/edit-earthupdate.component';
import { PromotionComponent } from './components/promotion/promotion.component';
import { AddPromotionComponent } from './components/add-promotion/add-promotion.component';
import { EditPromotionComponent } from './components/edit-promotion/edit-promotion.component';
import { FundrecommendComponent } from './components/fundrecommend/fundrecommend.component';
import { AddFundrecommComponent } from './components/add-fundrecomm/add-fundrecomm.component';
import { EditFundrecommComponent } from './components/edit-fundrecomm/edit-fundrecomm.component';
import { BenefitComponent } from './components/benefit/benefit.component';
import { AddBenefitComponent } from './components/add-benefit/add-benefit.component';
import { EditBenefitComponent } from './components/edit-benefit/edit-benefit.component';
import { AddMarketComponent } from './components/add-market/add-market.component';
import { EditMarketComponent } from './components/edit-market/edit-market.component';
import { MarketComponent } from './components/market/market.component';
import { GoodGovernenceComponent } from './components/good-governence/good-governence.component';
import { EditGoodGovernenceComponent } from './components/edit-good-governence/edit-good-governence.component';
import { AddGoodGovernenceComponent } from './components/add-good-governence/add-good-governence.component';
import { EthicComponent } from './components/ethic/ethic.component';
import { EditEthicComponent } from './components/edit-ethic/edit-ethic.component';
import { AddEthicComponent } from './components/add-ethic/add-ethic.component';
import { CorruptionComponent } from './components/corruption/corruption.component';
import { EditCorruptionComponent } from './components/edit-corruption/edit-corruption.component';
import { AddCorruptionComponent } from './components/add-corruption/add-corruption.component';
import { ProductComponent } from './components/product/product.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { ManagerListComponent } from './components/manager-list/manager-list.component';
import { EditManagerListComponent } from './components/edit-manager-list/edit-manager-list.component';
import { AddManagerListComponent } from './components/add-manager-list/add-manager-list.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { EditTransactionComponent } from './components/edit-transaction/edit-transaction.component';
import { AddTransactionComponent } from './components/add-transaction/add-transaction.component';
import { GuidelineComponent } from './components/guideline/guideline.component';
import { EditGuidelineComponent } from './components/edit-guideline/edit-guideline.component';
import { AddGuidelineComponent } from './components/add-guideline/add-guideline.component';
import { VoteComponent } from './components/vote/vote.component';
import { EditVoteComponent } from './components/edit-vote/edit-vote.component';
import { AddVoteComponent } from './components/add-vote/add-vote.component';
import { HoldingInvestmentUnitComponent } from './components/holding-investment-unit/holding-investment-unit.component';
import { EditHoldingInvestmentUnitComponent } from './components/edit-holding-investment-unit/edit-holding-investment-unit.component';
import { AddHoldingInvestmentUnitComponent } from './components/add-holding-investment-unit/add-holding-investment-unit.component';
import { FundRecomMappComponent } from './components/fund-recom-mapp/fund-recom-mapp.component';
import { FundRecomMappAddComponent } from './components/fund-recom-mapp-add/fund-recom-mapp-add.component';
import { FundRiskComponent } from './components/fund-risk/fund-risk.component';
import { FundRiskEditComponent } from './components/fund-risk-edit/fund-risk-edit.component';
import { FundDetailComponent } from './components/fund-detail/fund-detail.component';
import { EditFundDetailComponent } from './components/edit-fund-detail/edit-fund-detail.component';
import { NewsletterComponent } from './components/newsletter/newsletter.component';
import { VdoComponent } from './components/vdo/vdo.component';
import { NewsletterAddComponent } from './components/newsletter-add/newsletter-add.component';
import { NewsletterEditComponent } from './components/newsletter-edit/newsletter-edit.component';
import { VdoAddComponent } from './components/vdo-add/vdo-add.component';
import { VdoEditComponent } from './components/vdo-edit/vdo-edit.component';
import { DocumentTypeComponent } from './components/document-type/document-type.component';
import { AddDocumentTypeComponent } from './components/add-document-type/add-document-type.component';
import { EditDocumentTypeComponent } from './components/edit-document-type/edit-document-type.component';
import { DocumentComponent } from './components/document/document.component';
import { AddDocumentComponent } from './components/add-document/add-document.component';
import { EditDocumentComponent } from './components/edit-document/edit-document.component';
import { MaintainComponent } from './components/maintain/maintain.component';
import { AddInflationComponent } from './components/add-inflation/add-inflation.component';
import { EditInflationComponent } from './components/edit-inflation/edit-inflation.component';
import { AddProvidentComponent } from './components/add-provident/add-provident.component';
import { EditProvidentComponent } from './components/edit-provident/edit-provident.component';
import { AddExpectedReturnComponent } from './components/add-expected-return/add-expected-return.component';
import { EditExpectedReturnComponent } from './components/edit-expected-return/edit-expected-return.component';
import { AddSdComponent } from './components/add-sd/add-sd.component';
import { EditSdComponent } from './components/edit-sd/edit-sd.component';
import { AddInvestmentAllocationComponent } from './components/add-investment-allocation/add-investment-allocation.component';
import { EditInvestmentAllocationComponent } from './components/edit-investment-allocation/edit-investment-allocation.component';
import { UserGroupComponent } from './components/user-group/user-group.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { AddUserGroupComponent } from './components/add-user-group/add-user-group.component';
import { EditUserGroupComponent } from './components/edit-user-group/edit-user-group.component';
import { AddUserListComponent } from './components/add-user-list/add-user-list.component';
import { EditUserListComponent } from './components/edit-user-list/edit-user-list.component';
import { MenuMappingComponent } from './components/menu-mapping/menu-mapping.component';
import { FundDocumentListComponent } from './components/fund-document-list/fund-document-list.component';
import { EditFundDocumentComponent } from './components/edit-fund-document/edit-fund-document.component';
import { AddFundDocumentComponent } from './components/add-fund-document/add-fund-document.component';
import { InvestmentAllocationComponent } from './components/investment-allocation/investment-allocation.component';
import { AddBannerComponent } from './components/add-banner/add-banner.component';
import { BannerListComponent } from './components/banner-list/banner-list.component';
import { AddBannerPrivateComponent } from './components/add-banner-private/add-banner-private.component';
import { AddBannerProvidentComponent } from './components/add-banner-provident/add-banner-provident.component';
import { BannerPrivateComponent } from './components/banner-private/banner-private.component';
import { BannerProvidentComponent } from './components/banner-provident/banner-provident.component';

import { BannerPrivateService } from 'src/app/services/banner-private.service';
import { BannerProvidentService } from 'src/app/services/banner-provident.service';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  fgsColor: '#FFAF3E',
  fgsPosition: 'center-center',
  fgsSize: 140,
  fgsType: SPINNER.fadingCircle,
  hasProgressBar: false,
  overlayColor: 'rgba(0,0,0,0.8)'
};

@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    DashboardComponent,
    HeaderComponent,
    SideBarComponent,
    NewsComponent,
    FundsComponent,
    AddNewsComponent,
    EditNewsComponent,
    ResetPasswordComponent,
    AnnounceComponent,
    AddAnnounceComponent,
    EditAnnounceComponent,
    EarthupdateComponent,
    AddEarthupdateComponent,
    EditEarthupdateComponent,
    PromotionComponent,
    AddPromotionComponent,
    EditPromotionComponent,
    FundrecommendComponent,
    AddFundrecommComponent,
    EditFundrecommComponent,
    BenefitComponent,
    AddBenefitComponent,
    EditBenefitComponent,
    AddMarketComponent,
    EditMarketComponent,
    MarketComponent,
    GoodGovernenceComponent,
    EditGoodGovernenceComponent,
    AddGoodGovernenceComponent,
    EthicComponent,
    EditEthicComponent,
    AddEthicComponent,
    CorruptionComponent,
    EditCorruptionComponent,
    AddCorruptionComponent,
    ProductComponent,
    EditProductComponent,
    AddProductComponent,
    ManagerListComponent,
    EditManagerListComponent,
    AddManagerListComponent,
    TransactionComponent,
    EditTransactionComponent,
    AddTransactionComponent,
    GuidelineComponent,
    EditGuidelineComponent,
    AddGuidelineComponent,
    VoteComponent,
    EditVoteComponent,
    AddVoteComponent,
    HoldingInvestmentUnitComponent,
    EditHoldingInvestmentUnitComponent,
    AddHoldingInvestmentUnitComponent,
    FundRecomMappComponent,
    FundRecomMappAddComponent,
    FundRiskComponent,
    FundRiskEditComponent,
    FundDetailComponent,
    EditFundDetailComponent,
    NewsletterComponent,
    VdoComponent,
    NewsletterAddComponent,
    NewsletterEditComponent,
    VdoAddComponent,
    VdoEditComponent,
    DocumentTypeComponent,
    AddDocumentTypeComponent,
    EditDocumentTypeComponent,
    DocumentComponent,
    AddDocumentComponent,
    EditDocumentComponent,
    MaintainComponent,
    AddInflationComponent,
    EditInflationComponent,
    AddProvidentComponent,
    EditProvidentComponent,
    AddExpectedReturnComponent,
    EditExpectedReturnComponent,
    AddSdComponent,
    EditSdComponent,
    AddInvestmentAllocationComponent,
    EditInvestmentAllocationComponent,
    UserGroupComponent,
    UserListComponent,
    AddUserGroupComponent,
    EditUserGroupComponent,
    AddUserListComponent,
    EditUserListComponent,
    MenuMappingComponent,
    FundDocumentListComponent,
    EditFundDocumentComponent,
    AddFundDocumentComponent,
    InvestmentAllocationComponent,
    AddBannerComponent,
    BannerListComponent,
    AddBannerPrivateComponent,
    AddBannerProvidentComponent,
    BannerPrivateComponent,
    BannerProvidentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxSummernoteModule,
    ReactiveFormsModule,
    MyDatePickerModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    FlatpickrModule.forRoot()
  ],
  providers: [AuthGuard, AuthService, UserLoginService, NewsService, PagerService,
    BannerPrivateService, BannerProvidentService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
