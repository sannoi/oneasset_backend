import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogInComponent } from './components/log-in/log-in.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NewsComponent } from './components/news/news.component';
import { FundsComponent } from './components/funds/funds.component';
import { AddNewsComponent } from './components/add-news/add-news.component';
import { EditNewsComponent } from './components/edit-news/edit-news.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';

import { AnnounceComponent } from './components/announce/announce.component';
import { AddAnnounceComponent } from './components/add-announce/add-announce.component';
import { EditAnnounceComponent } from './components/edit-announce/edit-announce.component';


import { EarthupdateComponent } from './components/earthupdate/earthupdate.component';
import { AddEarthupdateComponent } from './components/add-earthupdate/add-earthupdate.component';
import { EditEarthupdateComponent } from './components/edit-earthupdate/edit-earthupdate.component';

import { PromotionComponent } from './components/promotion/promotion.component';
import { AddPromotionComponent } from './components/add-promotion/add-promotion.component';
import { EditPromotionComponent } from './components/edit-promotion/edit-promotion.component';

import { FundrecommendComponent } from './components/fundrecommend/fundrecommend.component';
import { AddFundrecommComponent } from './components/add-fundrecomm/add-fundrecomm.component';
import { EditFundrecommComponent } from './components/edit-fundrecomm/edit-fundrecomm.component';

import { BenefitComponent } from './components/benefit/benefit.component';
import { AddBenefitComponent } from './components/add-benefit/add-benefit.component';
import { EditBenefitComponent } from './components/edit-benefit/edit-benefit.component';

import { AddMarketComponent } from './components/add-market/add-market.component';
import { EditMarketComponent } from './components/edit-market/edit-market.component';
import { MarketComponent } from './components/market/market.component';

import { GoodGovernenceComponent } from './components/good-governence/good-governence.component';
import { AddGoodGovernenceComponent } from './components/add-good-governence/add-good-governence.component';
import { EditGoodGovernenceComponent } from './components/edit-good-governence/edit-good-governence.component';

import { EthicComponent } from './components/ethic/ethic.component';
import { EditEthicComponent } from './components/edit-ethic/edit-ethic.component';
import { AddEthicComponent } from './components/add-ethic/add-ethic.component';

import { CorruptionComponent } from './components/corruption/corruption.component';
import { AddCorruptionComponent } from './components/add-corruption/add-corruption.component';
import { EditCorruptionComponent } from './components/edit-corruption/edit-corruption.component';

import { ProductComponent } from './components/product/product.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';

import { ManagerListComponent } from './components/manager-list/manager-list.component';
import { AddManagerListComponent } from './components/add-manager-list/add-manager-list.component';
import { EditManagerListComponent } from './components/edit-manager-list/edit-manager-list.component';

import { TransactionComponent } from './components/transaction/transaction.component';
import { AddTransactionComponent } from './components/add-transaction/add-transaction.component';
import { EditTransactionComponent } from './components/edit-transaction/edit-transaction.component';

import { GuidelineComponent } from './components/guideline/guideline.component';
import { AddGuidelineComponent } from './components/add-guideline/add-guideline.component';
import { EditGuidelineComponent } from './components/edit-guideline/edit-guideline.component';

import { VoteComponent } from './components/vote/vote.component';
import { AddVoteComponent } from './components/add-vote/add-vote.component';
import { EditVoteComponent } from './components/edit-vote/edit-vote.component';

import { HoldingInvestmentUnitComponent } from './components/holding-investment-unit/holding-investment-unit.component';
import { AddHoldingInvestmentUnitComponent } from './components/add-holding-investment-unit/add-holding-investment-unit.component';
import { EditHoldingInvestmentUnitComponent } from './components/edit-holding-investment-unit/edit-holding-investment-unit.component';

import { FundRecomMappComponent } from './components/fund-recom-mapp/fund-recom-mapp.component';
import { FundRecomMappAddComponent } from './components/fund-recom-mapp-add/fund-recom-mapp-add.component';

import { FundRiskComponent } from './components/fund-risk/fund-risk.component';
import { FundRiskEditComponent } from './components/fund-risk-edit/fund-risk-edit.component';
import { FundDetailComponent } from './components/fund-detail/fund-detail.component';
import { EditFundDetailComponent } from './components/edit-fund-detail/edit-fund-detail.component';

import { NewsletterComponent } from './components/newsletter/newsletter.component';
import { NewsletterAddComponent } from './components/newsletter-add/newsletter-add.component';
import { NewsletterEditComponent } from './components/newsletter-edit/newsletter-edit.component';

import { VdoComponent } from './components/vdo/vdo.component';
import { VdoAddComponent } from './components/vdo-add/vdo-add.component';
import { VdoEditComponent } from './components/vdo-edit/vdo-edit.component';
import { DocumentTypeComponent } from './components/document-type/document-type.component';
import { AddDocumentTypeComponent } from './components/add-document-type/add-document-type.component';
import { EditDocumentTypeComponent } from './components/edit-document-type/edit-document-type.component';
import { DocumentComponent } from './components/document/document.component';
import { AddDocumentComponent } from './components/add-document/add-document.component';
import { EditDocumentComponent } from './components/edit-document/edit-document.component';
import { MaintainComponent } from './components/maintain/maintain.component';
import { AddInflationComponent } from './components/add-inflation/add-inflation.component';
import { EditInflationComponent } from './components/edit-inflation/edit-inflation.component';
import { AddProvidentComponent } from './components/add-provident/add-provident.component';
import { EditProvidentComponent } from './components/edit-provident/edit-provident.component';
import { AddExpectedReturnComponent } from './components/add-expected-return/add-expected-return.component';
import { EditExpectedReturnComponent } from './components/edit-expected-return/edit-expected-return.component';
import { AddSdComponent } from './components/add-sd/add-sd.component';
import { EditSdComponent } from './components/edit-sd/edit-sd.component';
import { EditInvestmentAllocationComponent } from './components/edit-investment-allocation/edit-investment-allocation.component';
import { AddInvestmentAllocationComponent } from './components/add-investment-allocation/add-investment-allocation.component';
import { UserGroupComponent } from './components/user-group/user-group.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { AddUserGroupComponent } from './components/add-user-group/add-user-group.component';
import { EditUserGroupComponent } from './components/edit-user-group/edit-user-group.component';
import { AddUserListComponent } from './components/add-user-list/add-user-list.component';
import { EditUserListComponent } from './components/edit-user-list/edit-user-list.component';
import { MenuMappingComponent } from './components/menu-mapping/menu-mapping.component';
import { FundDocumentListComponent } from './components/fund-document-list/fund-document-list.component';
import { EditFundDocumentComponent } from './components/edit-fund-document/edit-fund-document.component';
import { AddFundDocumentComponent } from './components/add-fund-document/add-fund-document.component';
import { InvestmentAllocationComponent } from './components/investment-allocation/investment-allocation.component';
import { BannerListComponent } from './components/banner-list/banner-list.component';
import { AddBannerComponent } from './components/add-banner/add-banner.component';

import { AddBannerPrivateComponent } from './components/add-banner-private/add-banner-private.component';
import { AddBannerProvidentComponent } from './components/add-banner-provident/add-banner-provident.component';
import { BannerPrivateComponent } from './components/banner-private/banner-private.component';
import { BannerProvidentComponent } from './components/banner-provident/banner-provident.component';

import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'back-office/login', pathMatch: 'full' },
  { path: 'back-office/login', component: LogInComponent },
  { path: 'back-office/reset-password', component: ResetPasswordComponent,  canActivate: [AuthGuard] },
  {
    path: 'back-office',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'banner', component: BannerListComponent },
      { path: 'banner/add', component: AddBannerComponent },

      { path: 'banner-private', component: BannerPrivateComponent },
      { path: 'banner-private/add', component: AddBannerPrivateComponent },

      { path: 'banner-provident', component: BannerProvidentComponent },
      { path: 'banner-provident/add', component: AddBannerProvidentComponent },

      { path: 'news', component: NewsComponent },
      { path: 'news/add', component: AddNewsComponent },
      { path: 'news/edit', component: EditNewsComponent },

      { path: 'announce', component: AnnounceComponent },
      { path: 'announce/add', component: AddAnnounceComponent },
      { path: 'announce/edit', component: EditAnnounceComponent },

      { path: 'newsletter', component: NewsletterComponent },
      { path: 'newsletter/add', component: NewsletterAddComponent },
      { path: 'newsletter/edit', component: NewsletterEditComponent },

      { path: 'vdo', component: VdoComponent },
      { path: 'vdo/add', component: VdoAddComponent },
      { path: 'vdo/edit', component: VdoEditComponent },

      { path: 'earthupdate', component: EarthupdateComponent },
      { path: 'earthupdate/add', component: AddEarthupdateComponent },
      { path: 'earthupdate/edit', component: EditEarthupdateComponent },

      { path: 'promotion', component: PromotionComponent },
      { path: 'promotion/add', component: AddPromotionComponent },
      { path: 'promotion/edit', component: EditPromotionComponent },

      { path: 'fundrecommend', component: FundrecommendComponent },
      { path: 'fundrecommend/add', component: AddFundrecommComponent },
      { path: 'fundrecommend/edit', component: EditFundrecommComponent },

      { path: 'benefit', component: BenefitComponent },
      { path: 'benefit/add', component: AddBenefitComponent },
      { path: 'benefit/edit', component: EditBenefitComponent },

      { path: 'market', component: MarketComponent },
      { path: 'market/add', component: AddMarketComponent },
      { path: 'market/edit', component: EditMarketComponent },

      { path: 'funds', component: FundsComponent },

      { path: 'governence', component: GoodGovernenceComponent },
      { path: 'governence/add', component: AddGoodGovernenceComponent },
      { path: 'governence/edit', component: EditGoodGovernenceComponent },

      { path: 'ethic', component: EthicComponent },
      { path: 'ethic/add', component: AddEthicComponent },
      { path: 'ethic/edit', component: EditEthicComponent },

      { path: 'corruption', component: CorruptionComponent },
      { path: 'corruption/add', component: AddCorruptionComponent },
      { path: 'corruption/edit', component: EditCorruptionComponent },

      { path: 'product', component: ProductComponent },
      { path: 'product/add', component: AddProductComponent },
      { path: 'product/edit', component: EditProductComponent },

      { path: 'manager-list', component: ManagerListComponent },
      { path: 'manager-list/add', component: AddManagerListComponent },
      { path: 'manager-list/edit', component: EditManagerListComponent },

      { path: 'transaction', component: TransactionComponent },
      { path: 'transaction/add', component: AddTransactionComponent },
      { path: 'transaction/edit', component: EditTransactionComponent },

      { path: 'guideline', component: GuidelineComponent },
      { path: 'guideline/add', component: AddGuidelineComponent },
      { path: 'guideline/edit', component: EditGuidelineComponent },

      { path: 'vote-report', component: VoteComponent },
      { path: 'vote-report/add', component: AddVoteComponent },
      { path: 'vote-report/edit', component: EditVoteComponent },

      { path: 'holding-investment-unit', component: HoldingInvestmentUnitComponent },
      { path: 'holding-investment-unit/add', component: AddHoldingInvestmentUnitComponent },
      { path: 'holding-investment-unit/edit', component: EditHoldingInvestmentUnitComponent },

      { path: 'fundRecom_mapp', component: FundRecomMappComponent },
      { path: 'fundRecom_map/add', component: FundRecomMappAddComponent },

      { path: 'fund-risk', component: FundRiskComponent },
      { path: 'fund-risk/edit', component: FundRiskEditComponent },

      { path: 'fund-detail', component: FundDetailComponent },
      { path: 'fund-detail/edit', component: EditFundDetailComponent },

      { path: 'document-type', component: DocumentTypeComponent },
      { path: 'document-type/add', component: AddDocumentTypeComponent },
      { path: 'document-type/edit', component: EditDocumentTypeComponent },

      { path: 'document', component: DocumentComponent },
      { path: 'document/add', component: AddDocumentComponent },
      { path: 'document/edit', component: EditDocumentComponent },

      { path: 'maintain', component: MaintainComponent },
      { path: 'investment-allocation', component: InvestmentAllocationComponent },

      { path: 'maintain/add-inflation', component: AddInflationComponent },
      { path: 'maintain/edit-inflation', component: EditInflationComponent },

      { path: 'maintain/add-provident', component: AddProvidentComponent },
      { path: 'maintain/edit-provident', component: EditProvidentComponent },

      { path: 'maintain/add-expected-return', component: AddExpectedReturnComponent },
      { path: 'maintain/edit-expected-return', component: EditExpectedReturnComponent },

      { path: 'maintain/add-sd', component: AddSdComponent },
      { path: 'maintain/edit-sd', component: EditSdComponent },

      { path: 'maintain/add-investment-allocation', component: AddInvestmentAllocationComponent },
      { path: 'maintain/edit-investment-allocation', component: EditInvestmentAllocationComponent },

      { path: 'user-group', component: UserGroupComponent },
      { path: 'user-group/add', component: AddUserGroupComponent },
      { path: 'user-group/edit', component: EditUserGroupComponent },
      { path: 'user-group/menu-mapping', component: MenuMappingComponent },

      { path: 'user-list', component: UserListComponent },
      { path: 'user-list/add', component: AddUserListComponent },
      { path: 'user-list/edit', component: EditUserListComponent },

      { path: 'fund-detail/document', component: FundDocumentListComponent },
      { path: 'fund-detail/document-add', component: AddFundDocumentComponent },
      { path: 'fund-detail/document-edit', component: EditFundDocumentComponent },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})



export class AppRoutingModule { }
