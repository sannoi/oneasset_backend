import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class AnnounceService {

  constructor(private http: HttpClient) { }

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];


  changeFormatDate(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] === month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year;
  }

  getAnnounce(status, search) {
    const URL = cmsApi + '/get_announceList';
    const body = { IsActive: status, Search: search };
    return this.http.post(URL, body);
  }

  uploadFile(fileItem) {
    const uploadUrl = cmsApi + '/UploadFile'
    const uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    const body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_title(fileItem) {
    const uploadUrl = cmsApi + '/UploadFile_title'
    const uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    const body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  uploadFile_mobile(fileItem) {
    const uploadUrl = cmsApi + '/UploadFile_mobile'
    const uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    const body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_banner(fileItem) {
    const uploadUrl = cmsApi + '/UploadFile_banner'
    const uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    const body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  addAnnounce(newsTitle, newsType, newsDetail, newsTitleImage, userid, newsStatus, banner, banner_mobile, publicDate) {
    const addNewsUrl = cmsApi + '/add_announce';
    const body = {
      Title: newsTitle,
      Announce_type: newsType,
      Detail: newsDetail,
      Announce_img_Thum: newsTitleImage,
      createid: userid,
      IsActive: newsStatus,
      banner_img : banner,
      banner_img_mobile : banner_mobile,
      publicDate: publicDate
    };
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  getAnnounceByID(announce_id) {
    const URL = cmsApi + '/get_announceById';
    const body = { announce_id: announce_id };

    return this.http.post(URL, body);
  }

  delBannerAnnounce(announce_id) {
    const addNewsUrl = cmsApi + '/del_banner_announce'
    const body = {
      announce_id: announce_id
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  delTitleAnnounce(announce_id) {
    const addNewsUrl = cmsApi + '/del_title_announce'
    const body = {
      announce_id: announce_id
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  delBannerAnnounce_mobile(announce_id) {
    const addNewsUrl = cmsApi + '/del_banner_announce_mobile'
    const body = {
      announce_id: announce_id
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  updateAnnounce(Title, Announce_type, Detail, Announce_img_Thum, Createid,
    IsActive,  Announceid, banner_img, banner_img_mobile, publicDate) {
    const addNewsUrl = cmsApi + '/update_announce'
    const body = {
      Title: Title,
      Announce_type: Announce_type,
      Detail: Detail,
      Announce_img_Thum: Announce_img_Thum,
      Createid: Createid,
      IsActive: IsActive,
      Announceid : Announceid,
      banner_img : banner_img,
      banner_img_mobile : banner_img_mobile,
      publicDate: publicDate
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    return (day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }
}
