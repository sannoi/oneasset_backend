import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class ManagerListService {

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];

  constructor(private http: HttpClient) { }

  changeFormatDate(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] === month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year;
  }

  getmanagerList(status, search) {
    let URL = cmsApi + '/get_managerList';
    let body = { IsActive: status, Search: search };
    return this.http.post(URL, body);
  }

  uploadFile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_title(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_title'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  uploadFile_mobile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_mobile'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_banner(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_banner'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }




  addmanagerList(newsTitle, newsType, newsDetail, newsTitleImage, userid, newsStatus, banner, banner_mobile, publicDate) {
    let addNewsUrl = cmsApi + '/add_managerList'
    let body = {
      Title: newsTitle,
      managerList_type: newsType,
      Detail: newsDetail,
      managerList_img_Thum: newsTitleImage,
      createid: userid,
      IsActive: newsStatus,
      banner_img : banner,
      banner_img_mobile : banner_mobile,
      publicDate: publicDate
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  getmanagerListByID(managerList_id) {
    let URL = cmsApi + '/get_managerListById';
    let body = { managerList_id: managerList_id };

    return this.http.post(URL, body);
  }

  delBannermanagerList(managerList_id) {
    let addNewsUrl = cmsApi + '/del_banner_managerList'
    let body = {
      managerList_id: managerList_id
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  delTitlemanagerList(managerList_id) {
    let addNewsUrl = cmsApi + '/del_title_managerList'
    let body = {
      managerList_id: managerList_id
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  delBannermanagerList_mobile(managerList_id) {
    let addNewsUrl = cmsApi + '/del_banner_managerList_mobile'
    let body = {
      managerList_id: managerList_id
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  updatemanagerList(Title, managerList_type, Detail, managerList_img_Thum, Createid,
    IsActive,  managerListid, banner_img, banner_img_mobile, publicDate) {
    let addNewsUrl = cmsApi + '/update_managerList'
    let body = {
      Title: Title,
      managerList_type: managerList_type,
      Detail: Detail,
      managerList_img_Thum: managerList_img_Thum,
      Createid: Createid,
      IsActive: IsActive,
      managerListid : managerListid,
      banner_img : banner_img,
      banner_img_mobile : banner_img_mobile,
      publicDate: publicDate
    }
    console.log("send body: ",body);
    return this.http.post(addNewsUrl, body)
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    return (day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }
}
