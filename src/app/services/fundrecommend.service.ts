import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class FundrecommendService {

  constructor(private http: HttpClient) { }

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];


  changeFormatDate(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] === month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year;
  }

  getFundType() {
    let URL = 'http://tmapi.one-asset.com/api/Fund/GetFundType';
    let body = {};
    return this.http.get(URL, body);
  }

  getDupRecomm(fundid) {
    let URL = cmsApi + '/get_DupFundRecomm';
    let body = { fundid: fundid };
    return this.http.post(URL, body);
  }


  getfundRecomByID(fundRecomid) {
    let URL = cmsApi + '/get_fundRecomById';
    let body = { fundRecomid: fundRecomid };
    return this.http.post(URL, body);
  }

  getFundRecommByType(fundType, search) {
    let URL = cmsApi + '/get_fundRecomListByType';
    let body = { fundType: fundType, search: search };
    return this.http.post(URL, body);
  }

  getFundListByType(fundType) {
    let URL = 'http://tmapi.one-asset.com/api/Fund/GetList';
    let body = { FundTypeId: fundType };
    return this.http.post(URL, body);
  }

  uploadFile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_title(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_title'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  uploadFile_mobile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_mobile'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_banner(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_banner'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  getfundRecomList(IsActive, search) {
    let URL = cmsApi + '/get_fundRecomList';
    let body = {
      IsActive: IsActive,
      search: search
    };
    return this.http.post(URL, body);
  }


  addfundRecom(Title, fundRecom_type, Detail, fundRecom_img_Thum,
    Createid, Updateid, IsActive,
    banner_img, banner_img_mobile , publicDate) {
    let URL = cmsApi + '/add_fundRecom'
    let body = {
      Title: Title,
      fundRecom_type: fundRecom_type,
      Detail: Detail,
      fundRecom_img_Thum: fundRecom_img_Thum,
      Createid: Createid,
      Updateid: Updateid,
      IsActive: IsActive,
      banner_img: banner_img,
      banner_img_mobile: banner_img_mobile,
      publicDate: publicDate
    };

    return this.http.post(URL, body);
  }

  updatefundRecom(Title, fundRecom_type, Detail, fundRecom_img_Thum, Createid,
    IsActive, fundRecomid, banner_img, banner_img_mobile, publicDate) {
    let addNewsUrl = cmsApi + '/update_fundRecom'
    let body = {
      Title: Title,
      fundRecom_type: fundRecom_type,
      Detail: Detail,
      fundRecom_img_Thum: fundRecom_img_Thum,
      Createid: Createid,
      IsActive: IsActive,
      fundRecomid: fundRecomid,
      banner_img: banner_img,
      banner_img_mobile: banner_img_mobile,
      publicDate: publicDate
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  delBannerfundRecom(fundRecom_id) {
    let addNewsUrl = cmsApi + '/del_banner_fundRecom'
    let body = {
      fundRecomid: fundRecom_id
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  delTitlefundRecom(fundRecom_id) {
    let addNewsUrl = cmsApi + '/del_title_fundRecom'
    let body = {
      fundRecomid: fundRecom_id
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  delBannerfundRecom_mobile(fundRecom_id) {
    let addNewsUrl = cmsApi + '/del_banner_fundRecom_mobile'
    let body = {
      fundRecomid: fundRecom_id
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    return (day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }


}
