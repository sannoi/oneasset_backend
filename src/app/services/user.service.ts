import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUserGroupList(groupName, status) {
    let URL = cmsApi + '/getUserGroup'
    let body = { GroupName: groupName, IsActive: status }
    return this.http.post(URL, body)
  }

  getUserGroupById(groupId) {
    let URL = cmsApi + '/getUserGroupById'
    let body = { Group_id: groupId }
    return this.http.post(URL, body)
  }

  delUserGroup(groupId) {
    let URL = cmsApi + '/delUserGroup'
    let body = { Group_id: groupId }
    return this.http.post(URL, body)
  }

  addUserGroup(groupName, status) {
    let URL = cmsApi + '/addUserGroup'
    let body = { GroupName: groupName, IsActive: status }
    return this.http.post(URL, body)
  }

  updateUserGroup(groupName, status, groupId) {
    let URL = cmsApi + '/UpdateUserGroup'
    let body = { GroupName: groupName, IsActive: status, Group_id: groupId }
    return this.http.post(URL, body)
  }

  getMenuList() {
    let URL = ''
    let body = {}
    return this.http.post(URL, body)
  }

  InsertMenuMapped(groupId, menuId, status) {
    let URL = cmsApi + '/InserMenuGroup'
    let body = { UserGroup_id: groupId, Menu_id: menuId, IsActive: status }
    return this.http.post(URL, body)
  }

  DeleteMenuMapped(menuId) {
    let URL = cmsApi + '/DelMenuGroup'
    let body = { Menu_id: menuId }
    return this.http.post(URL, body)
  }

  getMenuNotMap(groupId) {
    let URL = cmsApi + '/getMenuList_notMapp'
    let body = { UserGroup_id: groupId }
    return this.http.post(URL, body)
  }

  getMenuMapped(groupId) {
    let URL = cmsApi + '/getMenuList_Mapp'
    let body = { UserGroup_id: groupId }
    return this.http.post(URL, body)
  }

  getUserList(username, groupId) {
    let URL = cmsApi + '/getUserList'
    let body = { username: username, Group_id: groupId }
    return this.http.post(URL, body)
  }

  addUser(username, password, firstname, lastname, createId, status, email, groupId) {
    let URL = cmsApi + '/addUser'
    let body = {
      Username: username,
      Password: password,
      Fname: firstname,
      Lname: lastname,
      Createid: createId,
      IsActive: status,
      Email: email,
      group_id: groupId
    }
    return this.http.post(URL, body)
  }

  getUserById(userId) {
    let URL = cmsApi + '/getUserByID'
    let body = { userid: userId }
    return this.http.post(URL, body)
  }

  updateUser(userId, firstname, lastname, updateId, status, email, groupId) {
    let URL = cmsApi + '/updateUser'
    let body = {
      userid: userId,
      Fname: firstname,
      Lname: lastname,
      updateid: updateId,
      IsActive: status,
      Email: email,
      group_id: groupId
    }
    return this.http.post(URL, body)
  }

  resetPassword(userId, newPassword) {
    let URL = cmsApi + '/reset_password'
    let body = {
      password: newPassword,
      userid: userId
    }
    return this.http.post(URL, body)
  }

}
