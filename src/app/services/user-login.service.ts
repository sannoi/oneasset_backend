import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {

  constructor(private http: HttpClient) { }

  getUserLogin(inputUsername: string, inputPassword: string) {

    const _loginUrl = cmsApi + '/GetToken';
    const _loginBody = { username: inputUsername, password: inputPassword }
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post(_loginUrl, _loginBody, options);
  }

  chkExisting_password(userid: string, inputPassword: string) {
    const _loginUrl = cmsApi + '/chkExisting_password/';
    const _loginBody = { userid: userid, password: inputPassword }
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }
    return this.http.post(_loginUrl, _loginBody, options);
  }

  changePassword(userId, newPassword, oldPassword) {
    const _loginUrl = cmsApi + '/change_password'
    const _loginBody = { userid: userId, new_password: newPassword, old_password: oldPassword }
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }
    return this.http.post(_loginUrl, _loginBody, options);
  }

  checkTokenUserById(userId) {
    let URL = cmsApi + '/getUserByID'
    let body = { userid: userId }
    return this.http.post(URL, body)
  }

}
