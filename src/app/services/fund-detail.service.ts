import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class FundDetailService {

  constructor(private http: HttpClient) { }

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];

  changeFormatDate(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] === month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year;
  }


  getFund(fundSearch, publishStatus) {
    let URL = cmsApi + '/getFund_CMSByname';
    let body = { fundSearch: fundSearch, cms_publish_status: publishStatus }
    return this.http.post(URL, body)
  }

  getSync() {
    let URL = cmsApi + '/sync_fund';
    let body = {}
    return this.http.post(URL, body)
  }

  getSyncDoc() {
    let URL = cmsApi + '/getDocFund';
    let body = {}
    return this.http.post(URL, body)
  }

  getFundByID(id) {
    let URL = cmsApi + '/getFund_CMSById';
    let body = { id: id }
    return this.http.post(URL, body)
  }

  updateFundDetail(cmsFundId, publishStatus, reward, investmentStyle, investorType,
    investmentPolicy, accountPeriod, morningstarAward, morningStarLevel, morningStarDate,create_id) {
    let URL = cmsApi + '/Update_fund_cms';
    let body = {
      id: cmsFundId,
      cms_publish_status: publishStatus,
      cms_reward: reward,
      cms_investment_stype: investmentStyle,
      cms_investment_type: investorType,
      cms_investment_policy: investmentPolicy,
      cms_fund_cycle: accountPeriod,
      cms_morningstar_award: morningstarAward,
      cms_morningstar_level: morningStarLevel,
      cms_morningstar_date: morningStarDate,
      create_id: create_id
    }
    console.log('send update body: ', body);
    return this.http.post(URL, body)
  }

  getFundDoc(fundId) {
    let URL = cmsApi + '/getDocByFundId'
    let body = { id: fundId }
    return this.http.post(URL, body)
  }

  updateDoc(updateStatus, id) {
    let URL = cmsApi + '/Update_DocActive'
    let body = { Isactive: updateStatus, id: id }
    console.log('update doc send body: ', body);

    return this.http.post(URL, body)
  }


  uploadFile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_doc'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  InsertFile_DOC(FileName, File_Path, fundid, EN_TH) {
    let URL = cmsApi + '/InsertFile_DOC'
    let body = { FileName: FileName, File_Path: File_Path, fundid: fundid, EN_TH: EN_TH }
    return this.http.post(URL, body)
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date, short = false) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    if (short) {
      return (day + '/' + month + '/' + year);
    } else {
      return(day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
    }
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }

}
