import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';
@Injectable({
  providedIn: 'root'
})
export class CorruptionService {

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];

  constructor(private http: HttpClient) { }

  changeFormatDate(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] === month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year;
  }

  getcorruption(status, search) {
    let URL = cmsApi + '/get_corruptionList';
    let body = { IsActive: status, Search: search };
    return this.http.post(URL, body);
  }

  uploadFile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  uploadFile_title(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_title'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  uploadFile_mobile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_mobile'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_banner(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_banner'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  addcorruption(newsTitle, newsType, newsDetail, newsTitleImage, userid, newsStatus, banner, banner_mobile , publicDate) {
    let addNewsUrl = cmsApi + '/add_corruption'
    let body = {
      Title: newsTitle,
      corruption_type: newsType,
      Detail: newsDetail,
      corruption_img_Thum: newsTitleImage,
      createid: userid,
      IsActive: newsStatus,
      banner_img: banner,
      banner_img_mobile: banner_mobile,
      publicDate: publicDate
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  getcorruptionByID(corruption_id) {
    let URL = cmsApi + '/get_corruptionById';
    let body = { corruption_id: corruption_id };

    return this.http.post(URL, body);
  }

  delBannercorruption(corruption_id) {
    let addNewsUrl = cmsApi + '/del_banner_corruption'
    let body = {
      corruption_id: corruption_id
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  delTitlecorruption(corruption_id) {
    let addNewsUrl = cmsApi + '/del_title_corruption'
    let body = {
      corruption_id: corruption_id
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  delBannercorruption_mobile(corruption_id) {
    let addNewsUrl = cmsApi + '/del_banner_corruption_mobile'
    let body = {
      corruption_id: corruption_id
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  updatecorruption(Title, corruption_type, Detail, corruption_img_Thum, Createid,
    IsActive, corruptionid, banner_img, banner_img_mobile, publicDate) {
    let addNewsUrl = cmsApi + '/update_corruption'
    let body = {
      Title: Title,
      corruption_type: corruption_type,
      Detail: Detail,
      corruption_img_Thum: corruption_img_Thum,
      Createid: Createid,
      IsActive: IsActive,
      corruptionid: corruptionid,
      banner_img: banner_img,
      banner_img_mobile: banner_img_mobile,
      publicDate: publicDate
    }
    console.log("send body: ", body);
    return this.http.post(addNewsUrl, body)
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    return (day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }
}
