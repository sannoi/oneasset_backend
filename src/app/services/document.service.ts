import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';
@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];

  constructor(private http: HttpClient) { }

  changeFormatDate(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] === month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year;
  }

  getDocumentType(docType, typeStatus) {
    let URL = cmsApi + '/getDocTypeList';
    let body = { Document_type: docType, Type_status: typeStatus }
    return this.http.post(URL, body)
  }

  addDocType(docType, typeStatus, userid) {
    let URL = cmsApi + '/add_DocType';
    let body = { Document_type: docType, Type_status: typeStatus, userid: userid }
    return this.http.post(URL, body)
  }

  getDocTypeById(docTypeId) {
    let URL = cmsApi + '/getDocTypeById';
    let body = { docType_Id: docTypeId }
    return this.http.post(URL, body)
  }

  updateDocType(docTypeId, docType, typeStatus,userid) {
    let URL = cmsApi + '/update_DocType';
    let body = { docType_Id: docTypeId, Document_type: docType, Type_status: typeStatus, userid : userid }
    return this.http.post(URL, body)
  }

  deleteDocTypeById(docTypeId) {
    let URL = cmsApi + '/deleteDocTypeById'
    let body = { docType_Id: docTypeId }
    return this.http.post(URL, body)
  }


  deleteDocById(id) {
    let URL = cmsApi + '/deleteDocById'
    let body = { id: id }
    return this.http.post(URL, body)
  }

  getDocList(docName, docType, Isactive) {
    let URL = cmsApi + '/getWebDocument'
    let body = { Doc_name: docName, Doc_type: docType, Isactive: Isactive }
    return this.http.post(URL, body)
  }

  OrderUp(id) {
    let URL = cmsApi + '/OrderUp'
    let body = { id: id }
    return this.http.post(URL, body)
  }

  OrderDown(id) {
    let URL = cmsApi + '/OrderDown'
    let body = { id: id }
    return this.http.post(URL, body)
  }


  getDocById(id) {
    let URL = cmsApi + '/getWebDocumentByID'
    let body = { id: id }
    return this.http.post(URL, body)
  }

  uploadFile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile_doc'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  addDocument(fileName, docType, filePath, status, createId) {
    let URL = cmsApi + '/add_document'
    let body = { fileName: fileName, doc_type: docType, filePath: filePath, Createid: createId, IsActive: status }
    return this.http.post(URL, body)
  }

  update_document(Doc_type, Doc_name, Isactive, id, File_Path, update_id) {
    let URL = cmsApi + '/update_document'
    let body = { Doc_type: Doc_type, Doc_name: Doc_name, Isactive: Isactive, id: id, File_Path: File_Path,
      update_id: update_id }
    return this.http.post(URL, body)
  }

  addFundDocName(nameTh, nameEn) {
    let URL = cmsApi + '/add_fundDocName'
    let body = {
      TH_name: nameTh,
      EN_name: nameEn
    }
    return this.http.post(URL, body)
  }

  getFundDocName(searchFileName) {
    let URL = cmsApi + '/getFundDocName'
    let body = {
      searchName: searchFileName
    }
    return this.http.post(URL, body)
  }

  getFundDocNameByID(docNameId) {
    let URL = cmsApi + '/getFundDocNameByID'
    let body = {
      id: docNameId
    }
    return this.http.post(URL, body)
  }

  updateFundDocName(nameTh, nameEn, docNameId) {
    let URL = cmsApi + '/update_fundDocName'
    let body = {
      TH_name: nameTh,
      EN_name: nameEn,
      id: docNameId
    }
    return this.http.post(URL, body)
  }

  getMaintain(maintain_name) {
    let URL = cmsApi + '/getMaintain'
    let body = { maintain_name: maintain_name }
    return this.http.post(URL, body)
  }

  UpdateMaintain(maintain_name, r1, r2, r3, r4, r5) {
    let URL = cmsApi + '/UpdateMaintain'
    let body = {
      maintain_name: maintain_name,
      r1: r1,
      r2: r2,
      r3: r3,
      r4: r4,
      r5: r5,
    }
    return this.http.post(URL, body)
  }

  getMaintainCalFundType() {
    let URL = cmsApi + '/getMaintainCalFundType'
    let body = {}
    return this.http.post(URL, body)
  }

  updateMaintainCalFundType(fundTypeId, level1, level2, level3, level4, level5, groupId) {
    let URL = cmsApi + '/Update_MaintainCalFundType'
    let body = {
      FundTypeId: fundTypeId,
      r1: level1,
      r2: level2,
      r3: level3,
      r4: level4,
      r5: level5,
      id: groupId
    }
    return this.http.post(URL, body)
  }

  getMaintainCalFundTypeNotMapping(typeId, FundName) {
    let URL = cmsApi + '/getMaintainCalFundType_notMapping'
    let body = {
      fundType_id: typeId,
      FundName: FundName
    }
    return this.http.post(URL, body)
  }

  getMaintainCalFundTypeMapped(typeId) {
    let URL = cmsApi + '/getMaintainCalFundType_Mapping'
    let body = {
      fundType_id: typeId
    }
    return this.http.post(URL, body)
  }

  addMaintainCalFundType(typeId, fundId) {
    let URL = cmsApi + '/add_MaintainCalFundType'
    let body = {
      fundType_id: typeId,
      fundid: fundId
    }
    return this.http.post(URL, body)
  }

  deleteMainTainCalFundType(deleteid) {
    let URL = cmsApi + '/del_MaintainCalFundType'
    let body = {
      id: deleteid
    }
    return this.http.post(URL, body)
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date, short = false) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    if (short) {
      return (day + '/' + month + '/' + year);
    } else {
      return(day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
    }
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }

}
