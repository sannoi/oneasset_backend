import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class FundRecomMappService {

  constructor(private http: HttpClient) { }

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];


  changeFormatDate(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] === month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year;
  }

  // getFundRecom_map( search) {
  //   let URL = cmsApi + '/get_fund_recomend_map_list';
  //   let body = { name: name };
  //   return this.http.post(URL, body);
  // }

  get_fund_Not_mapp() {
    let URL = cmsApi + '/get_fund_Not_mapp';
    let body = {};
    return this.http.post(URL, body);
  }

  get_fundRecomMappList() {
    let URL = cmsApi + '/get_fundRecomMappList';
    let body = {};
    return this.http.post(URL, body);
  }

  get_fundRecomMappListByName(name) {
    let URL = cmsApi + '/get_fundRecomMappListByName';
    let body = { name: name };
    return this.http.post(URL, body);
  }

  get_fundNotRecom_mappByName(name) {
    let URL = cmsApi + '/get_fundNotRecom_mappByName';
    let body = { name: name };
    return this.http.post(URL, body);
  }

  Update_FundRecomm(fundid) {
    let URL = cmsApi + '/Update_FundRecomm';
    let body = { fundid: fundid };
    return this.http.post(URL, body);
  }


  Update_FundRecommStatus(fundid, cms_flag_recommend) {
    let URL = cmsApi + '/Update_FundRecommStatus';
    let body = {
      fundid: fundid,
      cms_flag_recommend: cms_flag_recommend
    };
    return this.http.post(URL, body);
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date, short = false) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    if (short) {
      return (day + '/' + month + '/' + year);
    } else {
      return(day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
    }
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }



}
