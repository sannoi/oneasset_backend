import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];

  constructor(private http: HttpClient) { }

  getMenu(userid) {
    const URL = cmsApi + '/get_menuList';
    const body = { userid: userid };
    return this.http.post(URL, body);
  }

  getNews(status, search) {
    const URL = cmsApi + '/get_newsList';
    const body = { IsActive: status, Search: search };
    return this.http.post(URL, body);
  }

  addNews(newsTitle, newsType, newsDetail, newsDetailMobile, newsTitleImage,
    userid, newsStatus, banner, banner_mobile, publicDate) {
    const addNewsUrl = cmsApi + '/add_news';
    const body = {
      TItle: newsTitle,
      new_type: newsType,
      Detail: newsDetail,
      DetailMobile: newsDetailMobile,
      News_img_Thum: newsTitleImage,
      createid: userid,
      IsActive: newsStatus,
      banner_img: banner,
      banner_img_mobile: banner_mobile,
      publicDate : publicDate
    };
    console.log('send body: ', body);
    return this.http.post(addNewsUrl, body);
  }


  delBannerNews(newsid) {
    const addNewsUrl = cmsApi + '/del_banner'
    const body = {
      newsid: newsid
    }
    console.log('send body: ', body);
    return this.http.post(addNewsUrl, body)
  }

  delTitleNews(newsid) {
    const addNewsUrl = cmsApi + '/del_title_news'
    const body = {
      newsid: newsid
    }
    console.log('send body: ', body);
    return this.http.post(addNewsUrl, body)
  }

  delBannerNews_mobile(newsid) {
    const addNewsUrl = cmsApi + '/del_banner_mobile'
    const body = {
      newsid: newsid
    }
    console.log('send body: ', body);
    return this.http.post(addNewsUrl, body)
  }

  updateNews(newTItle, newNew_type, newDetail, newDetailMobile, newNews_img_Thum, newNews_img_Thum_mobile,
    newCreateid, newIsActive, newnewsid, banner_img, banner_img_mobile, publicDate) {
    const addNewsUrl = cmsApi + '/update_news'
    const body = {
      TItle: newTItle,
      new_type: newNew_type,
      Detail: newDetail,
      Detail_mobile: newDetailMobile,
      News_img_Thum: newNews_img_Thum,
      News_img_Thumb_Mobile: newNews_img_Thum_mobile,
      createid: newCreateid,
      IsActive: newIsActive,
      newsid: newnewsid,
      banner_img: banner_img,
      banner_img_mobile: banner_img_mobile,
      publicDate: publicDate
    }
    console.log('send body: ', body);
    console.log('send body: ', JSON.stringify(body));
    return this.http.post(addNewsUrl, body)
  }

  uploadFile(fileItem) {
    const uploadUrl = cmsApi + '/UploadFile'
    const uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    const body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_title(fileItem) {
    const uploadUrl = cmsApi + '/UploadFile_title'
    const uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    const body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  uploadFile_mobile(fileItem) {
    const uploadUrl = cmsApi + '/UploadFile_mobile'
    const uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    const body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  uploadFile_banner(fileItem) {
    const uploadUrl = cmsApi + '/UploadFile_banner'
    const uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    const body = uploadFormData
    return this.http.post(uploadUrl, body)
  }

  changeFormatDate(date) {
    console.log(date)

    const d = new Date(date);
    console.log(d)

    const month = d.getMonth() + 1 ;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    console.log('เดือน')
    console.log(month)
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year; //monthTh['name'] +
  }

  getNewsByID(newsid) {
    const URL = cmsApi + '/get_newsById';
    const body = { newsid: newsid };

    return this.http.post(URL, body);
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    return (day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }

}
