import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { cmsApi } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class FundRiskService {

  constructor(private http: HttpClient) { }

  checkMonth: Array<Object> = [
    {
      number: 1,
      name: 'มกราคม'
    },
    {
      number: 2,
      name: 'กุมภาพันธ์'
    },
    {
      number: 3,
      name: 'มีนาคม'
    },
    {
      number: 4,
      name: 'เมษายน'
    },
    {
      number: 5,
      name: 'พฤษภาคม'
    },
    {
      number: 6,
      name: 'มิถุนายน'
    },
    {
      number: 7,
      name: 'กรกฎาคม'
    },
    {
      number: 8,
      name: 'สิงหาคม'
    },
    {
      number: 9,
      name: 'กันยายน'
    },
    {
      number: 10,
      name: 'ตุลาคม'
    },
    {
      number: 11,
      name: 'พฤศจิกายน'
    },
    {
      number: 12,
      name: 'ธันวาคม'
    }
  ];


  changeFormatDate(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day = d.getDate()
    const year = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] === month)[0];
    return day + ' ' + monthTh['name'] + ' ' + year;
  }

  get_riskLevel() {
    let URL = cmsApi + '/get_riskLevel';
    let body = {};
    return this.http.post(URL, body);
  }

  get_riskLevelByRiskID(riskid) {
    let URL = cmsApi + '/get_riskLevelByRiskID';
    let body = { riskid: riskid };
    return this.http.post(URL, body);
  }

  get_fundByTypeNotMapping(fundtype, name, riskid) {
    let URL = cmsApi + '/get_fundByTypeNotMapping';
    let body = {
      fundtype: fundtype,
      name: name,
      riskid: riskid
    };

    console.log(body);
    return this.http.post(URL, body);
  }

  getFundRiskMapp(fundtype, riskid) {
    let URL = cmsApi + '/getFundRiskMapp';
    let body = {
      fundtype: fundtype,
      riskid: riskid
    };
    console.log(body);
    return this.http.post(URL, body);
  }

  Insert_FundRisk_recommend(riskid, fundtype, fundid, IsActive, create_id, update_id) {
    let URL = cmsApi + '/Insert_FundRisk_recommend';
    let body = {
      riskid: riskid,
      fundtype: fundtype,
      fundid: fundid,
      IsActive: IsActive,
      create_id: create_id,
      update_id: update_id
    };
    return this.http.post(URL, body);
  }
  Delete_FundRisk_recommend(id) {
    let URL = cmsApi + '/Delete_FundRisk_recommend';
    let body = { id: id };
    return this.http.post(URL, body);
  }

  uploadFile(fileItem) {
    let uploadUrl = cmsApi + '/UploadFile'
    let uploadFormData = new FormData();
    uploadFormData.append('image', fileItem)
    let body = uploadFormData
    return this.http.post(uploadUrl, body)
  }


  Update_FundRisk_recommend(Title, Image, riskDetail1_1, riskDetail1_1p, riskDetail1_2, riskDetail1_2p, riskDetail2p,
    riskDetail3_1, riskDetail3_1p, riskDetail3_2, riskDetail3_2p, p1, p2, p3, p4, p5, p6, riskid) {
    let uploadUrl = cmsApi + '/Update_FundRisk_recommend'
    let body = {
      Title: Title, Image: Image, riskDetail1_1: riskDetail1_1, riskDetail1_1p: riskDetail1_1p,
      riskDetail1_2: riskDetail1_2, riskDetail1_2p: riskDetail1_2p, riskDetail2p: riskDetail2p,
      riskDetail3_1: riskDetail3_1, riskDetail3_1p: riskDetail3_1p, riskDetail3_2: riskDetail3_2,
      riskDetail3_2p: riskDetail3_2p, p1: p1, p2: p2, p3: p3, p4: p4, p5: p5, p6: p6, riskid: riskid
    };
    return this.http.post(uploadUrl, body)
  }

  date_thai(date) {
    const d = new Date(date);
    const month = d.getMonth() + 1;
    const day   = d.getDate();
    const year  = d.getFullYear() + 543;
    const monthTh = this.checkMonth.filter(m => m['number'] == month)[0];
    return (day + ' ' + monthTh['name'] + ' ' + year);
  }

  date_convert(date, short = false) {
    const d = new Date(date);
    const month  = d.getMonth() + 1;
    const day    = d.getDate();
    const year   = d.getFullYear() + 543;
    const hour   = d.getHours();
    const minute = d.getMinutes();
    if (short) {
      return (day + '/' + month + '/' + year);
    } else {
      return(day + '/' + month + '/' + year + ' | ' + this.addZero(hour) + ':' + this.addZero(minute));
    }
  }

  addZero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }


}
