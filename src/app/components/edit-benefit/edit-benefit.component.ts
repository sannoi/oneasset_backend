import { Component, OnInit } from '@angular/core';
import { BenefitService } from 'src/app/services/benefit.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-benefit',
  templateUrl: './edit-benefit.component.html',
  styleUrls: ['./edit-benefit.component.scss']
})
export class EditBenefitComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _benefitService: BenefitService, private ngxService: NgxUiLoaderService) { }

  private sub: any;
  isTh: boolean
  isEn: boolean
  id: string;
  benefitType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  benefit: any;


  benefitTtitleImage: any;
  benefitBannerImage: any
  benefitBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  benefitFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  typebenefit: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusbenefit: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editbenefitForm: FormGroup;

  old_benefitType: string;
  old_benefitDetail: string;
  isPDF_old: any;

  vdo_id: any

      // TODO: Public Date
      publicDate: any;
      public_date: any;

  ngOnInit() {
// TODO: Public Date
flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m2')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

    this._benefitService.getbenefitByID(this.id).subscribe(response => {

      this.benefit = response

      this.benefitType = response[0]['benefit_type']
      this.title = response[0]['title']
      this.title_image = response[0]['benefit_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['benefit_img_Thum']

      if (response[0]['benefit_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

         // TODO: Public Date
         if (response[0]['publicDate']) {
          this.public_date = response[0]['publicDate'];
          this.publicDate  = response[0]['publicDate'];
        } else {
          this.public_date = new Date().toISOString().slice(0,10);
          this.publicDate  = new Date().toISOString().slice(0,10);
        }


      if (response[0]['benefit_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_benefitType = response[0]['benefit_type']
      this.old_benefitDetail = response[0]['detail']

      if (response[0]['benefit_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }


      //=============== Binding Info ==================//
      this.editbenefitForm = this.fb.group({
        benefitTypeControl: this.benefitType,
        benefitTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }

      // TODO: Public Date
      setPublicDate(public_date: string) {
        this.publicDate = public_date;
      }


  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setbenefitType(code) {
    this.benefitType = code;
    if (this.benefitType == this.old_benefitType) {
      //=============== Binding Info ==================//
      this.editbenefitForm = this.fb.group({
        benefitTypeControl: this.benefitType,
        benefitTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_benefitDetail,
        vdo_id: this.old_benefitDetail,
        public_date: this.public_date
      });
      this.detail = this.old_benefitDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editbenefitForm = this.fb.group({
        benefitTypeControl: this.benefitType,
        benefitTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }
  setbenefitTitle(title) { this.title = title; }
  setbenefitFiles(event) {
    if (event.target.id === 'benefitTitleImage') {
      this.benefitTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'benefitFile') {
      this.benefitFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setbenefitStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/benefit']);
  }

  deleteBanner() {
    this._benefitService.delBannerbenefit(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
      this.router.navigate(['/back-office/benefit/edit'], { queryParams: { id: this.id } });

    })
  }


  deleteBanner_mobile() {
    this._benefitService.delBannerbenefit_mobile(this.id).subscribe(response => {
      console.log(response)
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
      this.router.navigate(['/back-office/benefit/edit'], { queryParams: { id: this.id } });

    })
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = ""


  }

  setbenefitBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.benefitBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setbenefitBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.benefitBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editbenefit() {
    let userId = sessionStorage.getItem('userId')
    this.ngxService.start();

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      if (this.benefitType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._benefitService.uploadFile_title(this.benefitTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {

            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._benefitService.uploadFile_mobile(this.benefitBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._benefitService.uploadFile_banner(this.benefitBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._benefitService.updatebenefit(this.title, this.benefitType, editorHtmlTag, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/benefit']);

                    })
                })

              })

            } else if (this.status === '0') {
              this._benefitService.delBannerbenefit(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._benefitService.updatebenefit(this.title, this.benefitType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/benefit']);

                  })
              })
            }

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }



        })


      } else if (this.benefitType === 'image') {

        //==================== Title Image ================================
        this._benefitService.uploadFile_title(this.benefitTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {

            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            //==================== File Download ================================
            this._benefitService.uploadFile(this.benefitFiles).subscribe(response => {
              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              //==================== Banner Image ================================
              if (this.status === '1') {
                this._benefitService.uploadFile_mobile(this.benefitBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._benefitService.uploadFile_banner(this.benefitBannerImage).subscribe(response => {

                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._benefitService.updatebenefit(this.title, this.benefitType, this.FilePath, this.titleImagepath, userId,
                      this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                        this.router.navigate(['/back-office/benefit']);

                      })
                  })

                })

              } else if (this.status === '0') {
                this._benefitService.delBannerbenefit(this.id).subscribe(response => {

                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";

                  this._benefitService.updatebenefit(this.title, this.benefitType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/benefit']);

                    })
                })
              }



            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }




        })





      } else if (this.benefitType === 'pdf') {

        //==================== Title Image ================================
        this._benefitService.uploadFile_title(this.benefitTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {

            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            //==================== File Download ================================
            this._benefitService.uploadFile(this.benefitFiles).subscribe(response => {

              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              //==================== Banner Image ================================
              if (this.status === '1') {
                this._benefitService.uploadFile_mobile(this.benefitBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._benefitService.uploadFile_banner(this.benefitBannerImage).subscribe(response => {

                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._benefitService.updatebenefit(this.title, this.benefitType, this.FilePath, this.titleImagepath,
                      userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/benefit']);

                      })
                  })

                })

              } else if (this.status === '0') {
                this._benefitService.delBannerbenefit(this.id).subscribe(response => {

                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";

                  this._benefitService.updatebenefit(this.title, this.benefitType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/benefit']);

                    })
                })
              }


            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();
          }




        })





      } else if (this.benefitType === 'vdo') {

        //==================== Title Image ================================
        this._benefitService.uploadFile_title(this.benefitTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {

            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._benefitService.uploadFile_mobile(this.benefitBannerImage_mobile).subscribe(response => {
                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._benefitService.uploadFile_banner(this.benefitBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._benefitService.updatebenefit(this.title, this.benefitType, this.vdo_id,
                    this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/benefit']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._benefitService.delBannerbenefit(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._benefitService.updatebenefit(this.title, this.benefitType, this.vdo_id,
                  this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/benefit']);
                  })

              })
            }

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();
          }







        })





      }

    }


    this.ngxService.stop();

  }




}
