import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GovernenceService } from 'src/app/services/governence.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-good-governence',
  templateUrl: './add-good-governence.component.html',
  styleUrls: ['./add-good-governence.component.scss']
})
export class AddGoodGovernenceComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };

  isTh: boolean
  isEn: boolean
  governenceType: string;
  typegovernence: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  governenceStatus: string;
  statusgovernence: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add governence
  governenceTitle: string
  governenceTitleEN: string
  governenceTtitleImage: any
  governenceBannerImage: any
  imageTitleName: any
  governenceContentImage: any
  imageContentName: any
  governenceFiles: any
  governenceFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any
  vdo_id: any

  governenceBannerImage_mobile: any;
  BannerImagepath_mobile: any;
  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _governenceService: GovernenceService, private router: Router,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    // TODO: Public Date
    flatpickr.localize(Thai);
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.governenceType = 'text';
    this.governenceStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }


  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setGovernenceType(code) {
    // console.log("choice: ", code);
    this.governenceType = code;
  }

  setGovernenceTitle(title: string) {
    this.governenceTitle = title
  }

  setGovernenceTitleEN(title: string) {
    this.governenceTitleEN = title
  }

  setGovernenceStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.governenceStatus = '1'
    } else if (code === 'Unpublish ') {
      this.governenceStatus = '0'
    }
  }

  setGovernenceFiles(event) {
    if (event.target.id === 'GovernenceTitleImage') {
      this.governenceTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'GovernenceContentImage') {
      this.governenceContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'GovernenceFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.governenceFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }

    }
  }

  setGovernenceBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.governenceBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setGovernenceBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.governenceBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addGovernence() {
    let userId = sessionStorage.getItem('userId')

    if (this.governenceTitle == undefined || this.governenceTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {
      this.ngxService.start();

      if (this.governenceType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._governenceService.uploadFile_title(this.governenceTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._governenceService.uploadFile_banner(this.governenceBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._governenceService.uploadFile_mobile(this.governenceBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._governenceService.addgovernence(this.governenceTitle, this.governenceType, editorHtmlTag, this.titleImagepath, userId,
                this.governenceStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/governence']);
                })
            })
          })
        })

      }
      else if (this.governenceType === 'image') {
        //==================== Title Image ================================
        this._governenceService.uploadFile_title(this.governenceTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._governenceService.uploadFile(this.governenceContentImage).subscribe(response => {
            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._governenceService.uploadFile_banner(this.governenceBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();

              this._governenceService.uploadFile_mobile(this.governenceBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._governenceService.addgovernence(this.governenceTitle, this.governenceType, this.contentImagePath, this.titleImagepath,
                  userId, this.governenceStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/governence']);

                  })


              })
            })
          })
        })








      } else if (this.governenceType === 'pdf') {

        if (this.governenceFileName == undefined || this.governenceFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {
          //==================== Title Image ================================
          this._governenceService.uploadFile_title(this.governenceTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._governenceService.uploadFile(this.governenceFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._governenceService.uploadFile_banner(this.governenceBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._governenceService.uploadFile_mobile(this.governenceBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._governenceService.addgovernence(this.governenceTitle, this.governenceType, this.pdfFilePath, this.titleImagepath,
                    userId, this.governenceStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/governence']);

                    })
                })
              })
            })
          })

        }





      }

      else if (this.governenceType === 'vdo') {
        //==================== Title Image ================================
        this._governenceService.uploadFile_title(this.governenceTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._governenceService.uploadFile_banner(this.governenceBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._governenceService.uploadFile_mobile(this.governenceBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._governenceService.addgovernence(this.governenceTitle, this.governenceType, this.vdo_id, this.titleImagepath,
                userId, this.governenceStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/governence']);

                })
            })
          })
        })








      }

      this.ngxService.stop();

    }




  }

  Back() {
    this.router.navigate(['/back-office/governence']);
  }

}
