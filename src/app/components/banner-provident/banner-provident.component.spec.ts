import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerProvidentComponent } from './banner-provident.component';

describe('BannerProvidentComponent', () => {
  let component: BannerProvidentComponent;
  let fixture: ComponentFixture<BannerProvidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerProvidentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerProvidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
