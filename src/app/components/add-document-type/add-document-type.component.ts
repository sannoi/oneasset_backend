import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DocumentService } from 'src/app/services/document.service'

@Component({
  selector: 'app-add-document-type',
  templateUrl: './add-document-type.component.html',
  styleUrls: ['./add-document-type.component.scss']
})
export class AddDocumentTypeComponent implements OnInit {

  docTypeName: string
  statusDocType: any[] = [
    { code: 'y', chooes: 'Publish' },
    { code: 'n', chooes: 'Unpublish ' },
  ];
  docTypeStatus: string


  constructor(private router: Router, private _documentService: DocumentService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    
    let ress = sessionStorage.getItem('m6')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.docTypeStatus = 'y'
  }

  setDoctypeName(docTypeName) {
    this.docTypeName = docTypeName
  }

  setDocTypeStatus(docStatus) {
    console.log(docStatus);
    this.docTypeStatus = docStatus
  }

  addDocType() {
    let userId = sessionStorage.getItem('userId')
    this._documentService.addDocType(this.docTypeName, this.docTypeStatus, userId).subscribe(response => {
      console.log('after add doctype:', response);
      this.router.navigate(['/back-office/document-type'])

    })
  }

  back() {
    this.router.navigate(['/back-office/document-type'])
  }

}
