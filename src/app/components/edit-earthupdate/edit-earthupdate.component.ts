import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EarthupdateService } from 'src/app/services/earthupdate.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-earthupdate',
  templateUrl: './edit-earthupdate.component.html',
  styleUrls: ['./edit-earthupdate.component.scss']
})
export class EditEarthupdateComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _earthupdateService: EarthupdateService, private ngxService: NgxUiLoaderService) { }

  private sub: any;

  isTh: boolean
  isEn: boolean
  id: string;
  EarthUpdateType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  EarthUpdate: any;

  EarthUpdateTtitleImage: any;
  EarthUpdateBannerImage: any
  EarthUpdateBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  EarthUpdateFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typeEarthUpdate: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusEarthUpdate: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editEarthUpdateForm: FormGroup;

  old_EarthUpdateType: string;
  old_EarthUpdateDetail: string;
  isPDF_old: any;

  vdo_id: any

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  ngOnInit() {
        // TODO: Public Date
        flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

    this._earthupdateService.getEarthUpdateByID(this.id).subscribe(response => {

      this.EarthUpdate = response
      console.log(this.EarthUpdate);
      this.EarthUpdateType = response[0]['earthUpdate_type']
      this.title = response[0]['title']
      this.title_image = response[0]['earthUpdate_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['earthUpdate_img_Thum']

      if (response[0]['earthUpdate_type'] != "text") {
        this.FilePath = response[0]['detail']
      }


         // TODO: Public Date
         if (response[0]['publicDate']) {
          this.public_date = response[0]['publicDate'];
          this.publicDate  = response[0]['publicDate'];
        } else {
          this.public_date = new Date().toISOString().slice(0,10);
          this.publicDate  = new Date().toISOString().slice(0,10);
        }

      if (response[0]['earthUpdate_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_EarthUpdateType = response[0]['earthUpdate_type']
      this.old_EarthUpdateDetail = response[0]['detail']

      if (response[0]['earthUpdate_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }

      //=============== Binding Info ==================//
      this.editEarthUpdateForm = this.fb.group({
        EarthUpdateTypeControl: this.EarthUpdateType,
        EarthUpdateTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date

      });
      //==============================================//
    })
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setEarthUpdateType(code) {
    this.EarthUpdateType = code;
    if (this.EarthUpdateType == this.old_EarthUpdateType) {
      //=============== Binding Info ==================//
      this.editEarthUpdateForm = this.fb.group({
        EarthUpdateTypeControl: this.EarthUpdateType,
        EarthUpdateTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_EarthUpdateDetail,
        vdo_id: this.old_EarthUpdateDetail,
        public_date: this.public_date
      });
      this.detail = this.old_EarthUpdateDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editEarthUpdateForm = this.fb.group({
        EarthUpdateTypeControl: this.EarthUpdateType,
        EarthUpdateTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setEarthUpdateTitle(title) { this.title = title; }
  setEarthUpdateFiles(event) {
    if (event.target.id === 'EarthUpdateTitleImage') {
      this.EarthUpdateTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'EarthUpdateFile') {
      this.EarthUpdateFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setEarthUpdateStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/earthupdate']);
  }

  deleteBanner() {
    this._earthupdateService.delBannerEarthUpdate(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/earthupdate/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._earthupdateService.delBannerEarthUpdate_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/earthupdate/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = ""

  }

  setEarthUpdateBannerFiles(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.EarthUpdateBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }
      };
    }
  }


  setEarthUpdateBannerFiles_mobile(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.EarthUpdateBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editEarthUpdate() {
    let userId = sessionStorage.getItem('userId')

    this.ngxService.start();

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      if (this.EarthUpdateType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._earthupdateService.uploadFile_title(this.EarthUpdateTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._earthupdateService.uploadFile_mobile(this.EarthUpdateBannerImage_mobile).subscribe(response => {


              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }


              this._earthupdateService.uploadFile_banner(this.EarthUpdateBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._earthupdateService.updateEarthUpdate(this.title, this.EarthUpdateType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/earthupdate']);
                  })

              })

            })

          } else if (this.status === '0') {
            this._earthupdateService.delBannerEarthUpdate(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._earthupdateService.updateEarthUpdate(this.title, this.EarthUpdateType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/earthupdate']);
                })
            })
          }


        })





      } else if (this.EarthUpdateType === 'image') {

        //==================== Title Image ================================
        this._earthupdateService.uploadFile_title(this.EarthUpdateTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._earthupdateService.uploadFile(this.EarthUpdateFiles).subscribe(response => {

            if (response.toString() != "") {
              this.FilePath = response.toString();
            }


            //==================== Banner Image ================================
            if (this.status === '1') {
              this._earthupdateService.uploadFile_mobile(this.EarthUpdateBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._earthupdateService.uploadFile_banner(this.EarthUpdateBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._earthupdateService.updateEarthUpdate(this.title, this.EarthUpdateType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/earthupdate']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._earthupdateService.delBannerEarthUpdate(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._earthupdateService.updateEarthUpdate(this.title, this.EarthUpdateType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/earthupdate']);
                  })
              })
            }


          })





        })



      } else if (this.EarthUpdateType === 'pdf') {

        //==================== Title Image ================================
        this._earthupdateService.uploadFile_title(this.EarthUpdateTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._earthupdateService.uploadFile(this.EarthUpdateFiles).subscribe(response => {

            if (response.toString() != "") {
              this.FilePath = response.toString();
            }


            //==================== Banner Image ================================
            if (this.status === '1') {
              this._earthupdateService.uploadFile_mobile(this.EarthUpdateBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._earthupdateService.uploadFile_banner(this.EarthUpdateBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._earthupdateService.updateEarthUpdate(this.title, this.EarthUpdateType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/earthupdate']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._earthupdateService.delBannerEarthUpdate(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._earthupdateService.updateEarthUpdate(this.title, this.EarthUpdateType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/earthupdate']);
                  })

              })
            }




          })





        })




      }
      else if (this.EarthUpdateType === 'vdo') {

        //==================== Title Image ================================
        this._earthupdateService.uploadFile_title(this.EarthUpdateTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._earthupdateService.uploadFile_mobile(this.EarthUpdateBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._earthupdateService.uploadFile_banner(this.EarthUpdateBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._earthupdateService.updateEarthUpdate(this.title, this.EarthUpdateType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/earthupdate']);
                  })
              })

            })

          } else if (this.status === '0') {
            this._earthupdateService.delBannerEarthUpdate(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._earthupdateService.updateEarthUpdate(this.title, this.EarthUpdateType, this.vdo_id, this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/earthupdate']);
                })


            })
          }



        })



      }

    }


    this.ngxService.stop();





  }
















}
