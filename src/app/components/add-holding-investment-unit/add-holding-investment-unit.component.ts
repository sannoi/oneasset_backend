import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HoldingInvestmentUnitService } from 'src/app/services/holding-investment-unit.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-holding-investment-unit',
  templateUrl: './add-holding-investment-unit.component.html',
  styleUrls: ['./add-holding-investment-unit.component.scss']
})
export class AddHoldingInvestmentUnitComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  holdingInvestUnitType: string;
  typeholdingInvestUnit: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  holdingInvestUnitStatus: string;
  statusholdingInvestUnit: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add holdingInvestUnit
  holdingInvestUnitTitle: string
  holdingInvestUnitTitleEN: string
  holdingInvestUnitTtitleImage: any
  holdingInvestUnitBannerImage: any
  imageTitleName: any
  holdingInvestUnitContentImage: any
  imageContentName: any
  holdingInvestUnitFiles: any
  holdingInvestUnitFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any


  holdingInvestUnitBannerImage_mobile: any;
  BannerImagepath_mobile: any;

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private _holdingInvestUnitService: HoldingInvestmentUnitService, private router: Router,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
 // TODO: Public Date
 flatpickr.localize(Thai);
 this.public_date = new Date().toISOString().slice(0,10);
 this.publicDate = this.public_date;
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.holdingInvestUnitType = 'text';
    this.holdingInvestUnitStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setHoldingInvestUnitType(code) {
    // console.log("choice: ", code);
    this.holdingInvestUnitType = code;
  }

  setHoldingInvestUnitTitle(title: string) {
    this.holdingInvestUnitTitle = title
  }

  setHoldingInvestUnitTitleEN(title: string) {
    this.holdingInvestUnitTitleEN = title
  }

  setHoldingInvestUnitStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.holdingInvestUnitStatus = '1'
    } else if (code === 'Unpublish ') {
      this.holdingInvestUnitStatus = '0'
    }
  }

  setHoldingInvestUnitFiles(event) {
    if (event.target.id === 'HoldingInvestUnitTitleImage') {
      this.holdingInvestUnitTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'HoldingInvestUnitContentImage') {
      this.holdingInvestUnitContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'HoldingInvestUnitFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.holdingInvestUnitFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }


    }
  }

  setHoldingInvestUnitBannerFiles(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.holdingInvestUnitBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setHoldingInvestUnitBannerFiles_mobile(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.holdingInvestUnitBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  addHoldingInvestUnit() {
    let userId = sessionStorage.getItem('userId')
    if (this.holdingInvestUnitTitle == undefined || this.holdingInvestUnitTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();

      if (this.holdingInvestUnitType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._holdingInvestUnitService.uploadFile_title(this.holdingInvestUnitTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._holdingInvestUnitService.uploadFile_banner(this.holdingInvestUnitBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._holdingInvestUnitService.uploadFile_mobile(this.holdingInvestUnitBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._holdingInvestUnitService.addholdingInvestUnit(this.holdingInvestUnitTitle, this.holdingInvestUnitType, editorHtmlTag, this.titleImagepath, userId,
                this.holdingInvestUnitStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/holding-investment-unit']);
                })
            })
          })

        })





      }
      else if (this.holdingInvestUnitType === 'image') {
        //==================== Title Image ================================
        this._holdingInvestUnitService.uploadFile_title(this.holdingInvestUnitTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();


          //==================== File Download ================================
          this._holdingInvestUnitService.uploadFile(this.holdingInvestUnitContentImage).subscribe(response => {

            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._holdingInvestUnitService.uploadFile_banner(this.holdingInvestUnitBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();


              this._holdingInvestUnitService.uploadFile_mobile(this.holdingInvestUnitBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._holdingInvestUnitService.addholdingInvestUnit(this.holdingInvestUnitTitle, this.holdingInvestUnitType, this.contentImagePath, this.titleImagepath,
                  userId, this.holdingInvestUnitStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/holding-investment-unit']);

                  })
              })
            })
          })
        })







      } else if (this.holdingInvestUnitType === 'pdf') {

        if (this.holdingInvestUnitFileName == undefined || this.holdingInvestUnitFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._holdingInvestUnitService.uploadFile_title(this.holdingInvestUnitTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();


            //==================== File Download ================================
            this._holdingInvestUnitService.uploadFile(this.holdingInvestUnitFileName).subscribe(response => {
              this.pdfFilePath = response.toString();


              //==================== Banner Image ================================
              this._holdingInvestUnitService.uploadFile_banner(this.holdingInvestUnitBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();


                this._holdingInvestUnitService.uploadFile_mobile(this.holdingInvestUnitBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._holdingInvestUnitService.addholdingInvestUnit(this.holdingInvestUnitTitle, this.holdingInvestUnitType, this.pdfFilePath, this.titleImagepath,
                    userId, this.holdingInvestUnitStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/holding-investment-unit']);

                    })
                })
              })
            })

          })


        }




      }

      else if (this.holdingInvestUnitType === 'vdo') {
        //==================== Title Image ================================
        this._holdingInvestUnitService.uploadFile_title(this.holdingInvestUnitTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();


          //==================== Banner Image ================================
          this._holdingInvestUnitService.uploadFile_banner(this.holdingInvestUnitBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();


            this._holdingInvestUnitService.uploadFile_mobile(this.holdingInvestUnitBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._holdingInvestUnitService.addholdingInvestUnit(this.holdingInvestUnitTitle, this.holdingInvestUnitType,
                this.vdo_id, this.titleImagepath,
                userId, this.holdingInvestUnitStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/holding-investment-unit']);

                })
            })
          })
        })






      }

      this.ngxService.stop();
    }


  }

  Back() {
    this.router.navigate(['/back-office/holding-investment-unit']);
  }

}
