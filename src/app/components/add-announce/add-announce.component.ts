import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AnnounceService } from 'src/app/services/announce.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-announce',
  templateUrl: './add-announce.component.html',
  styleUrls: ['./add-announce.component.scss']
})
export class AddAnnounceComponent implements OnInit {

  isTh: boolean
  isEn: boolean
  announceType: string;
  typeannounce: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  announceStatus: string;
  statusannounce: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };

  // for add announce
  announceTitle: string
  announceTitleEN: string
  announceTitleImage: any
  announceTitleImageMobile
  announceBannerImage: any
  imageTitleName: any
  announceContentImage: any
  announceContentImageMobile: any
  imageContentName: any
  announceFiles: any
  announceFileName: any

  // path response from server
  titleImagepath: any
  titleImagepathMobile: any
  BannerImagepath: any
  contentImagePath: any
  contentImagePathMobile: any
  pdfFilePath: any

  announceBannerImage_mobile: any;
  BannerImagepath_mobile: any;

  vdo_id: any

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _announceService: AnnounceService, private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }


  ngOnInit() {
    flatpickr.localize(Thai);
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.isTh = true
    this.isEn = false
    this.announceType = 'text';
    this.announceStatus = '1';
    this.BannerImagepath = '';
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setAnnounceType(code) {
    // console.log("choice: ", code);
    this.announceType = code;
  }

  setAnnounceTitle(title: string) {
    this.announceTitle = title
  }

  setAnnounceTitleEn(title: string) {
    this.announceTitleEN = title
  }

  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setAnnounceStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.announceStatus = '1'
    } else if (code === 'Unpublish ') {
      this.announceStatus = '0'
    }
  }

  setAnnounceFiles(event) {
    if (event.target.id === 'AnnounceTitleImage') {
      this.announceTitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name

    } else if (event.target.id === 'AnnounceTitleImageMobile') {
      this.announceTitleImageMobile = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/img_mobile/' + <File>event.target.files[0].name

    } else if (event.target.id === 'AnnounceContentImage') {
      this.announceContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name

    } else if (event.target.id === 'AnnounceContentImageMobile') {
      this.announceContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/img_mobile/' + <File>event.target.files[0].name

    } else if (event.target.id === 'AnnounceFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.announceFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }

    }
  }

  setAnnounceBannerFiles(event) {
    var file_true = false;
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.announceBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }


  setAnnounceBannerFiles_mobile(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.announceBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addAnnounce() {
    let userId = sessionStorage.getItem('userId')

    if (this.announceTitle == undefined || this.announceTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();
      if (this.announceType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._announceService.uploadFile_title(this.announceTitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._announceService.uploadFile_banner(this.announceBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            //==================== Banner Mobile Image ================================
            this._announceService.uploadFile_mobile(this.announceBannerImage_mobile).subscribe(response => {

              this.BannerImagepath_mobile = response.toString();

              this._announceService.addAnnounce(this.announceTitle, this.announceType, editorHtmlTag, this.titleImagepath, userId,
                this.announceStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/announce']);
                })
            })
          })
        })


      }
      else if (this.announceType === 'image') {
        //==================== Title Image ================================
        this._announceService.uploadFile_title(this.announceTitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== File Download ================================
          this._announceService.uploadFile(this.announceContentImage).subscribe(response => {
            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._announceService.uploadFile_banner(this.announceBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();
              //==================== Banner Mobile Image ================================
              this._announceService.uploadFile_mobile(this.announceBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._announceService.addAnnounce(this.announceTitle, this.announceType, this.contentImagePath,
                  this.titleImagepath,
                  userId, this.announceStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/announce']);

                  })
              })
            })
          })
        })

      } else if (this.announceType === 'pdf') {

        console.log(this.announceFileName)

        if (this.announceFileName == undefined || this.announceFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();

        }
        else {
          //==================== Title Image ================================
          this._announceService.uploadFile_title(this.announceTitleImage).subscribe(response => {
            this.titleImagepath = response.toString();
            //==================== File Download ================================
            this._announceService.uploadFile(this.announceFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._announceService.uploadFile_banner(this.announceBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();
                this._announceService.uploadFile_mobile(this.announceBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._announceService.addAnnounce(this.announceTitle, this.announceType, this.pdfFilePath, this.titleImagepath,
                    userId, this.announceStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/announce']);

                    })

                })
              })
            })
          })
        }



      }

      else if (this.announceType === 'vdo') {
        //==================== Title Image ================================
        this._announceService.uploadFile_title(this.announceTitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._announceService.uploadFile_banner(this.announceBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();
            //==================== Banner Image MObile ================================
            this._announceService.uploadFile_mobile(this.announceBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();
              this._announceService.addAnnounce(this.announceTitle, this.announceType, this.vdo_id, this.titleImagepath,
                userId, this.announceStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/announce']);

                })
            })
          })
        })






      }
      this.ngxService.stop();
    }




  }

  Back() {
    this.router.navigate(['/back-office/announce']);
  }



}
