import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-expected-return',
  templateUrl: './add-expected-return.component.html',
  styleUrls: ['./add-expected-return.component.scss']
})
export class AddExpectedReturnComponent implements OnInit {

  //Expected Return
  extlvl1: any
  extlvl2: any
  extlvl3: any
  extlvl4: any
  extlvl5: any

  constructor(private router: Router) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
  }

  addExpectedReturn() {
    console.log(parseFloat(this.extlvl1),
      parseFloat(this.extlvl2), parseFloat(this.extlvl3),
      parseFloat(this.extlvl4), parseFloat(this.extlvl5));
    this.router.navigate(['/back-office/maintain'])
  }

  Back() {
    this.router.navigate(['/back-office/maintain'])
  }

}
