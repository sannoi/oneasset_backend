import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { VoteService } from 'src/app/services/vote.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';
// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-vote',
  templateUrl: './edit-vote.component.html',
  styleUrls: ['./edit-vote.component.scss']
})
export class EditVoteComponent implements OnInit {

  private sub: any;
  isTh: boolean
  isEn: boolean
  id: string;
  VoteType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  Vote: any;

  VoteTtitleImage: any;
  VoteBannerImage: any
  VoteBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  VoteFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typeVote: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusVote: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editVoteForm: FormGroup;

  old_voteType: string;
  old_voteDetail: string;
  isPDF_old: any;

  vdo_id: any


    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _voteService: VoteService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
 // TODO: Public Date
 flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

    this._voteService.getvoteByID(this.id).subscribe(response => {

      this.Vote = response

      this.VoteType = response[0]['vote_type']
      this.title = response[0]['title']
      this.title_image = response[0]['vote_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['vote_img_Thum']

      if (response[0]['vote_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

 // TODO: Public Date
if (response[0]['publicDate']) {
  this.public_date = response[0]['publicDate'];
  this.publicDate  = response[0]['publicDate'];
} else {
  this.public_date = new Date().toISOString().slice(0,10);
  this.publicDate  = new Date().toISOString().slice(0,10);
}

      if (response[0]['vote_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_voteType = response[0]['vote_type']
      this.old_voteDetail = response[0]['detail']

      if (response[0]['vote_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }



      //=============== Binding Info ==================//
      this.editVoteForm = this.fb.group({
        VoteTypeControl: this.VoteType,
        VoteTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date : this.public_date
      });
      //==============================================//
    })
  }


  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setVoteType(code) {
    this.VoteType = code;
    if (this.VoteType == this.old_voteType) {
      //=============== Binding Info ==================//
      this.editVoteForm = this.fb.group({
        VoteTypeControl: this.VoteType,
        VoteTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_voteDetail,
        vdo_id: this.old_voteDetail,
        public_date : this.public_date
      });
      this.detail = this.old_voteDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editVoteForm = this.fb.group({
        VoteTypeControl: this.VoteType,
        VoteTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date : this.public_date
      });
      this.detail = "";
    }
  }
  setVoteTitle(title) { this.title = title; }
  setVoteFiles(event) {
    if (event.target.id === 'VoteTitleImage') {
      this.VoteTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'VoteFile') {
      this.VoteFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setVoteStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/vote-report']);
  }

  deleteBanner() {
    this._voteService.delBannervote(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/vote-report/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._voteService.delBannervote_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/vote-report/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = ""


  }

  setVoteBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {

          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.VoteBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }


      };
    }
  }

  setVoteBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.VoteBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editVote() {
    this.ngxService.start();

    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.VoteType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._voteService.uploadFile_title(this.VoteTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._voteService.uploadFile_mobile(this.VoteBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._voteService.uploadFile_banner(this.VoteBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }
                this._voteService.updatevote(this.title, this.VoteType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vote-report']);
                  })
              })

            })

          } else if (this.status === '0') {
            this._voteService.delBannervote(this.id).subscribe(response => {
              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";
              this._voteService.updatevote(this.title, this.VoteType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/vote-report']);
                })
            })
          }


        })




      } else if (this.VoteType === 'image') {

        //==================== Title Image ================================
        this._voteService.uploadFile_title(this.VoteTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== File Download ================================
          this._voteService.uploadFile(this.VoteFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._voteService.uploadFile_mobile(this.VoteBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._voteService.uploadFile_banner(this.VoteBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }


                  this._voteService.updatevote(this.title, this.VoteType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vote-report']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._voteService.delBannervote(this.id).subscribe(response => {
                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";
                this._voteService.updatevote(this.title, this.VoteType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vote-report']);
                  })
              })
            }



          })



        })




      } else if (this.VoteType === 'pdf') {

        //==================== Title Image ================================
        this._voteService.uploadFile_title(this.VoteTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== File Download ================================
          this._voteService.uploadFile(this.VoteFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._voteService.uploadFile_mobile(this.VoteBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._voteService.uploadFile_banner(this.VoteBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._voteService.updatevote(this.title, this.VoteType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vote-report']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._voteService.delBannervote(this.id).subscribe(response => {
                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";
                this._voteService.updatevote(this.title, this.VoteType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vote-report']);
                  })
              })
            }




          })


        })





      } else if (this.VoteType === 'vdo') {

        //==================== Title Image ================================
        this._voteService.uploadFile_title(this.VoteTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== Banner Image ================================
          if (this.status === '1') {
            this._voteService.uploadFile_mobile(this.VoteBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._voteService.uploadFile_banner(this.VoteBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._voteService.updatevote(this.title, this.VoteType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vote-report']);
                  })
              })


            })

          } else if (this.status === '0') {
            this._voteService.delBannervote(this.id).subscribe(response => {
              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";
              this._voteService.updatevote(this.title, this.VoteType, this.vdo_id, this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/vote-report']);
                })
            })
          }



        })





      }



    }

  }

}
