import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { DocumentService } from 'src/app/services/document.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {

  data: any[] = [
    { val: 'y', name: 'Publish ' },
    { val: 'n', name: 'Unpublish  ' },
  ];

  doc: any
  docList: any
  docStatus: any

  docDate: any

  responsedoctypeList: any
  docTypeList: any; // Array<any> = []
  doctype: any
  fileName: any
  //============================== Paging ======================
  // private docTypeList: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  constructor(private router: Router, private _documentService: DocumentService, private pagerService: PagerService,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m6')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this.ngxService.start();
    this._documentService.getDocumentType('', 'y').subscribe(response => {

      this.docTypeList = response
      this.fileName = ''
      this.docStatus = 'y'
      this.doctype = this.docTypeList[0].id


      this._documentService.getDocList('', this.docTypeList[0].id, this.docStatus).subscribe(response => {
        console.log(response)
        this.doc = response
        this.docList = response
        this.setPage(1);
        this.ngxService.stop();
      })
    });
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.docDate = this._documentService.changeFormatDate(date)
    }
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.docList.length, page);
    this.pagedItems = this.docList.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setDocType(docType) {
    this.doctype = docType
  }

  setStatus(status) {
    this.docStatus = status
  }


  setFileName(fileNameSearch) {
    console.log(fileNameSearch);

    this.fileName = fileNameSearch
  }

  getDocFromSerch() {


    this._documentService.getDocList(this.fileName, this.doctype, this.docStatus).subscribe(response => {
      console.log(response)
      this.doc = response
      this.docList = response
      this.setPage(1);
      this.ngxService.stop();
    })

  }

  clickEdit(xxx) {
    event.preventDefault()
    this.router.navigate(['/back-office/document/edit'], { queryParams: { id: xxx } })
  }

  clickAddDoc() {
    event.preventDefault()
    this.router.navigate(['/back-office/document/add'])
  }

  clickUp(id) {
    this._documentService.OrderUp(id).subscribe(response => {
      console.log(response)
      this.getDocFromSerch();
    })
  }

  clickDown(id) {
    console.log(id)
    this._documentService.OrderDown(id).subscribe(response => {
      console.log(response)
      this.getDocFromSerch();
    })
  }

  clickDel(id) {
    this._documentService.deleteDocById(id).subscribe(response => {
      console.log(response)
      this.getDocFromSerch();
    })
  }
}
