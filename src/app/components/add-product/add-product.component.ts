import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  productType: string;
  typeproduct: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  productStatus: string;
  statusproduct: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add product
  productTitle: string
  productTtitleImage: any
  productBannerImage: any
  imageTitleName: any
  productContentImage: any
  imageContentName: any
  productFiles: any
  productFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  productBannerImage_mobile: any;
  BannerImagepath_mobile: any;

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _productService: ProductService, private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }

  ngOnInit() {
     // TODO: Public Date
     flatpickr.localize(Thai);
     this.public_date = new Date().toISOString().slice(0,10);
     this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.productType = 'text';
    this.productStatus = '1';
    this.BannerImagepath = '';

    this.isTh = true
    this.isEn = false
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setProductType(code) {
    // console.log("choice: ", code);
    this.productType = code;
  }

  setProductTitle(title: string) {
    this.productTitle = title
  }

  setProductStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.productStatus = '1'
    } else if (code === 'Unpublish ') {
      this.productStatus = '0'
    }
  }

  setProductFiles(event) {
    if (event.target.id === 'ProductTitleImage') {
      this.productTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'ProductContentImage') {
      this.productContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'ProductFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.productFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }


    }
  }

  setProductBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {

          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.productBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;

        }

      };
    }
  }

  setProductBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.productBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addProduct() {
    let userId = sessionStorage.getItem('userId')
    if (this.productTitle == undefined || this.productTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();



      if (this.productType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._productService.uploadFile_title(this.productTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._productService.uploadFile_banner(this.productBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._productService.uploadFile_mobile(this.productBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._productService.addproduct(this.productTitle, this.productType, editorHtmlTag, this.titleImagepath, userId,
                this.productStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/product']);
                })
            })
          })
        })




      }
      else if (this.productType === 'image') {
        //==================== Title Image ================================
        this._productService.uploadFile_title(this.productTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== File Download ================================
          this._productService.uploadFile(this.productContentImage).subscribe(response => {
            this.contentImagePath = response.toString();
            //==================== Banner Image ================================
            this._productService.uploadFile_banner(this.productBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();

              this._productService.uploadFile_mobile(this.productBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._productService.addproduct(this.productTitle, this.productType, this.contentImagePath, this.titleImagepath,
                  userId, this.productStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/product']);

                  })
              })
            })
          })
        })







      } else if (this.productType === 'pdf') {


        if (this.productFileName == undefined || this.productFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._productService.uploadFile_title(this.productTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._productService.uploadFile(this.productFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._productService.uploadFile_banner(this.productBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._productService.uploadFile_mobile(this.productBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._productService.addproduct(this.productTitle, this.productType, this.pdfFilePath, this.titleImagepath,
                    userId, this.productStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/product']);

                    })
                })
              })
            })
          })

        }




      }

      else if (this.productType === 'vdo') {
        //==================== Title Image ================================
        this._productService.uploadFile_title(this.productTtitleImage).subscribe(response => {

          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._productService.uploadFile_banner(this.productBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();


            this._productService.uploadFile_mobile(this.productBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._productService.addproduct(this.productTitle, this.productType, this.vdo_id, this.titleImagepath,
                userId, this.productStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/product']);

                })
            })
          })
        })


      }

      this.ngxService.stop();

    }




  }

  Back() {
    this.router.navigate(['/back-office/product']);
  }

}
