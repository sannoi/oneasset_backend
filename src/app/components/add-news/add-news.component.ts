import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { HttpEventType, HttpResponse } from '@angular/common/http'
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';


@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.scss']
})



export class AddNewsComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  newsType: string;
  typenews: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  newsStatus: string;
  statusnews: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add news
  newsTitleTH: string
  newsTitleEN: string
  newsTtitleImage: any
  // newsTtitleImageMobile: any
  newsBannerImage: any
  imageTitleName: any
  newsContentImage: any
  newsContentImageMobile: any
  imageContentName: any
  newsFiles: any
  newsFileName: any

  // path response from server
  titleImagepath: any
  titleImagepathMobile: any
  BannerImagepath: any
  contentImagePath: any
  contentImagePathMobile: any
  pdfFilePath: any

  vdo_id: any

  width: any
  height: any

  newsBannerImage_mobile: any;
  BannerImagepath_mobile: any;

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _newsService: NewsService,
    private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }

  ngOnInit() {
    // TODO: Public Date
    flatpickr.localize(Thai);
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.newsType = 'text';
    this.newsStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setNewsType(code) {
    // console.log("choice: ", code);
    this.newsType = code;
  }

  setNewsTitleTH(title: string) {
    this.newsTitleTH = title
  }

  setNewsTitleEN(title: string) {
    this.newsTitleTH = title
  }

  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
    console.log(this.publicDate);
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setNewsStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.newsStatus = '1'
    } else if (code === 'Unpublish ') {
      this.newsStatus = '0'
    }
  }

  setNewsFiles(event) {
    if (event.target.id === 'newsTitleImage') {
      this.newsTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'newsContentmageMobile') {
      this.newsContentImageMobile = <File>event.target.files[0]
      this.contentImagePathMobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name
    } else if (event.target.id === 'newsContentImage') {
      this.newsContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'newsFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.newsFiles = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }


    }


    // else if (event.target.id === 'newsTitleImageMobile') {
    //   this.newsTtitleImageMobile = <File>event.target.files[0]
    //   this.titleImagepathMobile = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    // }
  }

  setNewsBannerFiles(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.newsBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setNewsBannerFiles_mobile(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.newsBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addNews() {
    let userId = sessionStorage.getItem('userId')
    if (this.newsTitleTH == undefined || this.newsTitleTH == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else if (this.newsTtitleImage == undefined || this.newsTtitleImage == "") {
      alert("กรุณาเลือก Title Image !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();

      if (this.newsType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._newsService.uploadFile_title(this.newsTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          console.log(response);
          //==================== Banner Image ================================
          this._newsService.uploadFile_banner(this.newsBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();
            console.log(response);

            this._newsService.uploadFile_mobile(this.newsBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();
              console.log(response);
              console.log(this.publicDate);
              // TODO: Public Date
              this._newsService.addNews(this.newsTitleTH, this.newsType,
                editorHtmlTag, '', this.titleImagepath,
                userId, this.newsStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  console.log("add text news response: ", response);
                  this.router.navigate(['/back-office/news']);
                })
            })
          })

        })

        // Title Image Mobile
        // this._newsService.uploadFile(this.newsTtitleImageMobile)

      }
      else if (this.newsType === 'image') {
        //==================== Title Image ================================
        this._newsService.uploadFile_title(this.newsTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          console.log(response);

          //==================== File Download ================================
          this._newsService.uploadFile(this.newsContentImage).subscribe(response => {
            this.contentImagePath = response.toString();
            console.log(response);

            //==================== Banner Image ================================
            this._newsService.uploadFile_banner(this.newsBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();
              console.log(response);

              this._newsService.uploadFile_mobile(this.newsBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();
                console.log(response);
                // TODO: Public Date
                this._newsService.addNews(this.newsTitleTH, this.newsType,
                  this.contentImagePath, this.contentImagePathMobile, this.titleImagepath,
                  userId, this.newsStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    console.log("add text news response: ", response);
                    this.router.navigate(['/back-office/news']);

                  })
              })
            })

          })


        })




      } else if (this.newsType === 'pdf') {

        if (this.newsFiles == undefined || this.newsFiles == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._newsService.uploadFile_title(this.newsTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._newsService.uploadFile(this.newsFiles).subscribe(response => {
              this.pdfFilePath = response.toString();
              console.log(response);

              //==================== Banner Image ================================
              this._newsService.uploadFile_banner(this.newsBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();
                console.log(response);


                this._newsService.uploadFile_mobile(this.newsBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();
                  console.log(response);
                  // TODO: Public Date
                  this._newsService.addNews(this.newsTitleTH, this.newsType,
                    this.pdfFilePath, '', this.titleImagepath,
                    userId, this.newsStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                      console.log("add text news response: ", response);
                      this.router.navigate(['/back-office/news']);

                    })
                })
              })
            })
          })



        }

        // Title Image Mobile
        // this._newsService.uploadFile(this.newsTtitleImageMobile)



      }

      else if (this.newsType === 'vdo') {
        //==================== Title Image ================================
        this._newsService.uploadFile_title(this.newsTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          console.log(response);

          //==================== Banner Image ================================
          this._newsService.uploadFile_banner(this.newsBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();
            console.log(response);

            this._newsService.uploadFile_mobile(this.newsBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();
              console.log(response);
              // TODO: Public Date
              this._newsService.addNews(this.newsTitleTH, this.newsType,
                this.vdo_id, '', this.titleImagepath,
                userId, this.newsStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  console.log("add text news response: ", response);
                  this.router.navigate(['/back-office/news']);

                })
            })
          })

        })



      }


    }

  }

  Back() {
    this.router.navigate(['/back-office/news']);
  }

}
