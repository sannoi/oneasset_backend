import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransactionService } from 'src/app/services/transaction.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.component.html',
  styleUrls: ['./add-transaction.component.scss']
})
export class AddTransactionComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  transactionType: string;
  typetransaction: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  transactionStatus: string;
  statustransaction: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];
  isTh: boolean
  isEn: boolean

  // for add transaction
  transactionTitle: string
  transactionTtitleImage: any
  transactionBannerImage: any
  imageTitleName: any
  transactionContentImage: any
  imageContentName: any
  transactionFiles: any
  transactionFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any


  transactionBannerImage_mobile: any;
  BannerImagepath_mobile: any;

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _transactionService: TransactionService, private router: Router,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
     // TODO: Public Date
     flatpickr.localize(Thai);
     this.public_date = new Date().toISOString().slice(0,10);
     this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.transactionType = 'text';
    this.transactionStatus = '1';
    this.BannerImagepath = '';

    this.isTh = true
    this.isEn = false
  }


  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setTransactionType(code) {
    // console.log("choice: ", code);
    this.transactionType = code;
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  setTransactionTitle(title: string) {
    this.transactionTitle = title
  }

  setTransactionStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.transactionStatus = '1'
    } else if (code === 'Unpublish ') {
      this.transactionStatus = '0'
    }
  }

  setTransactionFiles(event) {
    if (event.target.id === 'TransactionTitleImage') {
      this.transactionTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'TransactionContentImage') {
      this.transactionContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'TransactionFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.transactionFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }


    }
  }

  setTransactionBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.transactionBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setTransactionBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.transactionBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }


  addTransaction() {
    let userId = sessionStorage.getItem('userId')
    if (this.transactionTitle == undefined || this.transactionTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();

      if (this.transactionType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._transactionService.uploadFile_title(this.transactionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._transactionService.uploadFile_banner(this.transactionBannerImage).subscribe(response => {


            this._transactionService.uploadFile_mobile(this.transactionBannerImage_mobile).subscribe(response => {

              this._transactionService.addtransaction(this.transactionTitle, this.transactionType, editorHtmlTag,
                this.titleImagepath, userId,
                this.transactionStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/transaction']);
                })
            })
          })
        })




      }
      else if (this.transactionType === 'image') {
        //==================== Title Image ================================
        this._transactionService.uploadFile_title(this.transactionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._transactionService.uploadFile(this.transactionContentImage).subscribe(response => {

            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._transactionService.uploadFile_banner(this.transactionBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();


              this._transactionService.uploadFile_mobile(this.transactionBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._transactionService.addtransaction(this.transactionTitle, this.transactionType, this.contentImagePath,
                  this.titleImagepath,
                  userId, this.transactionStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/transaction']);

                  })
              })
            })
          })

        })







      } else if (this.transactionType === 'pdf') {

        if (this.transactionFileName == undefined || this.transactionFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {


          //==================== Title Image ================================
          this._transactionService.uploadFile_title(this.transactionTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._transactionService.uploadFile(this.transactionFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._transactionService.uploadFile_banner(this.transactionBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._transactionService.uploadFile_mobile(this.transactionBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._transactionService.addtransaction(this.transactionTitle, this.transactionType, this.pdfFilePath, this.titleImagepath,
                    userId, this.transactionStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/transaction']);
                    })

                })
              })
            })
          })


        }





      }

      else if (this.transactionType === 'vdo') {
        //==================== Title Image ================================
        this._transactionService.uploadFile_title(this.transactionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._transactionService.uploadFile_banner(this.transactionBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._transactionService.uploadFile_mobile(this.transactionBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._transactionService.addtransaction(this.transactionTitle, this.transactionType, this.vdo_id,
                this.titleImagepath,
                userId, this.transactionStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/transaction']);

                })
            })
          })

        })





      }

      this.ngxService.stop();



    }



  }

  Back() {
    this.router.navigate(['/back-office/transaction']);
  }

}
