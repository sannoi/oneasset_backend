import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-maintain',
  templateUrl: './maintain.component.html',
  styleUrls: ['./maintain.component.scss']
})
export class MaintainComponent implements OnInit {

  item: any

  inflation: any
  savings: any
  // expected return
  exptRlvl1: any
  exptRlvl2: any
  exptRlvl3: any
  exptRlvl4: any
  exptRlvl5: any

  // sd
  sdlvl1: any
  sdlvl2: any
  sdlvl3: any
  sdlvl4: any
  sdlvl5: any

  // drawndown
  ddlvl1: any
  ddlvl2: any
  ddlvl3: any
  ddlvl4: any
  ddlvl5: any

  maintainForm: FormGroup

  constructor(private router: Router, private ngxService: NgxUiLoaderService, private fb: FormBuilder,
    private maintainService: DocumentService) { }

  ngOnInit() {
    
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    
    let ress = sessionStorage.getItem('m10')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this.ngxService.start()
    this.maintainService.getMaintain('Inflation').subscribe(response => {
      this.inflation = response['r1'];

      this.maintainService.getMaintain('Provident').subscribe(response => {
        this.savings = response['r1']

        this.maintainService.getMaintain('Expected').subscribe(response => {
          this.exptRlvl1 = response['r1']
          this.exptRlvl2 = response['r2']
          this.exptRlvl3 = response['r3']
          this.exptRlvl4 = response['r4']
          this.exptRlvl5 = response['r5']

          this.maintainService.getMaintain('SD').subscribe(response => {
            this.sdlvl1 = response['r1']
            this.sdlvl2 = response['r2']
            this.sdlvl3 = response['r3']
            this.sdlvl4 = response['r4']
            this.sdlvl5 = response['r5']

            this.maintainService.getMaintain('Draw Down').subscribe(response => {
              this.ddlvl1 = response['r1']
              this.ddlvl2 = response['r2']
              this.ddlvl3 = response['r3']
              this.ddlvl4 = response['r4']
              this.ddlvl5 = response['r5']

              // binding info
              this.maintainForm = this.fb.group({
                inflationCtrl: this.inflation,
                savingsCtrl: this.savings,

                exptlvl1Ctrl: this.exptRlvl1,
                exptlvl2Ctrl: this.exptRlvl2,
                exptlvl3Ctrl: this.exptRlvl3,
                exptlvl4Ctrl: this.exptRlvl4,
                exptlvl5Ctrl: this.exptRlvl5,

                sdlvl1Ctrl: this.sdlvl1,
                sdlvl2Ctrl: this.sdlvl2,
                sdlvl3Ctrl: this.sdlvl3,
                sdlvl4Ctrl: this.sdlvl4,
                sdlvl5Ctrl: this.sdlvl5,

                ddlvl1Ctrl: this.ddlvl1,
                ddlvl2Ctrl: this.ddlvl2,
                ddlvl3Ctrl: this.ddlvl3,
                ddlvl4Ctrl: this.ddlvl4,
                ddlvl5Ctrl: this.ddlvl5,
              })
              // binding info
              this.item = response
              this.ngxService.stop()
            })
          })
        })
      })
    })
  }

  setInflation(inpInflation) {
    this.inflation = inpInflation
  }

  setSavings(inpSavings) {
    this.savings = inpSavings
  }

  // set expected return
  setExptLvl1(inputlvl1) {
    this.exptRlvl1 = inputlvl1
  }

  setExptLvl2(inputlvl2) {
    this.exptRlvl2 = inputlvl2
  }

  setExptLvl3(inputlvl3) {
    this.exptRlvl3 = inputlvl3
  }

  setExptLvl4(inputlvl4) {
    this.exptRlvl4 = inputlvl4
  }

  setExptLvl5(inputlvl5) {
    this.exptRlvl5 = inputlvl5
  }

  // set SD
  setSDLvl1(inputlvl1) {
    this.sdlvl1 = inputlvl1
  }

  setSDLvl2(inputlvl2) {
    this.sdlvl2 = inputlvl2
  }

  setSDLvl3(inputlvl3) {
    this.sdlvl3 = inputlvl3
  }

  setSDLvl4(inputlvl4) {
    this.sdlvl4 = inputlvl4
  }

  setSDLvl5(inputlvl5) {
    this.sdlvl5 = inputlvl5
  }

  // set drawdown
  setDDLvl1(inputlvl1) {
    this.ddlvl1 = inputlvl1
  }

  setDDLvl2(inputlvl2) {
    this.ddlvl2 = inputlvl2
  }

  setDDLvl3(inputlvl3) {
    this.ddlvl3 = inputlvl3
  }

  setDDLvl4(inputlvl4) {
    this.ddlvl4 = inputlvl4
  }

  setDDLvl5(inputlvl5) {
    this.ddlvl5 = inputlvl5
  }

  save() {
    this.ngxService.start()
    this.maintainService.UpdateMaintain('Inflation', this.inflation, '', '', '', '').subscribe(response => {

      this.maintainService.UpdateMaintain('Provident', this.savings, '', '', '', '').subscribe(response => {

        this.maintainService.UpdateMaintain('Expected', this.exptRlvl1, this.exptRlvl2,
          this.exptRlvl3, this.exptRlvl4, this.exptRlvl5).subscribe(response => {

            this.maintainService.UpdateMaintain('SD', this.sdlvl1, this.sdlvl2,
              this.sdlvl3, this.sdlvl4, this.sdlvl5).subscribe(response => {

                this.maintainService.UpdateMaintain('Draw Down', this.ddlvl1, this.ddlvl2,
                  this.ddlvl3, this.ddlvl4, this.ddlvl5).subscribe(response => {
                    this.router.navigate(['/back-office/maintain'])
                    this.ngxService.stop()
                  })
              })
          })
      })
    })
  }

}
