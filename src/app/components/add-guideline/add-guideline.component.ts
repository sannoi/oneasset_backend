import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GuidelineService } from 'src/app/services/guideline.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-guideline',
  templateUrl: './add-guideline.component.html',
  styleUrls: ['./add-guideline.component.scss']
})
export class AddGuidelineComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  guidelineType: string;
  typeguideline: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  guidelineStatus: string;
  statusguideline: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add guideline
  guidelineTitle: string
  guidelineTitleEN: string
  guidelineTtitleImage: any
  guidelineBannerImage: any
  imageTitleName: any
  guidelineContentImage: any
  imageContentName: any
  guidelineFiles: any
  guidelineFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  guidelineBannerImage_mobile: any
  BannerImagepath_mobile: any
  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _guidelineService: GuidelineService, private router: Router,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
     // TODO: Public Date
     flatpickr.localize(Thai);
     this.public_date = new Date().toISOString().slice(0,10);
     this.publicDate = this.public_date;
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.guidelineType = 'text';
    this.guidelineStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }

  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setGuidelineType(code) {
    // console.log("choice: ", code);
    this.guidelineType = code;
  }

  setGuidelineTitle(title: string) {
    this.guidelineTitle = title
  }

  setGuidelineTitleEN(title: string) {
    this.guidelineTitleEN = title
  }

  setGuidelineStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.guidelineStatus = '1'
    } else if (code === 'Unpublish ') {
      this.guidelineStatus = '0'
    }
  }

  setGuidelineFiles(event) {
    if (event.target.id === 'GuidelineTitleImage') {
      this.guidelineTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'GuidelineContentImage') {
      this.guidelineContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'GuidelineFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.guidelineFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }

    }
  }

  setGuidelineBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.guidelineBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setGuidelineBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.guidelineBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addGuideline() {
    let userId = sessionStorage.getItem('userId')

    if (this.guidelineTitle == undefined || this.guidelineTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {
      this.ngxService.start();

      if (this.guidelineType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._guidelineService.uploadFile_title(this.guidelineTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._guidelineService.uploadFile_banner(this.guidelineBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._guidelineService.uploadFile_mobile(this.guidelineBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._guidelineService.addguideline(this.guidelineTitle, this.guidelineType, editorHtmlTag, this.titleImagepath, userId,
                this.guidelineStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/guideline']);
                })

            })
          })
        })





      }
      else if (this.guidelineType === 'image') {
        //==================== Title Image ================================
        this._guidelineService.uploadFile_title(this.guidelineTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._guidelineService.uploadFile(this.guidelineContentImage).subscribe(response => {
            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._guidelineService.uploadFile_banner(this.guidelineBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();


              this._guidelineService.uploadFile_mobile(this.guidelineBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._guidelineService.addguideline(this.guidelineTitle, this.guidelineType, this.contentImagePath, this.titleImagepath,
                  userId, this.guidelineStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/guideline']);

                  })
              })
            })
          })
        })







      } else if (this.guidelineType === 'pdf') {

        if (this.guidelineFileName == undefined || this.guidelineFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._guidelineService.uploadFile_title(this.guidelineTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._guidelineService.uploadFile(this.guidelineFileName).subscribe(response => {
              this.pdfFilePath = response.toString();


              //==================== Banner Image ================================
              this._guidelineService.uploadFile_banner(this.guidelineBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();


                this._guidelineService.uploadFile_mobile(this.guidelineBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._guidelineService.addguideline(this.guidelineTitle, this.guidelineType, this.pdfFilePath, this.titleImagepath,
                    userId, this.guidelineStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/guideline']);

                    })
                })
              })
            })

          })


        }





      }
      else if (this.guidelineType === 'vdo') {
        //==================== Title Image ================================
        this._guidelineService.uploadFile_title(this.guidelineTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._guidelineService.uploadFile_banner(this.guidelineBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._guidelineService.uploadFile_mobile(this.guidelineBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._guidelineService.addguideline(this.guidelineTitle, this.guidelineType, this.vdo_id, this.titleImagepath,
                userId, this.guidelineStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/guideline']);

                })
            })

          })
        })



      }
      this.ngxService.stop();

    }


  }

  Back() {
    this.router.navigate(['/back-office/guideline']);
  }

}
