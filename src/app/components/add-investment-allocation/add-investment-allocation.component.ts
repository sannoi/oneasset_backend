import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-investment-allocation',
  templateUrl: './add-investment-allocation.component.html',
  styleUrls: ['./add-investment-allocation.component.scss']
})
export class AddInvestmentAllocationComponent implements OnInit {

  // Equity Investment Allocation
  eqlvl1: any
  eqlvl2: any
  eqlvl3: any
  eqlvl4: any
  eqlvl5: any

  // Money Market Investment Allocation
  mmlvl1: any
  mmlvl2: any
  mmlvl3: any
  mmlvl4: any
  mmlvl5: any

  // Fixed Income Investment Allocation
  filvl1: any
  filvl2: any
  filvl3: any
  filvl4: any
  filvl5: any

  // Commodity Investment Allocation
  commlvl1: any
  commlvl2: any
  commlvl3: any
  commlvl4: any
  commlvl5: any

  // Property Investment Allocation
  proplvl1: any
  proplvl2: any
  proplvl3: any
  proplvl4: any
  proplvl5: any

  constructor(private router: Router) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m10')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
  }

  addInvestAllo() {
    this.router.navigate(['/back-office/maintain'])
  }

  Back() {
    this.router.navigate(['/back-office/maintain'])
  }

}
