import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-user-group',
  templateUrl: './edit-user-group.component.html',
  styleUrls: ['./edit-user-group.component.scss']
})
export class EditUserGroupComponent implements OnInit {
  private sub: any;
  id: string;
  data: any[] = [
    { val: 'y', name: 'Active ' },
    { val: 'n', name: 'Not Active  ' },
  ];

  resGroupStatus: any
  resGroupname: any
  groupName: any
  selectedStatus: any
  editUserGroupForm: FormGroup

  constructor(private router: Router, private ngxService: NgxUiLoaderService,
    private userService: UserService, private fb: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m11')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this.ngxService.start()
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.userService.getUserGroupById(this.id).subscribe(response => {
      this.resGroupname = response[0]['GroupName']
      this.resGroupStatus = response[0]['IsActive']
      //=============== Binding Info ==================//
      this.editUserGroupForm = this.fb.group({
        GroupnameControl: this.resGroupname,
        GroupStatusTypeControl: this.resGroupStatus,

      });
      //==============================================//
      this.ngxService.stop()
    })

  }

  setGroupName(groupName) {
    this.groupName = groupName
  }

  setGroupStatus(status) {
    this.selectedStatus = status
  }

  editUserGroup() {
    this.ngxService.start()
    if (this.selectedStatus === undefined && this.groupName === undefined) {
      this.userService.updateUserGroup(this.resGroupname, this.resGroupStatus, this.id).subscribe(response => {
        this.router.navigate(['/back-office/user-group'])
        this.ngxService.stop()
      })
    }
    if (this.selectedStatus === undefined && this.groupName !== undefined) {
      this.userService.updateUserGroup(this.groupName, this.resGroupStatus, this.id).subscribe(response => {
        this.router.navigate(['/back-office/user-group'])
        this.ngxService.stop()
      })
    }
    if (this.selectedStatus !== undefined && this.groupName === undefined) {
      this.userService.updateUserGroup(this.resGroupname, this.selectedStatus, this.id).subscribe(response => {
        this.router.navigate(['/back-office/user-group'])
        this.ngxService.stop()
      })
    }
    if (this.selectedStatus !== undefined && this.groupName !== undefined) {
      this.userService.updateUserGroup(this.groupName, this.selectedStatus, this.id).subscribe(response => {
        this.router.navigate(['/back-office/user-group'])
        this.ngxService.stop()
      })
    }
  }

  Back() {
    this.router.navigate(['/back-office/user-group'])
  }

}
