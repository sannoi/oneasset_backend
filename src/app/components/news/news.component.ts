import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NewsService } from '../../services/news.service';
import { PagerService } from 'src/app/_services';
import { NgxUiLoaderService } from 'ngx-ui-loader';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  // news status
  selectedStatus: string;

  //all news
  news: any
  newsDate: any

  // searchFunction
  topicSearch: string

  constructor(private router: Router, private _newsService: NewsService, private pagerService: PagerService,
    private ngxService: NgxUiLoaderService) { }

  selectedLevel: any;

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];


  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================



  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._newsService.getNews(1, "").subscribe(response => {

      console.log(response);

      this.news = response
      this.allItems = response
      this.setPage(1);
      this.selectedLevel = 1
      this.ngxService.stop();
    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }



  getNewsFromSerch() {
    this.ngxService.start();

    this._newsService.getNews(this.selectedLevel, this.topicSearch).subscribe(response => {
      this.news = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  goAddNews() {
    this.router.navigate(['add']);
  }
  goEditNews() {
    this.router.navigate(['edit']);
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.newsDate = this._newsService.changeFormatDate(date)
    }
  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.newsDate = this._newsService.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.newsDate = this._newsService.date_convert(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/news/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickAdd() {
    event.preventDefault()
    this.router.navigate(['/back-office/news/add'])

  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();

      this._newsService.getNews(this.selectedLevel, this.topicSearch).subscribe(response => {
        this.news = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }


}
