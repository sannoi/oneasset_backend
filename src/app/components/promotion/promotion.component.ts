import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PromotionService } from 'src/app/services/promotion.service';
import { PagerService } from 'src/app/_services';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss']
})
export class PromotionComponent implements OnInit {

  constructor(private router: Router, private _promotionService: PromotionService,
    private pagerService: PagerService, private ngxService: NgxUiLoaderService) { }

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];

  promotionDate: any
  promotion: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m2')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._promotionService.getpromotion(1, "").subscribe(response => {
      this.promotion = response
      console.log(this.promotion);
      this.allItems = response
      this.setPage(1);
      this.selectedLevel = 1
      this.ngxService.stop();
    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();

      this._promotionService.getpromotion(this.selectedLevel, this.topicSearch).subscribe(response => {
        this.promotion = response

        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  getpromotionFromSerch() {
    this.ngxService.start();
    this._promotionService.getpromotion(this.selectedLevel, this.topicSearch).subscribe(response => {
      this.promotion = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.promotionDate = this._promotionService.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/promotion/edit'], { queryParams: { id: xxx } });
  }

  clickAddpromotion() {
    event.preventDefault()
    this.router.navigate(['/back-office/promotion/add'])

  }


  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.promotionDate = this._promotionService.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.promotionDate = this._promotionService.date_convert(date)
    }
  }



}
