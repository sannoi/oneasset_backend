import { Component, OnInit } from '@angular/core';
import { DocumentService } from 'src/app/services/document.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-investment-allocation',
  templateUrl: './investment-allocation.component.html',
  styleUrls: ['./investment-allocation.component.scss']
})
export class InvestmentAllocationComponent implements OnInit {

  menuName: any

  investAllo: any
  resinvestAllo: any
  fundType = []

  group1Id: any
  group2Id: any
  group3Id: any
  group4Id: any
  group5Id: any

  typeId1: any
  typeId2: any
  typeId3: any
  typeId4: any
  typeId5: any

  type1: any
  type2: any
  type3: any
  type4: any
  type5: any

  group1lvl1: any
  group1lvl2: any
  group1lvl3: any
  group1lvl4: any
  group1lvl5: any

  group2lvl1: any
  group2lvl2: any
  group2lvl3: any
  group2lvl4: any
  group2lvl5: any

  group3lvl1: any
  group3lvl2: any
  group3lvl3: any
  group3lvl4: any
  group3lvl5: any

  group4lvl1: any
  group4lvl2: any
  group4lvl3: any
  group4lvl4: any
  group4lvl5: any

  group5lvl1: any
  group5lvl2: any
  group5lvl3: any
  group5lvl4: any
  group5lvl5: any

  editInvestAlloForm: FormGroup

  choiceFromSelect: any;

  // group 1 mapp
  fundName1: any
  fundNotMapp1: any
  fundMapp1: any

  // group 2 mapp
  fundName2: any
  fundNotMapp2: any
  fundMapp2: any

  // group 3 mapp
  fundName3: any
  fundNotMapp3: any
  fundMapp3: any

  // group 4 mapp
  fundName4: any
  fundNotMapp4: any
  fundMapp4: any

  // group 5 mapp
  fundName5: any
  fundNotMapp5: any
  fundMapp5: any

  fundSelectList: Array<any> = []
  delSelectList: Array<any> = []

  constructor(private investAlloService: DocumentService, private ngxService: NgxUiLoaderService,
    private fb: FormBuilder, private router: Router) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m10')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this.menuName = 'Investment plan Maintain'
    this.choiceFromSelect = "display-form";
    this.ngxService.start()
    this.investAlloService.getMaintainCalFundType().subscribe(response => {

      console.log(response);
      this.resinvestAllo = response

      this.group1Id = response[0].id
      this.group2Id = response[1].id
      this.group3Id = response[2].id
      this.group4Id = response[3].id
      this.group5Id = response[4].id

      this.type1 = response[0].FundType
      this.type2 = response[1].FundType
      this.type3 = response[2].FundType
      this.type4 = response[3].FundType
      this.type5 = response[4].FundType

      this.typeId1 = response[0].FundTypeId
      this.typeId2 = response[1].FundTypeId
      this.typeId3 = response[2].FundTypeId
      this.typeId4 = response[3].FundTypeId
      this.typeId5 = response[4].FundTypeId

      this.group1lvl1 = response[0].r1
      this.group1lvl2 = response[0].r2
      this.group1lvl3 = response[0].r3
      this.group1lvl4 = response[0].r4
      this.group1lvl5 = response[0].r5

      this.group2lvl1 = response[1].r1
      this.group2lvl2 = response[1].r2
      this.group2lvl3 = response[1].r3
      this.group2lvl4 = response[1].r4
      this.group2lvl5 = response[1].r5

      this.group3lvl1 = response[2].r1
      this.group3lvl2 = response[2].r2
      this.group3lvl3 = response[2].r3
      this.group3lvl4 = response[2].r4
      this.group3lvl5 = response[2].r5

      this.group4lvl1 = response[3].r1
      this.group4lvl2 = response[3].r2
      this.group4lvl3 = response[3].r3
      this.group4lvl4 = response[3].r4
      this.group4lvl5 = response[3].r5

      this.group5lvl1 = response[4].r1
      this.group5lvl2 = response[4].r2
      this.group5lvl3 = response[4].r3
      this.group5lvl4 = response[4].r4
      this.group5lvl5 = response[4].r5

      this.resinvestAllo.forEach(element => {
        this.fundType.push({
          typeId: element.FundTypeId,
          fundType: element.FundType
        })
      });

      this.editInvestAlloForm = this.fb.group({
        slot1TypeCtrl: this.typeId1,
        slot2TypeCtrl: this.typeId2,
        slot3TypeCtrl: this.typeId3,
        slot4TypeCtrl: this.typeId4,
        slot5TypeCtrl: this.typeId5,

        group1lvl1Ctrl: this.group1lvl1,
        group1lvl2Ctrl: this.group1lvl2,
        group1lvl3Ctrl: this.group1lvl3,
        group1lvl4Ctrl: this.group1lvl4,
        group1lvl5Ctrl: this.group1lvl5,

        group2lvl1Ctrl: this.group2lvl1,
        group2lvl2Ctrl: this.group2lvl2,
        group2lvl3Ctrl: this.group2lvl3,
        group2lvl4Ctrl: this.group2lvl4,
        group2lvl5Ctrl: this.group2lvl5,

        group3lvl1Ctrl: this.group3lvl1,
        group3lvl2Ctrl: this.group3lvl2,
        group3lvl3Ctrl: this.group3lvl3,
        group3lvl4Ctrl: this.group3lvl4,
        group3lvl5Ctrl: this.group3lvl5,

        group4lvl1Ctrl: this.group4lvl1,
        group4lvl2Ctrl: this.group4lvl2,
        group4lvl3Ctrl: this.group4lvl3,
        group4lvl4Ctrl: this.group4lvl4,
        group4lvl5Ctrl: this.group4lvl5,

        group5lvl1Ctrl: this.group5lvl1,
        group5lvl2Ctrl: this.group5lvl2,
        group5lvl3Ctrl: this.group5lvl3,
        group5lvl4Ctrl: this.group5lvl4,
        group5lvl5Ctrl: this.group5lvl5,
      })
      this.investAllo = response
      this.ngxService.stop()

    })
  }

  // group 3
  setgroup1lvl1(input) {
    this.group1lvl1 = input
  }

  setgroup1lvl2(input) {
    this.group1lvl2 = input
  }

  setgroup1lvl3(input) {
    this.group1lvl3 = input
  }

  setgroup1lvl4(input) {
    this.group1lvl4 = input
  }

  setgroup1lvl5(input) {
    this.group1lvl5 = input
  }

  // group 2 
  setgroup2lvl1(input) {
    this.group2lvl1 = input
  }
  setgroup2lvl2(input) {
    this.group2lvl2 = input
  }
  setgroup2lvl3(input) {
    this.group2lvl3 = input
  }
  setgroup2lvl4(input) {
    this.group2lvl4 = input
  }
  setgroup2lvl5(input) {
    this.group2lvl5 = input
  }

  // group 3 
  setgroup3lvl1(input) {
    this.group3lvl1 = input
  }
  setgroup3lvl2(input) {
    this.group3lvl2 = input
  }
  setgroup3lvl3(input) {
    this.group3lvl3 = input
  }
  setgroup3lvl4(input) {
    this.group3lvl4 = input
  }
  setgroup3lvl5(input) {
    this.group3lvl5 = input
  }

  // group 4 
  setgroup4lvl1(input) {
    this.group4lvl1 = input
  }
  setgroup4lvl2(input) {
    this.group4lvl2 = input
  }
  setgroup4lvl3(input) {
    this.group4lvl3 = input
  }
  setgroup4lvl4(input) {
    this.group4lvl4 = input
  }
  setgroup4lvl5(input) {
    this.group4lvl5 = input
  }

  // group 5 
  setgroup5lvl1(input) {
    this.group5lvl1 = input
  }
  setgroup5lvl2(input) {
    this.group5lvl2 = input
  }
  setgroup5lvl3(input) {
    this.group5lvl3 = input
  }
  setgroup5lvl4(input) {
    this.group5lvl4 = input
  }
  setgroup5lvl5(input) {
    this.group5lvl5 = input
  }

  setGroup1TypeId(input) {
    this.group1Id = input
  }

  setGroup2TypeId(input) {
    this.group2Id = input
  }

  setGroup3TypeId(input) {
    this.group3Id = input
  }

  setGroup4TypeId(input) {
    this.group4Id = input
  }

  setGroup5TypeId(input) {
    this.group5Id = input
  }

  // mapp group 1
  addP1() {
    this.ngxService.start();
    this.menuName = this.type1 + ' Mapping'
    this.choiceFromSelect = "display-p1";
    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId1, '').subscribe(response => {
      this.fundNotMapp1 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId1).subscribe(response => {
      this.fundMapp1 = response;
    })
    this.ngxService.stop();
  }

  clickbackP1() {
    this.menuName = 'Investment plan Maintain'
    this.choiceFromSelect = "display-form";
  }

  setFundName1(name) {
    this.fundName1 = name;
  }

  clickSearchp1() {

    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId1, this.fundName1).subscribe(response => {
      this.fundNotMapp1 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId1).subscribe(response => {
      this.fundMapp1 = response;
    })
  }

  onSaveUsernameChanged1(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }

  onDeleteUsernameChanged1(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
    }
    else {

      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectP1() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this.investAlloService.addMaintainCalFundType(this.typeId1, this.fundSelectList[i]).subscribe(response => {
          this.investAlloService.getMaintainCalFundTypeMapped(this.typeId1).subscribe(response => {
            this.fundMapp1 = response;
            this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId1, this.fundName1).subscribe(response => {
              this.fundNotMapp1 = response;
            })

          })
        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();

    } else {
      this.ngxService.stop();
    }
  }

  delP1() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {

        this.investAlloService.deleteMainTainCalFundType(this.delSelectList[i]).subscribe(response => {
          this.investAlloService.getMaintainCalFundTypeMapped(this.typeId1).subscribe(response => {
            this.fundMapp1 = response;
            this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId1, this.fundName1).subscribe(response => {
              this.fundNotMapp1 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();
    } else {
      this.ngxService.stop();
    }
  }

  // mapp group 2 
  addP2() {
    this.ngxService.start();
    this.menuName = this.type2 + ' Mapping'
    this.choiceFromSelect = "display-p2";

    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId2, '').subscribe(response => {
      this.fundNotMapp2 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId2).subscribe(response => {
      this.fundMapp2 = response;
    })
    this.ngxService.stop();

  }

  clickbackp2() {
    this.menuName = 'Investment plan Maintain'
    this.choiceFromSelect = "display-form";
  }
  setFundName2(name) {
    this.fundName2 = name;
  }
  clickSearchp2() {
    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId2, this.fundName2).subscribe(response => {
      this.fundNotMapp2 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId2).subscribe(response => {
      this.fundMapp2 = response;
    })
  }
  onSaveUsernameChanged2(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }

  onDeleteUsernameChanged2(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectp2() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this.investAlloService.addMaintainCalFundType(this.typeId2, this.fundSelectList[i]).subscribe(response => {

          this.investAlloService.getMaintainCalFundTypeMapped(this.typeId2).subscribe(response => {
            this.fundMapp2 = response;

            this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId2, this.fundName2).subscribe(response => {
              this.fundNotMapp2 = response;
            })
          })


        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();

    } else {
      this.ngxService.stop();
    }
  }

  delp2() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this.investAlloService.deleteMainTainCalFundType(this.delSelectList[i]).subscribe(response => {
          this.investAlloService.getMaintainCalFundTypeMapped(this.typeId2).subscribe(response => {
            this.fundMapp2 = response;

            this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId2, this.fundName2).subscribe(response => {
              this.fundNotMapp2 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();
    } else {
      this.ngxService.stop();
    }
  }

  // mapp group 3 
  addp3() {
    this.ngxService.start();
    this.menuName = this.type3 + ' Mapping'
    this.choiceFromSelect = "display-p3";

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId3).subscribe(response => {
      this.fundMapp3 = response;
    })

    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId3, '').subscribe(response => {
      this.fundNotMapp3 = response;
    })
    this.ngxService.stop();
  }

  clickbackp3() {
    this.menuName = 'Investment plan Maintain'
    this.choiceFromSelect = "display-form";
  }
  setFundName3(name) {
    this.fundName3 = name;
  }
  clickSearchp3() {
    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId3, this.fundName3).subscribe(response => {
      this.fundNotMapp3 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId3).subscribe(response => {
      this.fundMapp3 = response;
    })
  }
  onSaveUsernameChanged3(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }
  onDeleteUsernameChanged3(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }
  selectp3() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this.investAlloService.addMaintainCalFundType(this.typeId3, this.fundSelectList[i]).subscribe(response => {

          this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId3, this.fundName3).subscribe(response => {
            this.fundNotMapp3 = response;

            this.investAlloService.getMaintainCalFundTypeMapped(this.typeId3).subscribe(response => {
              this.fundMapp3 = response;
            })

          })
        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();
    } else {
      this.ngxService.stop();
    }
  }

  delp3() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this.investAlloService.deleteMainTainCalFundType(this.delSelectList[i]).subscribe(response => {
          this.investAlloService.getMaintainCalFundTypeMapped(this.typeId3).subscribe(response => {
            this.fundMapp3 = response;

            this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId3, this.fundName3).subscribe(response => {
              this.fundNotMapp3 = response;
            })

          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    } else {
      this.ngxService.stop();
    }
  }

  // mapp group 4
  addp4() {
    this.ngxService.start();
    this.menuName = this.type4 + ' Mapping'
    this.choiceFromSelect = "display-p4";

    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId4, '').subscribe(response => {
      this.fundNotMapp4 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId4).subscribe(response => {
      this.fundMapp4 = response;
    })
    this.ngxService.stop();
  }

  clickbackp4() {
    this.menuName = 'Investment plan Maintain'
    this.choiceFromSelect = "display-form";
  }
  setFundName4(name) {
    this.fundName4 = name;
  }
  clickSearchp4() {
    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId4, this.fundName4).subscribe(response => {
      this.fundNotMapp4 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId4).subscribe(response => {
      this.fundMapp4 = response;
    })
  }
  onSaveUsernameChanged4(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }
  onDeleteUsernameChanged4(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectp4() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this.investAlloService.addMaintainCalFundType(this.typeId4, this.fundSelectList[i]).subscribe(response => {

          this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId4, this.fundName4).subscribe(response => {
            this.fundNotMapp4 = response;

            this.investAlloService.getMaintainCalFundTypeMapped(this.typeId4).subscribe(response => {
              this.fundMapp4 = response;
            })

          })

        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();
    } else {
      this.ngxService.stop();
    }
  }

  delp4() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this.investAlloService.deleteMainTainCalFundType(this.delSelectList[i]).subscribe(response => {
          this.investAlloService.getMaintainCalFundTypeMapped(this.typeId4).subscribe(response => {
            this.fundMapp4 = response;
            this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId4, this.fundName4).subscribe(response => {
              this.fundNotMapp4 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    } else {
      this.ngxService.stop();
    }
  }

  // mapp group 5
  addp5() {
    this.ngxService.start();
    this.menuName = this.type5 + ' Mapping'
    this.choiceFromSelect = "display-p5";

    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId5, '').subscribe(response => {
      this.fundNotMapp5 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId5).subscribe(response => {
      this.fundMapp5 = response;
    })
    this.ngxService.stop();
  }


  clickbackp5() {
    this.menuName = 'Investment plan Maintain'
    this.choiceFromSelect = "display-form";
  }
  setFundName5(name) {
    this.fundName5 = name;
  }
  clickSearchp5() {
    this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId5, this.fundName5).subscribe(response => {
      this.fundNotMapp5 = response;
    })

    this.investAlloService.getMaintainCalFundTypeMapped(this.typeId5).subscribe(response => {
      this.fundMapp5 = response;
    })
  }
  onSaveUsernameChanged5(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }
  onDeleteUsernameChanged5(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectp5() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this.investAlloService.addMaintainCalFundType(this.typeId5, this.fundSelectList[i]).subscribe(response => {

          this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId5, this.fundName5).subscribe(response => {
            this.fundNotMapp5 = response;
          })
          this.investAlloService.getMaintainCalFundTypeMapped(this.typeId5).subscribe(response => {
            this.fundMapp5 = response;
          })

        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();

    } else {
      this.ngxService.stop();
    }
  }

  delp5() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this.investAlloService.deleteMainTainCalFundType(this.delSelectList[i]).subscribe(response => {
          this.investAlloService.getMaintainCalFundTypeMapped(this.typeId5).subscribe(response => {
            this.fundMapp5 = response;
            this.investAlloService.getMaintainCalFundTypeNotMapping(this.typeId5, this.fundName5).subscribe(response => {
              this.fundNotMapp5 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    } else {
      this.ngxService.stop();
    }
  }

  save() {
    this.ngxService.start()
    this.investAlloService.updateMaintainCalFundType(this.typeId1, this.group1lvl1, this.group1lvl2,
      this.group1lvl3, this.group1lvl4, this.group1lvl5, this.group1Id).subscribe(response => {

        this.investAlloService.updateMaintainCalFundType(this.typeId2, this.group2lvl1, this.group2lvl2,
          this.group2lvl3, this.group2lvl4, this.group2lvl5, this.group2Id).subscribe(response => {

            this.investAlloService.updateMaintainCalFundType(this.typeId3, this.group3lvl1, this.group3lvl2,
              this.group3lvl3, this.group3lvl4, this.group3lvl5, this.group3Id).subscribe(response => {

                this.investAlloService.updateMaintainCalFundType(this.typeId4, this.group4lvl1, this.group4lvl2,
                  this.group4lvl3, this.group4lvl4, this.group4lvl5, this.group4Id).subscribe(response => {

                    this.investAlloService.updateMaintainCalFundType(this.typeId5, this.group5lvl1, this.group5lvl2,
                      this.group5lvl3, this.group5lvl4, this.group5lvl5, this.group5Id).subscribe(response => {

                        this.ngxService.stop()
                      })
                  })
              })
          })
      })
  }


}
