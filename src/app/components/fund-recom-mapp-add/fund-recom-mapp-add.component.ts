import { Component, OnInit } from '@angular/core';
import { FundRecomMappService } from 'src/app/services/fund-recom-mapp.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-fund-recom-mapp-add',
  templateUrl: './fund-recom-mapp-add.component.html',
  styleUrls: ['./fund-recom-mapp-add.component.scss']
})
export class FundRecomMappAddComponent implements OnInit {


  fundList: Array<any> = []
  fundList2: Array<any> = []

  constructor(private _Service: FundRecomMappService, private router: Router,
    private ngxService: NgxUiLoaderService) { }

  fund: any;
  fundname: any;
  fundNotMapp: any;
  fundMapp: any;

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m7')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this.ngxService.start();
    this._Service.get_fund_Not_mapp().subscribe(response => {
      this.fund = response;
    })

    this._Service.get_fundRecomMappList().subscribe(response => {
      this.fundMapp = response;
      this.ngxService.stop();
    })

  }

  clickSearch() {
    this.ngxService.start();
    this._Service.get_fundNotRecom_mappByName(this.fundname).subscribe(response => {
      this.fund = response;
    })

    this._Service.get_fundRecomMappList().subscribe(response => {
      this.fundMapp = response;
      this.ngxService.stop();
    })

  }

  onSaveUsernameChanged(value: boolean, x) {

    if (value == true) {
      this.fundList.push(x);
    }
    else {
      const index: number = this.fundList.indexOf(x);
      if (index !== -1) {
        this.fundList.splice(index, 1);
      }
    }

  }

  onSaveUsernameChanged2(value: boolean, x) {

    if (value == true) {
      this.fundList2.push(x);
    }
    else {
      const index: number = this.fundList2.indexOf(x);
      if (index !== -1) {
        this.fundList2.splice(index, 1);
      }
    }

  }

  setFundName(fundname) {
    this.fundname = fundname
  }

  select() {

    this.ngxService.start();

    if (this.fundList.length != 0) {

      var num: number = 5;
      var i: number;

      for (i = 0; i < this.fundList.length; i++) {
        this._Service.Update_FundRecomm(this.fundList[i]).subscribe(response => {
          this._Service.get_fund_Not_mapp().subscribe(response => {
            this.fund = response;
            this._Service.get_fundRecomMappList().subscribe(response => {
              this.fundMapp = response;
            })
          })
        })
      }

      this.ngxService.stop();






    }
  }

  select2() {

    this.ngxService.start();

    if (this.fundList2.length != 0) {

      var num: number = 5;
      var i: number;

      for (i = 0; i < this.fundList2.length; i++) {
        this._Service.Update_FundRecommStatus(this.fundList2[i], "n").subscribe(response => {
          this._Service.get_fund_Not_mapp().subscribe(response => {
            this.fund = response;
            this._Service.get_fundRecomMappList().subscribe(response => {
              this.fundMapp = response;
            })
          })
        })
      }

      this.ngxService.stop();







    }
  }

  clickback() {
    event.preventDefault()
    this.router.navigate(['/back-office/fundRecom_mapp/'])
  }

}
