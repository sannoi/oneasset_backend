import { Component, OnInit } from '@angular/core';
import { PromotionService } from 'src/app/services/promotion.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-promotion',
  templateUrl: './add-promotion.component.html',
  styleUrls: ['./add-promotion.component.scss']
})
export class AddPromotionComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  promotionType: string;
  typepromotion: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  promotionStatus: string;
  statuspromotion: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add promotion
  promotionTitle: string
  promotionTtitleImage: any
  promotionBannerImage: any
  imageTitleName: any
  promotionContentImage: any
  imageContentName: any
  promotionFiles: any
  promotionFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  promotionBannerImage_mobile: any;
  BannerImagepath_mobile: any;

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _promotionService: PromotionService, private router: Router,
    private ngxService: NgxUiLoaderService) { }


  ngOnInit() {
    // TODO: Public Date
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m2')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.isTh = true
    this.isEn = false
    this.promotionType = 'text';
    this.promotionStatus = '1';
    this.BannerImagepath = '';
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setpromotionType(code) {
    // console.log("choice: ", code);
    this.promotionType = code;
  }

  setpromotionTitle(title: string) {
    this.promotionTitle = title
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setpromotionStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.promotionStatus = '1'
    } else if (code === 'Unpublish ') {
      this.promotionStatus = '0'
    }
  }

  setpromotionFiles(event) {
    if (event.target.id === 'promotionTitleImage') {
      this.promotionTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'promotionContentImage') {
      this.promotionContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'promotionFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.promotionFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }


    }
  }

  setpromotionBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.promotionBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }


      };
    }
  }

  setpromotionBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.promotionBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }


  addpromotion() {
    let userId = sessionStorage.getItem('userId')
    if (this.promotionTitle == undefined || this.promotionTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else if (this.promotionTtitleImage == undefined || this.promotionTtitleImage == "") {
      alert("กรุณาเลือก Title Image !!");
      this.ngxService.stop();

    }
    else {
      this.ngxService.start();

      if (this.promotionType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._promotionService.uploadFile_title(this.promotionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._promotionService.uploadFile_banner(this.promotionBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._promotionService.uploadFile_mobile(this.promotionBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._promotionService.addpromotion(this.promotionTitle, this.promotionType, editorHtmlTag, this.titleImagepath, userId,
                this.promotionStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {

                  this.router.navigate(['/back-office/promotion']);
                })
            })
          })
        })




      }
      else if (this.promotionType === 'image') {
        //==================== Title Image ================================
        this._promotionService.uploadFile_title(this.promotionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._promotionService.uploadFile(this.promotionContentImage).subscribe(response => {

            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._promotionService.uploadFile_banner(this.promotionBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();


              this._promotionService.uploadFile_mobile(this.promotionBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._promotionService.addpromotion(this.promotionTitle, this.promotionType, this.contentImagePath, this.titleImagepath,
                  userId, this.promotionStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/promotion']);
                  })
              })
            })
          })
        })







      } else if (this.promotionType === 'pdf') {

        if (this.promotionFileName == undefined || this.promotionFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._promotionService.uploadFile_title(this.promotionTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();
            //==================== File Download ================================
            this._promotionService.uploadFile(this.promotionFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._promotionService.uploadFile_banner(this.promotionBannerImage).subscribe(response => {

                this.BannerImagepath = response.toString();

                this._promotionService.uploadFile_mobile(this.promotionBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._promotionService.addpromotion(this.promotionTitle, this.promotionType, this.pdfFilePath,
                    this.titleImagepath,
                    userId, this.promotionStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                      this.router.navigate(['/back-office/promotion']);

                    })
                })
              })
            })
          })



        }




      }

      else if (this.promotionType === 'vdo') {
        //==================== Title Image ================================
        this._promotionService.uploadFile_title(this.promotionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._promotionService.uploadFile_banner(this.promotionBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._promotionService.uploadFile_mobile(this.promotionBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._promotionService.addpromotion(this.promotionTitle, this.promotionType, this.vdo_id, this.titleImagepath,
                userId, this.promotionStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                  this.router.navigate(['/back-office/promotion']);

                })
            })
          })
        })





      }

      this.ngxService.stop();

    }


  }

  Back() {
    this.router.navigate(['/back-office/promotion']);
  }






}
