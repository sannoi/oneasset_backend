import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-user-group',
  templateUrl: './add-user-group.component.html',
  styleUrls: ['./add-user-group.component.scss']
})
export class AddUserGroupComponent implements OnInit {

  data: any[] = [
    { val: 'y', name: 'Active ' },
    { val: 'n', name: 'Not Active  ' },
  ];

  selectedStatus: any
  groupName: any

  constructor(private router: Router, private ngxService: NgxUiLoaderService,
    private userService: UserService) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m11')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
  }

  setGroupName(groupName) {
    this.groupName = groupName
  }

  setGroupStatus(status) {
    this.selectedStatus = status
  }

  addUserGroup() {
    this.ngxService.start()
    if (this.selectedStatus === undefined) {
      this.userService.addUserGroup(this.groupName, 'y').subscribe(response => {
        this.ngxService.stop()
        this.router.navigate(['/back-office/user-group'])
      })
    } else {
      this.userService.addUserGroup(this.groupName, this.selectedStatus).subscribe(response => {
        this.ngxService.stop()
        this.router.navigate(['/back-office/user-group'])
      })
    }

  }

  Back() {
    this.router.navigate(['/back-office/user-group'])
  }

}
