import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { VdoService } from 'src/app/services/vdo.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';


@Component({
  selector: 'app-vdo-edit',
  templateUrl: './vdo-edit.component.html',
  styleUrls: ['./vdo-edit.component.scss']
})
export class VdoEditComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _Service: VdoService, private ngxService: NgxUiLoaderService) { }

  private sub: any;

  isTh: boolean
  isEn: boolean
  id: string;
  vdoType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  vdo: any;

  vdoTtitleImage: any;
  vdoBannerImage: any
  vdoBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  vdoFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  typevdo: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusvdo: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editvdoForm: FormGroup;

  old_vdoType: string;
  old_vdoDetail: string;
  isPDF_old: any;

  vdo_id: any

      // TODO: Public Date
      publicDate: any;
      public_date: any;

  ngOnInit() {

      // TODO: Public Date
      flatpickr.localize(Thai);

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.isTh = true
    this.isEn = false

    this._Service.getvdoByID(this.id).subscribe(response => {

      this.vdo = response
      console.log(this.vdo)
      this.vdoType = response[0]['vdo_type']
      this.title = response[0]['title']
      this.title_image = response[0]['vdo_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['vdo_img_Thum']

      if (response[0]['vdo_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

      if (response[0]['vdo_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }

      this.old_vdoType = response[0]['vdo_type']
      this.old_vdoDetail = response[0]['detail']

      if (response[0]['vdo_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }

       // TODO: Public Date
       if (response[0]['publicDate']) {
        this.public_date = response[0]['publicDate'];
        this.publicDate  = response[0]['publicDate'];
      } else {
        this.public_date = new Date().toISOString().slice(0,10);
        this.publicDate  = new Date().toISOString().slice(0,10);
      }



      //=============== Binding Info ==================//
      this.editvdoForm = this.fb.group({
        vdoTypeControl: this.vdoType,
        vdoTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }

  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setvdoType(code) {
    this.vdoType = code;

    if (this.vdoType == this.old_vdoType) {
      //=============== Binding Info ==================//
      this.editvdoForm = this.fb.group({
        vdoTypeControl: this.vdoType,
        vdoTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_vdoDetail,
        public_date: this.public_date
      });
      this.detail = this.old_vdoDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editvdoForm = this.fb.group({
        vdoTypeControl: this.vdoType,
        vdoTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }


  setvdoTitle(title) { this.title = title; }
  setvdoFiles(event) {
    if (event.target.id === 'vdoTitleImage') {
      this.vdoTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'vdoFile') {
      this.vdoFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setvdoStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/vdo']);
  }

  deleteBanner() {
    this._Service.delBannervdo(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/vdo/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = ""

  }

  deleteBanner_mobile() {
    this._Service.delBannervdo_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/vdo/edit'], { queryParams: { id: this.id } });
  }

  setvdoBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.vdoBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setvdoBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.vdoBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editvdo() {
    this.ngxService.start();

    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.vdoType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._Service.uploadFile_title(this.vdoTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            if (this.status === '1') {
              //==================== Banner Image ================================
              this._Service.uploadFile_mobile(this.vdoBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._Service.uploadFile_banner(this.vdoBannerImage).subscribe(response => {
                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }
                  this._Service.updatevdo(this.title, this.vdoType, editorHtmlTag, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vdo']);
                    })

                })

              })

            }
            else if (this.status === '0') {
              this._Service.delBannervdo(this.id).subscribe(response => {
                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";
                this._Service.updatevdo(this.title, this.vdoType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vdo']);
                  })

              })
            }

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }



        })



      } else if (this.vdoType === 'image') {

        //==================== Title Image ================================
        this._Service.uploadFile_title(this.vdoTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            //==================== File Download ================================
            this._Service.uploadFile(this.vdoFiles).subscribe(response => {
              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              if (this.status === '1') {
                //==================== Banner Image ================================
                this._Service.uploadFile_mobile(this.vdoBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._Service.uploadFile_banner(this.vdoBannerImage).subscribe(response => {
                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }
                    this._Service.updatevdo(this.title, this.vdoType, this.FilePath, this.titleImagepath, userId,
                      this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/vdo']);
                      })
                  })

                })

              }
              else if (this.status === '0') {
                this._Service.delBannervdo(this.id).subscribe(response => {
                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";
                  this._Service.updatevdo(this.title, this.vdoType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vdo']);
                    })
                })
              }



            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })




      } else if (this.vdoType === 'pdf') {

        //==================== Title Image ================================
        this._Service.uploadFile_title(this.vdoTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== File Download ================================
            this._Service.uploadFile(this.vdoFiles).subscribe(response => {
              if (response.toString() != "") {
                this.FilePath = response.toString();
              }
              if (this.status === '1') {
                //==================== Banner Image ================================
                this._Service.uploadFile_mobile(this.vdoBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._Service.uploadFile_banner(this.vdoBannerImage).subscribe(response => {
                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }
                    this._Service.updatevdo(this.title, this.vdoType, this.FilePath, this.titleImagepath,
                      userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/vdo']);
                      })
                  })

                })

              }
              else if (this.status === '0') {
                this._Service.delBannervdo(this.id).subscribe(response => {
                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";
                  this._Service.updatevdo(this.title, this.vdoType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vdo']);
                    })
                })
              }

            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })







      }
      else if (this.vdoType === 'vdo') {

        //==================== Title Image ================================
        this._Service.uploadFile_title(this.vdoTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            if (this.status === '1') {
              //==================== Banner Image ================================
              this._Service.uploadFile_mobile(this.vdoBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._Service.uploadFile_banner(this.vdoBannerImage).subscribe(response => {
                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }
                  this._Service.updatevdo(this.title, this.vdoType, this.vdo_id, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vdo']);
                    })
                })


              })

            }
            else if (this.status === '0') {
              this._Service.delBannervdo(this.id).subscribe(response => {
                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";
                this._Service.updatevdo(this.title, this.vdoType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vdo']);
                  })
              })
            }



          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })



      }

    }



    this.ngxService.stop();

  }



}
