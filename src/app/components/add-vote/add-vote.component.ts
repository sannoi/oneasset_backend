import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VoteService } from 'src/app/services/vote.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';


// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-vote',
  templateUrl: './add-vote.component.html',
  styleUrls: ['./add-vote.component.scss']
})
export class AddVoteComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };

  isTh: boolean
  isEn: boolean
  voteType: string;
  typevote: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  voteStatus: string;
  statusvote: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add vote
  voteTitle: string
  voteTtitleImage: any
  voteBannerImage: any
  imageTitleName: any
  voteContentImage: any
  imageContentName: any
  voteFiles: any
  voteFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  voteBannerImage_mobile: any;
  BannerImagepath_mobile: any;

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private _voteService: VoteService, private router: Router,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
       // TODO: Public Date
       flatpickr.localize(Thai);
       this.public_date = new Date().toISOString().slice(0,10);
      this.publicDate = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }


    this.voteType = 'text';
    this.voteStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVoteType(code) {
    // console.log("choice: ", code);
    this.voteType = code;
  }

  setVoteTitle(title: string) {
    this.voteTitle = title
  }

  setVoteStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.voteStatus = '1'
    } else if (code === 'Unpublish ') {
      this.voteStatus = '0'
    }
  }

  setVoteFiles(event) {
    if (event.target.id === 'VoteTitleImage') {
      this.voteTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'VoteContentImage') {
      this.voteContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'VoteFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.voteFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }


    }
  }

  setVoteBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.voteBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setVoteBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.voteBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }


  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  addVote() {
    let userId = sessionStorage.getItem('userId')

    this.ngxService.start();

    if (this.voteTitle == undefined || this.voteTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      if (this.voteType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._voteService.uploadFile_title(this.voteTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._voteService.uploadFile_banner(this.voteBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._voteService.uploadFile_mobile(this.voteBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._voteService.addvote(this.voteTitle, this.voteType, editorHtmlTag, this.titleImagepath, userId,
                this.voteStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/vote-report']);
                })

            })
          })

        })



      }
      else if (this.voteType === 'image') {
        //==================== Title Image ================================
        this._voteService.uploadFile_title(this.voteTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._voteService.uploadFile(this.voteContentImage).subscribe(response => {
            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._voteService.uploadFile_banner(this.voteBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();


              this._voteService.uploadFile_mobile(this.voteBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._voteService.addvote(this.voteTitle, this.voteType, this.contentImagePath, this.titleImagepath,
                  userId, this.voteStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vote-report']);

                  })

              })
            })
          })
        })







      } else if (this.voteType === 'pdf') {

        if (this.voteFileName == undefined || this.voteFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {


          //==================== Title Image ================================
          this._voteService.uploadFile_title(this.voteTtitleImage).subscribe(response => {

            this.titleImagepath.response.toString();

            //==================== File Download ================================
            this._voteService.uploadFile(this.voteFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._voteService.uploadFile_banner(this.voteBannerImage).subscribe(response => {
                this.BannerImagepath.response.toString();

                this._voteService.uploadFile_mobile(this.voteBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile.response.toString();

                  this._voteService.addvote(this.voteTitle, this.voteType, this.pdfFilePath, this.titleImagepath,
                    userId, this.voteStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vote-report']);

                    })
                })
              })
            })
          })



        }


      }

    }


    this.ngxService.stop();

  }

  Back() {
    this.router.navigate(['/back-office/vote-report']);
  }

}
