import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentService } from 'src/app/services/document.service';
import { FormBuilder } from '@angular/forms';
import { linkApi } from '../../constants/constants';

@Component({
  selector: 'app-edit-document',
  templateUrl: './edit-document.component.html',
  styleUrls: ['./edit-document.component.scss']
})
export class EditDocumentComponent implements OnInit {
  private sub: any;
  id: any;
  doc: any;
  File_Name: any;
  docType: any;
  status: any;
  editDocForm: any;
  docTypeList: any;
  docFile: any
  docFilePaht: string = '';

  docStatus: any[] = [
    { code: 'y', chooes: 'Publish' },
    { code: 'n', chooes: 'Unpublish' },
  ];


  File_Path: any;

  constructor(private route: ActivatedRoute, private _documentService: DocumentService, private fb: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m6')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this._documentService.getDocumentType('', 'y').subscribe(response => {
      this.docTypeList = response
    })

    this._documentService.getDocById(this.id).subscribe(response => {

      console.log(response)
      this.doc = response
      this.File_Name = response[0].File_Name
      this.File_Path = response[0].File_Path
      this.docType = response[0].docType
      this.status = response[0].status

      this.editDocForm = this.fb.group({
        documentType: this.docType,
        docName: this.File_Name,
        tmpStatus: this.status
      })
    })
  }

  setDocFile(docFile) {
    this.docFile = <File>docFile.target.files[0]
    this.docFilePaht = linkApi + '/FileUpload/' + <File>docFile.target.files[0].name
  }

  editDocument() {

    let userId = sessionStorage.getItem('userId')

    this._documentService.uploadFile(this.docFile).subscribe(response => {

      console.log(response);

      if (response.toString() != "") {
        this.docFilePaht = response.toString();
      }


      if (this.docFilePaht != "") {
        this._documentService.update_document(this.docType, this.File_Name, this.status, this.id, this.docFilePaht,
          userId).subscribe(response => {
            event.preventDefault()
            this.router.navigate(['/back-office/document/'])
          })
      }
      else {
        this._documentService.update_document(this.docType, this.File_Name, this.status, this.id, this.File_Path,
          userId).subscribe(response => {
            event.preventDefault()
            this.router.navigate(['/back-office/document/'])
          })
      }
    })

  }

  back() {
    event.preventDefault()
    this.router.navigate(['/back-office/document/'])
  }

  setDocTypeName(typeid) {
    this.docType = typeid
  }

  setDocName(docname) {
    this.File_Name = docname
  }

  setDocumentStatus(status) {
    this.status = status
  }

}
