import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EthicService } from 'src/app/services/ethic.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';


@Component({
  selector: 'app-add-ethic',
  templateUrl: './add-ethic.component.html',
  styleUrls: ['./add-ethic.component.scss']
})
export class AddEthicComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  ethicType: string;
  typeethic: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  ethicStatus: string;
  statusethic: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add ethic
  ethicTitle: string
  ethicTitleEN: string
  ethicTtitleImage: any
  ethicBannerImage: any
  imageTitleName: any
  ethicContentImage: any
  imageContentName: any
  ethicFiles: any
  ethicFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  ethicBannerImage_mobile: any;
  BannerImagepath_mobile: any;

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private _ethicService: EthicService, private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }

  ngOnInit() {
        // TODO: Public Date
        flatpickr.localize(Thai);
        this.public_date = new Date().toISOString().slice(0,10);
        this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ethicType = 'text';
    this.ethicStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }
  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }
  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setEthicType(code) {
    // console.log("choice: ", code);
    this.ethicType = code;
  }

  setEthicTitle(title: string) {
    this.ethicTitle = title
  }

  setEthicTitleEN(title: string) {
    this.ethicTitleEN = title
  }

  setEthicStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.ethicStatus = '1'
    } else if (code === 'Unpublish ') {
      this.ethicStatus = '0'
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  setEthicFiles(event) {
    if (event.target.id === 'EthicTitleImage') {
      this.ethicTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'EthicContentImage') {
      this.ethicContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'EthicFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.ethicFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }



    }

  }

  setEthicBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.ethicBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setEthicBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.ethicBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addEthic() {
    let userId = sessionStorage.getItem('userId')
    if (this.ethicTitle == undefined || this.ethicTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {
      this.ngxService.start();

      if (this.ethicType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._ethicService.uploadFile_title(this.ethicTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._ethicService.uploadFile_banner(this.ethicBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            //==================== Banner Image ================================
            this._ethicService.uploadFile_mobile(this.ethicBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._ethicService.addethic(this.ethicTitle, this.ethicType, editorHtmlTag, this.titleImagepath, userId,
                this.ethicStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/ethic']);
                })

            })
          })
        })





      }
      else if (this.ethicType === 'image') {
        //==================== Title Image ================================
        this._ethicService.uploadFile_title(this.ethicTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._ethicService.uploadFile(this.ethicContentImage).subscribe(response => {
            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._ethicService.uploadFile_banner(this.ethicBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();

              this._ethicService.uploadFile_mobile(this.ethicBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._ethicService.addethic(this.ethicTitle, this.ethicType, this.contentImagePath, this.titleImagepath,
                  userId, this.ethicStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/ethic']);

                  })
              })
            })
          })

        })




      } else if (this.ethicType === 'pdf') {

        if (this.ethicFileName == undefined || this.ethicFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {
          //==================== Title Image ================================
          this._ethicService.uploadFile_title(this.ethicTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._ethicService.uploadFile(this.ethicFileName).subscribe(response => {

              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._ethicService.uploadFile_banner(this.ethicBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._ethicService.uploadFile_mobile(this.ethicBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._ethicService.addethic(this.ethicTitle, this.ethicType, this.pdfFilePath, this.titleImagepath,
                    userId, this.ethicStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/ethic']);
                    })
                })
              })
            })
          })
        }







      }
      else if (this.ethicType === 'vdo') {
        //==================== Title Image ================================
        this._ethicService.uploadFile_title(this.ethicTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._ethicService.uploadFile_banner(this.ethicBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._ethicService.uploadFile_mobile(this.ethicBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._ethicService.addethic(this.ethicTitle, this.ethicType, this.vdo_id, this.titleImagepath,
                userId, this.ethicStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/ethic']);

                })
            })
          })
        })


      }
    }

    this.ngxService.stop();


  }

  Back() {
    this.router.navigate(['/back-office/ethic']);
  }

}
