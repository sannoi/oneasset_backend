import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBannerProvidentComponent } from './add-banner-provident.component';

describe('AddBannerProvidentComponent', () => {
  let component: AddBannerProvidentComponent;
  let fixture: ComponentFixture<AddBannerProvidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBannerProvidentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBannerProvidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
