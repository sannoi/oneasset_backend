import { Component, OnInit } from '@angular/core';
import { PagerService } from 'src/app/_services';
import { FundrecommendService } from 'src/app/services/fundrecommend.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-fundrecommend',
  templateUrl: './fundrecommend.component.html',
  styleUrls: ['./fundrecommend.component.scss']
})
export class FundrecommendComponent implements OnInit {

  constructor(private router: Router, private ngxService: NgxUiLoaderService,
    private _fundrecommService: FundrecommendService, private pagerService: PagerService) { }

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];

  fundRecomDate: any
  fundRecoms: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._fundrecommService.getfundRecomList(1, "").subscribe(response => {
      this.fundRecoms = response
      this.allItems = response
      this.setPage(1);
      this.selectedLevel = 1
      this.ngxService.stop();
    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();

      this._fundrecommService.getfundRecomList(this.selectedLevel, this.topicSearch).subscribe(response => {
        this.fundRecoms = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  getfundRecomFromSerch() {
    this.ngxService.start();
    this._fundrecommService.getfundRecomList(this.selectedLevel, this.topicSearch).subscribe(response => {
      this.fundRecoms = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.fundRecomDate = this._fundrecommService.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/fundrecommend/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickAddfundRecom() {
    event.preventDefault()
    this.router.navigate(['/back-office/fundrecommend/add'])

  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.fundRecomDate = this._fundrecommService.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.fundRecomDate = this._fundrecommService.date_convert(date)
    }
  }




}
