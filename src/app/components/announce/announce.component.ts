import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { AnnounceService } from 'src/app/services/announce.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-announce',
  templateUrl: './announce.component.html',
  styleUrls: ['./announce.component.scss']
})
export class AnnounceComponent implements OnInit {

  constructor(private router: Router, private _announceService: AnnounceService, private pagerService: PagerService,
    private ngxService: NgxUiLoaderService) { }

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];

  announceDate: any
  announces: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();

    this._announceService.getAnnounce(1, "").subscribe(response => {
      this.announces = response
      console.log(this.announces);
      this.allItems = response
      this.setPage(1);

      this.selectedLevel = 1

      this.ngxService.stop();

    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  getNewsFromSerch() {
    this.ngxService.start();


    this._announceService.getAnnounce(this.selectedLevel, this.topicSearch).subscribe(response => {
      this.announces = response
      this.allItems = response
      this.setPage(1);

      this.ngxService.stop();

    })
  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();

      this._announceService.getAnnounce(this.selectedLevel, this.topicSearch).subscribe(response => {
        this.announces = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.announceDate = this._announceService.changeFormatDate(date)
    }
  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.announceDate = this._announceService.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.announceDate = this._announceService.date_convert(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/announce/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickAddAnnounce() {
    event.preventDefault()
    this.router.navigate(['/back-office/announce/add'])

  }
















}
