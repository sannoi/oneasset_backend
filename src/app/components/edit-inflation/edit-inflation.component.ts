import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-edit-inflation',
  templateUrl: './edit-inflation.component.html',
  styleUrls: ['./edit-inflation.component.scss']
})
export class EditInflationComponent implements OnInit {

  editForm: FormGroup

  resInflation: any
  inflation: any

  r1: any;
  r2: any;
  r3: any;
  r4: any;
  r5: any;


  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _documentService: DocumentService, ) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m10')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this._documentService.getMaintain("Inflation").subscribe(response => {
      // console.log(response);

      // this.r1 = response["r1"];
      this.resInflation = response["r1"];

      // this.r2 = response["r2"];
      // this.r3 = response["r3"];
      // this.r4 = response["r4"];
      // this.r5 = response["r5"];

      //=============== Binding Info ==================//
      this.editForm = this.fb.group({
        inflation1_control: this.resInflation,
        // inflation2_control : this.r2,
        // inflation3_control : this.r3,
        // inflation4_control : this.r4,
        // inflation5_control : this.r5,
      });
      //==============================================//

    })
  }

  setInflation(inflation) {
    this.inflation = inflation

  }



  editInflation() {


    this._documentService.UpdateMaintain("Inflation", this.inflation, "0", "0", "0", "0").subscribe(response => {
      this.router.navigate(['back-office/maintain'])

    })

  }

  Back() {
    this.router.navigate(['back-office/maintain'])
  }

}
