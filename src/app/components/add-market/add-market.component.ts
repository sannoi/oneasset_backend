import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MarketService } from 'src/app/services/market.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-market',
  templateUrl: './add-market.component.html',
  styleUrls: ['./add-market.component.scss']
})
export class AddMarketComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  marketType: string;
  typemarket: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  marketStatus: string;
  statusmarket: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add market
  marketTitle: string
  marketTtitleImage: any
  marketBannerImage: any
  imageTitleName: any
  marketContentImage: any
  imageContentName: any
  marketFiles: any
  marketFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  marketBannerImage_mobile: any;
  BannerImagepath_mobile: any;

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private _marketService: MarketService, private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }


  ngOnInit() {
      // TODO: Public Date
      flatpickr.localize(Thai);
      this.public_date = new Date().toISOString().slice(0,10);
      this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.marketType = 'text';
    this.marketStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }

   // TODO: Public Date
   setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  setmarketType(code) {
    // console.log("choice: ", code);
    this.marketType = code;
  }

  setmarketTitle(title: string) {
    this.marketTitle = title
  }

  setmarketStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.marketStatus = '1'
    } else if (code === 'Unpublish ') {
      this.marketStatus = '0'
    }
  }

  setmarketFiles(event) {
    if (event.target.id === 'marketTitleImage') {
      this.marketTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'marketContentImage') {
      this.marketContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'marketFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.marketFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }



    }
  }

  setmarketBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.marketBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setmarketBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.marketBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }


  addmarket() {
    let userId = sessionStorage.getItem('userId')
    if (this.marketTitle == undefined || this.marketTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else if (this.marketTtitleImage == undefined || this.marketTtitleImage == "") {
      alert("กรุณาเลือก Title Image !!");
      this.ngxService.stop();

    }
    else {
      if (this.marketType === 'text') {

        this.ngxService.start();

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._marketService.uploadFile_title(this.marketTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._marketService.uploadFile_banner(this.marketBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._marketService.uploadFile_mobile(this.marketBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._marketService.addmarket(this.marketTitle, this.marketType, editorHtmlTag, this.titleImagepath, userId,
                this.marketStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/market']);
                })
            })
          })
        })






      }
      else if (this.marketType === 'image') {
        //==================== Title Image ================================
        this._marketService.uploadFile_title(this.marketTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();


          //==================== File Download ================================
          this._marketService.uploadFile(this.marketContentImage).subscribe(response => {
            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._marketService.uploadFile_banner(this.marketBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();


              this._marketService.uploadFile_mobile(this.marketBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();


                this._marketService.addmarket(this.marketTitle, this.marketType, this.contentImagePath, this.titleImagepath,
                  userId, this.marketStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/market']);

                  })
              })
            })
          })
        })






      } else if (this.marketType === 'pdf') {

        if (this.marketFileName == undefined || this.marketFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {


          //==================== Title Image ================================
          this._marketService.uploadFile_title(this.marketTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();


            //==================== File Download ================================
            this._marketService.uploadFile(this.marketFileName).subscribe(response => {
              this.pdfFilePath = response.toString();


              //==================== Banner Image ================================
              this._marketService.uploadFile_banner(this.marketBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();


                this._marketService.uploadFile_mobile(this.marketBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();


                  this._marketService.addmarket(this.marketTitle, this.marketType, this.pdfFilePath, this.titleImagepath,
                    userId, this.marketStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/market']);

                    })
                })
              })
            })
          })


        }




      }
      else if (this.marketType === 'vdo') {
        //==================== Title Image ================================
        this._marketService.uploadFile_title(this.marketTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();


          //==================== Banner Image ================================
          this._marketService.uploadFile_banner(this.marketBannerImage).subscribe(response => {

            this.BannerImagepath = response.toString();

            this._marketService.uploadFile_mobile(this.marketBannerImage_mobile).subscribe(response => {

              this.BannerImagepath_mobile = response.toString();

              this._marketService.addmarket(this.marketTitle, this.marketType, this.vdo_id, this.titleImagepath,
                userId, this.marketStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/market']);

                })
            })
          })
        })



      }

      this.ngxService.stop();

    }


  }

  Back() {
    this.router.navigate(['/back-office/market']);
  }





}
