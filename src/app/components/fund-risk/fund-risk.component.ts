import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { FundRiskService } from 'src/app/services/fund-risk.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-fund-risk',
  templateUrl: './fund-risk.component.html',
  styleUrls: ['./fund-risk.component.scss']
})
export class FundRiskComponent implements OnInit {

  constructor(private router: Router, private _Service: FundRiskService, private pagerService: PagerService,
    private ngxService: NgxUiLoaderService) { }

  fundRisk: any;
  fundRiskDate: any;

  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m8')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }


    this.ngxService.start();
    this._Service.get_riskLevel().subscribe(response => {
      this.fundRisk = response
      console.log(this.fundRisk);
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();

    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.fundRiskDate = this._Service.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/fund-risk/edit'], { queryParams: { id: xxx } });

  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.fundRiskDate = this._Service.date_thai(date)
    }
  }

  date_convert(date, short) {
    if (date === '') {
      return null
    } else {
      return this.fundRiskDate = this._Service.date_convert(date, short)
    }
  }

}
