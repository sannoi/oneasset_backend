import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerPrivateComponent } from './banner-private.component';

describe('BannerPrivateComponent', () => {
  let component: BannerPrivateComponent;
  let fixture: ComponentFixture<BannerPrivateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerPrivateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerPrivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
