import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { BannerPrivateService } from 'src/app/services/banner-private.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-banner-private',
  templateUrl: './banner-private.component.html',
  styleUrls: ['./banner-private.component.scss']
})
export class BannerPrivateComponent implements OnInit {

  constructor(private router: Router, private _bannerServe: BannerPrivateService, private pagerService: PagerService,
    private ngxService: NgxUiLoaderService) { }

  bannerDate: any
  banner: any;
  topicSearch: any;
  selectedLevel: any;
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();

    this._bannerServe.getBanner("").subscribe(response => {
      this.banner = response
      this.allItems = response
      this.setPage(1);

      this.ngxService.stop();

    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  getBannerFromSerch() {
    this.ngxService.start();
    this._bannerServe.getBanner(this.topicSearch).subscribe(response => {
      this.banner = response
      console.log(this.banner)
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();

    })
  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();

      this._bannerServe.getBanner(this.topicSearch).subscribe(response => {
        this.banner = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.bannerDate = this._bannerServe.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {

    this.ngxService.start();

    this._bannerServe.delBanner(xxx).subscribe(response => {

      console.log(response)
      this._bannerServe.getBanner(this.topicSearch).subscribe(response => {
        this.banner = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })

    })

    // this.router.navigate(['/back-office/news/edit'])
  }

  clickAdd() {
    event.preventDefault()
    this.router.navigate(['/back-office/banner-private/add'])

  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.bannerDate = this._bannerServe.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.bannerDate = this._bannerServe.date_convert(date)
    }
  }

}
