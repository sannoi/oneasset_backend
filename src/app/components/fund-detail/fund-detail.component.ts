import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { FundDetailService } from 'src/app/services/fund-detail.service'
import { NgxUiLoaderService } from 'ngx-ui-loader';


@Component({
  selector: 'app-fund-detail',
  templateUrl: './fund-detail.component.html',
  styleUrls: ['./fund-detail.component.scss']
})
export class FundDetailComponent implements OnInit {

  data: any[] = [
    { val: '', name: 'All' },
    { val: 'w', name: 'Publish On Web' },
    { val: 'm', name: 'Publish On Mobile' },
    { val: 'b', name: 'Publish Both' },
    { val: 'n', name: 'Unassign' }
  ];

  selectedLevel: any;

  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  // searchFunction
  topicSearch: string

  fundDate : any;

  constructor(private router: Router, private pagerService: PagerService, private _fundDetailService: FundDetailService,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m9')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._fundDetailService.getFund('', '').subscribe(response => {
      // console.log('cms fund detail response: ', response);

      console.log(response)
      this.selectedLevel = "";
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.fundDate = this._fundDetailService.changeFormatDate(date)
    }
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log(val);

    this.selectedLevel = val;
  }

  getNewsFromSerch() {
    // console.log('on serach: ', this.selectedLevel);
    this.ngxService.start();
    this._fundDetailService.getFund(this.topicSearch, this.selectedLevel).subscribe(response => {
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  getSync(){
    this.ngxService.start();
    this._fundDetailService.getSync().subscribe(response => {
      console.log(response)
      this.ngxService.stop();
    })
  }


  getSyncDoc(){
    this.ngxService.start();
    this._fundDetailService.getSyncDoc().subscribe(response => {
      console.log(response)
      this.ngxService.stop();
    })
  }



  clickEdit(xxx) {
    this.router.navigate(['/back-office/fund-detail/edit'], { queryParams: { id: xxx } });
  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.fundDate = this._fundDetailService.date_thai(date)
    }
  }

  date_convert(date, short) {
    if (date === '') {
      return null
    } else {
      return this.fundDate = this._fundDetailService.date_convert(date, short)
    }
  }
}
