import { Component, OnInit } from '@angular/core';
import { NewsletterService } from 'src/app/services/newsletter.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-newsletter-add',
  templateUrl: './newsletter-add.component.html',
  styleUrls: ['./newsletter-add.component.scss']
})
export class NewsletterAddComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  newsletterType: string;
  typenewsletter: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  newsletterStatus: string;
  statusnewsletter: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add newsletter
  newsletterTitle: string
  newsletterTtitleImage: any
  newsletterBannerImage: any
  newsletterBannerImage_mobile: any
  imageTitleName: any
  newsletterContentImage: any
  imageContentName: any
  newsletterFiles: any
  newsletterFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  BannerImagepath_mobile: any
  contentImagePath: any
  pdfFilePath: any
  vdo_id: any

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private _newsletterService: NewsletterService, private router: Router,
    private ngxService: NgxUiLoaderService) { }


  ngOnInit() {
 // TODO: Public Date
 flatpickr.localize(Thai);
 this.public_date = new Date().toISOString().slice(0,10);
 this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.isTh = true
    this.isEn = false
    this.newsletterType = 'text';
    this.newsletterStatus = '1';
    this.BannerImagepath = '';
    this.BannerImagepath_mobile = '';
  }


  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setnewsletterType(code) {
    // console.log("choice: ", code);
    this.newsletterType = code;
  }

  setnewsletterTitle(title: string) {
    this.newsletterTitle = title
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  setnewsletterStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.newsletterStatus = '1'
    } else if (code === 'Unpublish ') {
      this.newsletterStatus = '0'
    }
  }

  setnewsletterFiles(event) {
    if (event.target.id === 'newsletterTitleImage') {
      this.newsletterTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'newsletterContentImage') {
      this.newsletterContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'newsletterFile') {
      this.newsletterFileName = <File>event.target.files[0]
      this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }

  setnewsletterBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.newsletterBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setnewsletterBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.newsletterBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }


  addnewsletter() {
    this.ngxService.start();

    let userId = sessionStorage.getItem('userId')

    if (this.newsletterTitle == undefined || this.newsletterTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {



      if (this.newsletterType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._newsletterService.uploadFile_title(this.newsletterTtitleImage).subscribe(response => {

          if (response.toString() != "") {
            this.titleImagepath = response.toString();



            //==================== Banner Image ================================
            this._newsletterService.uploadFile_banner(this.newsletterBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();
              console.log(response)

              this._newsletterService.uploadFile_mobile(this.newsletterBannerImage_mobile).subscribe(response => {

                this.BannerImagepath_mobile = response.toString();
                console.log(response)

                this._newsletterService.addnewsletter(this.newsletterTitle, this.newsletterType, editorHtmlTag, this.titleImagepath, userId,
                  this.newsletterStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/newsletter']);
                  })

              })



            })

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }






        })



      }
      else if (this.newsletterType === 'image') {
        //==================== Title Image ================================
        this._newsletterService.uploadFile_title(this.newsletterTtitleImage).subscribe(response => {

          if (response.toString() != "") {
            this.titleImagepath = response.toString();


            //==================== File Download ================================
            this._newsletterService.uploadFile(this.newsletterContentImage).subscribe(response => {
              this.contentImagePath = response.toString();

              //==================== Banner Image ================================
              this._newsletterService.uploadFile_banner(this.newsletterBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._newsletterService.uploadFile_mobile(this.newsletterBannerImage_mobile).subscribe(response => {

                  this.BannerImagepath_mobile = response.toString();

                  this._newsletterService.addnewsletter(this.newsletterTitle, this.newsletterType, this.contentImagePath,
                    this.titleImagepath,
                    userId, this.newsletterStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => { })

                  this.router.navigate(['/back-office/newsletter']);

                })

              })

            })

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }


        })








      } else if (this.newsletterType === 'pdf') {
        //==================== Title Image ================================
        this._newsletterService.uploadFile_title(this.newsletterTtitleImage).subscribe(response => {

          if (response.toString() != "") {
            this.titleImagepath = response.toString();


            //==================== File Download ================================
            this._newsletterService.uploadFile(this.newsletterFileName).subscribe(response => {
              this.pdfFilePath = response.toString();
              //==================== Banner Image ================================
              this._newsletterService.uploadFile_banner(this.newsletterBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();
                this._newsletterService.uploadFile_mobile(this.newsletterBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._newsletterService.addnewsletter(this.newsletterTitle, this.newsletterType, this.pdfFilePath,
                    this.titleImagepath, userId, this.newsletterStatus, this.BannerImagepath,
                    this.BannerImagepath_mobile , this.publicDate).subscribe(response => { })
                  this.router.navigate(['/back-office/newsletter']);

                })
              })
            })

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }


        })






      }
      else if (this.newsletterType === 'vdo') {
        //==================== Title Image ================================
        this._newsletterService.uploadFile_title(this.newsletterTtitleImage).subscribe(response => {

          if (response.toString() != "") {
            this.titleImagepath = response.toString();


            //==================== Banner Image ================================
            this._newsletterService.uploadFile_banner(this.newsletterBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();
              this._newsletterService.uploadFile_mobile(this.newsletterBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._newsletterService.addnewsletter(this.newsletterTitle, this.newsletterType,
                  this.vdo_id, this.titleImagepath,
                  userId, this.newsletterStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => { })

                this.router.navigate(['/back-office/newsletter']);

              })
            })

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })






      }


    }

    this.ngxService.stop();

  }

  Back() {
    this.router.navigate(['/back-office/newsletter']);
  }

}
