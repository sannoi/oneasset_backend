import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-menu-mapping',
  templateUrl: './menu-mapping.component.html',
  styleUrls: ['./menu-mapping.component.scss']
})
export class MenuMappingComponent implements OnInit {

  private sub: any
  groupName: any
  groupId: any

  selectMenu: Array<any> = []
  deleteMenu: Array<any> = []

  menuMapped: any
  menuNotMap: any

  constructor(private route: ActivatedRoute, private router: Router,
    private _userService: UserService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    
    this.ngxService.start();
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.groupName = params['groupName'] || '';
        this.groupId = params['id'] || 0;
      });

    this._userService.getMenuNotMap(this.groupId).subscribe(response => {
      console.log('menu not map', response);
      this.menuNotMap = response
    })

    this._userService.getMenuMapped(this.groupId).subscribe(response => {
      this.menuMapped = response
      this.ngxService.stop()
    })
  }

  saveMenu(value: boolean, menuId) {
    // console.log('select menu id', menuId);

    if (value == true) {
      this.selectMenu.push(menuId);
    }
    else {
      const index: number = this.selectMenu.indexOf(menuId);
      if (index !== -1) {
        this.selectMenu.splice(index, 1);
      }
    }
    console.log(this.selectMenu);

  }

  delMenu(value: boolean, menuId) {
    if (value == true) {
      this.deleteMenu.push(menuId);
    }
    else {
      const index: number = this.deleteMenu.indexOf(menuId);
      if (index !== -1) {
        this.deleteMenu.splice(index, 1);
      }
    }
  }

  selectMenuMapp() {
    this.ngxService.start();
    if (this.selectMenu.length != 0) {
      var i: number;
      for (i = 0; i < this.selectMenu.length; i++) {
        this._userService.InsertMenuMapped(this.groupId, this.selectMenu[i], "y").subscribe(response => {

          this._userService.getMenuNotMap(this.groupId).subscribe(response => {
            this.menuNotMap = response;

            this._userService.getMenuMapped(this.groupId).subscribe(response => {
              this.menuMapped = response;
            })

          })

        })
      }
      this.selectMenu = [];
      this.ngxService.stop();
    }
  }


  delMenuMapp() {
    this.ngxService.start();
    if (this.deleteMenu.length != 0) {
      var i: number
      for (i = 0; i < this.deleteMenu.length; i++) {

        this._userService.DeleteMenuMapped(this.deleteMenu[i]).subscribe(response => {
          this._userService.getMenuMapped(this.groupId).subscribe(response => {
            this.menuMapped = response;
            this._userService.getMenuNotMap(this.groupId).subscribe(response => {
              this.menuNotMap = response;
            })
          })
        })
      }
      this.deleteMenu = [];
      this.ngxService.stop();

    }
  }

  Back() {
    this.router.navigate(['back-office/user-group'])
  }

}
