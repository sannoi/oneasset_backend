import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-edit-document-type',
  templateUrl: './edit-document-type.component.html',
  styleUrls: ['./edit-document-type.component.scss']
})
export class EditDocumentTypeComponent implements OnInit {

  private sub: any;
  id: string;
  docType: any
  documentTypeName: string
  doctypeStatus: string
  doctypeStatus1: string

  editDocTypeForm: FormGroup
  statusDocType: any[] = [
    { code: 'y', chooes: 'Publish' },
    { code: 'n', chooes: 'Unpublish' },
  ];


  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _documentService: DocumentService) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m6')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this._documentService.getDocTypeById(this.id).subscribe(response => {
      this.docType = response
      this.documentTypeName = response[0].Document_type
      this.doctypeStatus1 = response[0].Type_status
      console.log(response[0])

      if (response[0].Type_status === 'y') {
        this.doctypeStatus = 'y'
      } else if (response[0].Type_status === 'n') {
        this.doctypeStatus = 'n'
      }

      console.log(this.doctypeStatus)
      this.editDocTypeForm = this.fb.group({
        
        documentTypeNameControl: this.documentTypeName,
        statusControl: this.doctypeStatus
      })
    })
  }

  setDoctypeName(docType) {
    this.documentTypeName = docType
  }

  setDocTypeStatus(docStatus) {
    this.doctypeStatus1 = docStatus
  }

  editDocType() {
    let userId = sessionStorage.getItem('userId')

    this._documentService.updateDocType(this.id, this.documentTypeName, this.doctypeStatus1, userId).subscribe(response => {
      this.router.navigate(['/back-office/document-type']);
    })
  }

  back() {
    this.router.navigate(['/back-office/document-type']);
  }


}
