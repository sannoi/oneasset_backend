import { Component, OnInit } from '@angular/core';
import { NewsletterService } from 'src/app/services/newsletter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';


// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-newsletter-edit',
  templateUrl: './newsletter-edit.component.html',
  styleUrls: ['./newsletter-edit.component.scss']
})
export class NewsletterEditComponent implements OnInit {


  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _Service: NewsletterService, private ngxService: NgxUiLoaderService) { }

  private sub: any;

  isTh: boolean
  isEn: boolean
  id: string;
  newsletterType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  newsletter: any;

  newsletterTtitleImage: any;
  newsletterBannerImage: any
  newsletterBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  newsletterFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typenewsletter: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusnewsletter: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editnewsletterForm: FormGroup;

  old_newsletterType: string;
  old_newsletterDetail: string;
  isPDF_old: any;

  vdo_id: any

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  ngOnInit() {
  // TODO: Public Date
  flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

    this._Service.getnewsletterByID(this.id).subscribe(response => {

      console.log(response)
      this.newsletter = response

      this.newsletterType = response[0]['newsletter_type']
      this.title = response[0]['title']
      this.title_image = response[0]['newsletter_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['newsletter_img_Thum']

      if (response[0]['newsletter_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

      if (response[0]['newsletter_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_newsletterType = response[0]['newsletter_type']
      this.old_newsletterDetail = response[0]['detail']

      if (response[0]['newsletter_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }
       // TODO: Public Date
       if (response[0]['publicDate']) {
        this.public_date = response[0]['publicDate'];
        this.publicDate  = response[0]['publicDate'];
      } else {
        this.public_date = new Date().toISOString().slice(0,10);
        this.publicDate  = new Date().toISOString().slice(0,10);
      }

      //=============== Binding Info ==================//
      this.editnewsletterForm = this.fb.group({
        newsletterTypeControl: this.newsletterType,
        newsletterTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date : this.public_date
      });
      //==============================================//
    })
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setnewsletterType(code) {
    this.newsletterType = code;

    if (this.newsletterType == this.old_newsletterType) {
      //=============== Binding Info ==================//
      this.editnewsletterForm = this.fb.group({
        newsletterTypeControl: this.newsletterType,
        newsletterTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_newsletterDetail,
        public_date : this.public_date
      });
      this.detail = this.old_newsletterDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editnewsletterForm = this.fb.group({
        newsletterTypeControl: this.newsletterType,
        newsletterTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        public_date : this.public_date
      });
      this.detail = "";
    }

  }
  setnewsletterTitle(title) { this.title = title; }
  setnewsletterFiles(event) {
    if (event.target.id === 'newsletterTitleImage') {
      this.newsletterTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'newsletterFile') {
      this.newsletterFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setnewsletterStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/newsletter']);
  }

  deleteBanner() {
    this._Service.delBannernewsletter(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/newsletter/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._Service.delBannernewsletter_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/newsletter/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = "";

  }

  setnewsletterBannerFiles(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.newsletterBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setnewsletterBannerFiles_mobile(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.newsletterBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editnewsletter() {



    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.newsletterType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._Service.uploadFile_title(this.newsletterTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }




            if (this.status === '1') {
              //==================== Banner Image ================================
              this._Service.uploadFile_mobile(this.newsletterBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                console.log(this.BannerImagepath_mobile)

                this._Service.uploadFile_banner(this.newsletterBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  console.log(this.BannerImagepath)

                  this._Service.updatenewsletter(this.title, this.newsletterType, editorHtmlTag, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/newsletter']);
                    })
                })

              })

            }
            else if (this.status === '0') {
              this._Service.delBannernewsletter(this.id).subscribe(response => {
                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";
                this._Service.updatenewsletter(this.title, this.newsletterType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/newsletter']);
                  })
              })
            }




          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }



        })




      } else if (this.newsletterType === 'image') {

        //==================== Title Image ================================
        this._Service.uploadFile_title(this.newsletterTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            //==================== File Download ================================
            this._Service.uploadFile(this.newsletterFiles).subscribe(response => {
              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              if (this.status === '1') {
                //==================== Banner Image ================================
                this._Service.uploadFile_mobile(this.newsletterBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._Service.uploadFile_banner(this.newsletterBannerImage).subscribe(response => {
                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._Service.updatenewsletter(this.title, this.newsletterType, this.FilePath, this.titleImagepath, userId,
                      this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/newsletter']);
                      })
                  })

                })

              }
              else if (this.status === '0') {
                this._Service.delBannernewsletter(this.id).subscribe(response => {
                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";
                  this._Service.updatenewsletter(this.title, this.newsletterType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/newsletter']);
                    })
                })
              }



            })




          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }


        })





      } else if (this.newsletterType === 'pdf') {

        //==================== Title Image ================================
        this._Service.uploadFile_title(this.newsletterTtitleImage).subscribe(response => {


          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }



            //==================== File Download ================================
            this._Service.uploadFile(this.newsletterFiles).subscribe(response => {
              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              if (this.status === '1') {
                //==================== Banner Image ================================
                this._Service.uploadFile_mobile(this.newsletterBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._Service.uploadFile_banner(this.newsletterBannerImage).subscribe(response => {
                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._Service.updatenewsletter(this.title, this.newsletterType, this.FilePath, this.titleImagepath,
                      userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/newsletter']);
                      })
                  })

                })

              }
              else if (this.status === '0') {
                this._Service.delBannernewsletter(this.id).subscribe(response => {
                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";
                  this._Service.updatenewsletter(this.title, this.newsletterType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/newsletter']);
                    })

                })

              }


            })

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })






      }
      else if (this.newsletterType === 'vdo') {

        //==================== Title Image ================================
        this._Service.uploadFile_title(this.newsletterTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            if (this.status === '1') {
              //==================== Banner Image ================================
              this._Service.uploadFile_mobile(this.newsletterBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._Service.uploadFile_banner(this.newsletterBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._Service.updatenewsletter(this.title, this.newsletterType, this.vdo_id,
                    this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/newsletter']);
                    })
                })

              })


            }
            else if (this.status === '0') {
              this._Service.delBannernewsletter(this.id).subscribe(response => {
                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";
                this._Service.updatenewsletter(this.title, this.newsletterType, this.vdo_id,
                  this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/newsletter']);
                  })
              })
            }


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }


        })


      }




    }



  }









}
