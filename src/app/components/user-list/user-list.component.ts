import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user.service'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  data: Array<any> = []
  groupList: any
  groupName: any
  username: any
  groupId: any

  userList: any
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  constructor(private router: Router, private pagerService: PagerService, private ngxService: NgxUiLoaderService,
    private userService: UserService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m11')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start()
    this.groupId = '1'
    this.userService.getUserGroupList('', 'y').subscribe(response => {
      this.groupList = response
      this.groupName = response[0].GroupName
      this.groupList.forEach(element => {
        this.data.push({
          groupId: element.Group_id,
          groupName: element.GroupName

        })
      });
      // call webservice
      this.userService.getUserList('', this.groupId).subscribe(response => {
        this.userList = response
        this.allItems = response
        // console.log(this.userList);

        this.setPage(1);
        this.ngxService.stop();
      })
    })
  }

  setSearch(userName) {
    this.username = userName
  }

  setGroupName(groupId) {

    this.groupId = parseInt(groupId)

  }

  keyDownFunction(event, search: string) {
    // 
    this.username = search
    if (event.keyCode == 13) {
      this.ngxService.start();
      if (this.username === undefined || this.username === '') {
        this.userService.getUserList('', this.groupId).subscribe(response => {
          this.userList = response
          this.allItems = response
          this.setPage(1);
          this.ngxService.stop();
        })
      } else {
        this.userService.getUserList(this.username, this.groupId).subscribe(response => {
          this.userList = response
          this.allItems = response
          this.setPage(1);
          this.ngxService.stop();
        })
      }
    }
  }

  getUserFromSerch() {
    this.ngxService.start();
    // call webservice
    if (this.username === undefined || this.username === '') {
      this.userService.getUserList('', this.groupId).subscribe(response => {
        this.userList = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    } else {
      this.userService.getUserList(this.username, this.groupId).subscribe(response => {
        this.userList = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  AddUser() {
    this.router.navigate(['/back-office/user-list/add'])
  }

  clickEdit(userId) {
    this.router.navigate(['/back-office/user-list/edit'], { queryParams: { id: userId } })
  }

}
