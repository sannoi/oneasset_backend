import { Component, OnInit } from '@angular/core';
import { DocumentService } from 'src/app/services/document.service';
import { PagerService } from 'src/app/_services/pager.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-fund-document-list',
  templateUrl: './fund-document-list.component.html',
  styleUrls: ['./fund-document-list.component.scss']
})
export class FundDocumentListComponent implements OnInit {

  docNameList: any

  docName: any
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================
  constructor(private router: Router, private docService: DocumentService, private ngxService: NgxUiLoaderService,
    private pagerService: PagerService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m9')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this.ngxService.start()
    this.docService.getFundDocName('').subscribe(response => {
      console.log('fund doc name list:', response);
      this.docNameList = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop()
    })

  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setSearch(searchValue) {
    this.docName = searchValue
  }

  getDocFromSearch() {
    this.ngxService.start();
    if (this.docName === undefined || this.docName === '') {
      this.docService.getFundDocName('').subscribe(response => {
        this.docNameList = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    } else {
      this.docService.getFundDocName(this.docName).subscribe(response => {
        this.docNameList = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  keyDownFunction(event, search: string) {
    this.docName = search
    if (event.keyCode == 13) {
      this.ngxService.start();
      if (this.docName === undefined || this.docName === '') {
        this.docService.getFundDocName('').subscribe(response => {
          this.docNameList = response
          this.allItems = response
          this.setPage(1);
          this.ngxService.stop();
        })
      } else {
        this.docService.getFundDocName(this.docName).subscribe(response => {
          this.docNameList = response
          this.allItems = response
          this.setPage(1);
          this.ngxService.stop();
        })
      }
    }
  }

  clickEdit(editId) {
    this.router.navigate(['/back-office/fund-detail/document-edit'], { queryParams: { id: editId } })
  }


  Add() {
    this.router.navigate(['back-office/fund-detail/document-add'])
  }

}
