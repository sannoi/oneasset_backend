import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DocumentService } from 'src/app/services/document.service'

@Component({
  selector: 'app-edit-fund-document',
  templateUrl: './edit-fund-document.component.html',
  styleUrls: ['./edit-fund-document.component.scss']
})
export class EditFundDocumentComponent implements OnInit {

  private sub: any;
  docId: any
  docNameDetail: any
  docNameTH: any
  docNameEN: any

  editDocNameForm: FormGroup

  constructor(private router: Router, private ngxService: NgxUiLoaderService,
    private docService: DocumentService, private fb: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit() {
    
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m9')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start()
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.docId = params['id'] || 0;
      });
    this.docService.getFundDocNameByID(this.docId).subscribe(response => {
      this.docNameDetail = response
      this.docNameTH = response[0].TH_name
      this.docNameEN = response[0].EN_name
      this.editDocNameForm = this.fb.group({
        nameTHCtrl: this.docNameTH,
        nameENCtrl: this.docNameEN
      })
      this.ngxService.stop()
    })
  }

  setNameTH(inpNameTH) {
    this.docNameTH = inpNameTH
  }

  editDocName() {
    this.docService.updateFundDocName(this.docNameTH, this.docNameEN, this.docId).subscribe(response => {
      this.router.navigate(['/back-office/fund-detail/document'])
    })
  }

  Back() {
    this.router.navigate(['/back-office/fund-detail/document'])
  }

}
