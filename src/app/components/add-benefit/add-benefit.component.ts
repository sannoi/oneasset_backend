import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BenefitService } from 'src/app/services/benefit.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-benefit',
  templateUrl: './add-benefit.component.html',
  styleUrls: ['./add-benefit.component.scss']
})
export class AddBenefitComponent implements OnInit {


  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };

  isTh: boolean
  isEn: boolean
  benefitType: string;
  typebenefit: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  benefitStatus: string;
  statusbenefit: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add benefit
  benefitTitle: string
  benefitTitleEN: string
  benefitTtitleImage: any
  benefitBannerImage: any
  benefitBannerImage_mobile: any
  imageTitleName: any
  benefitContentImage: any
  imageContentName: any
  benefitFiles: any
  benefitFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  BannerImagepath_mobile: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private _benefitService: BenefitService, private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }


  ngOnInit() {

        // TODO: Public Date
        flatpickr.localize(Thai);
        this.public_date = new Date().toISOString().slice(0,10);
        this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m2')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.benefitType = 'text';
    this.benefitStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }


  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setbenefitType(code) {
    // console.log("choice: ", code);
    this.benefitType = code;
  }

  setbenefitTitle(title: string) {
    this.benefitTitle = title
  }

  setbenefitTitleEN(title: string) {
    this.benefitTitleEN = title
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  setbenefitStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.benefitStatus = '1'
    } else if (code === 'Unpublish ') {
      this.benefitStatus = '0'
    }
  }

  setbenefitFiles(event) {
    if (event.target.id === 'benefitTitleImage') {
      this.benefitTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name

    } else if (event.target.id === 'benefitContentImage') {
      this.benefitContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name

    } else if (event.target.id === 'benefitFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.benefitFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }

    }
  }

  setbenefitBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.benefitBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setbenefitBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.benefitBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addbenefit() {
    let userId = sessionStorage.getItem('userId')

    if (this.benefitTitle == undefined || this.benefitTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else if (this.benefitTtitleImage == undefined || this.benefitTtitleImage == "") {
      alert("กรุณาเลือก Title Image !!");
      this.ngxService.stop();

    }
    else {
      this.ngxService.start();
      if (this.benefitType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._benefitService.uploadFile_title(this.benefitTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._benefitService.uploadFile_banner(this.benefitBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            //==================== Banner Image Mobile ================================
            this._benefitService.uploadFile_mobile(this.benefitBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._benefitService.addbenefit(this.benefitTitle, this.benefitType, editorHtmlTag, this.titleImagepath, userId,
                this.benefitStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/benefit']);
                })
            })
          })
        })







      }
      else if (this.benefitType === 'image') {
        //==================== Title Image ================================
        this._benefitService.uploadFile_title(this.benefitTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._benefitService.uploadFile(this.benefitContentImage).subscribe(response => {

            this.contentImagePath = response.toString();
            //==================== Banner Image ================================
            this._benefitService.uploadFile_banner(this.benefitBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();

              this._benefitService.uploadFile_mobile(this.benefitBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._benefitService.addbenefit(this.benefitTitle, this.benefitType, this.contentImagePath, this.titleImagepath,
                  userId, this.benefitStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/benefit']);
                  })

              })
            })
          })
        })




      } else if (this.benefitType === 'pdf') {
        if (this.benefitFileName == undefined || this.benefitFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._benefitService.uploadFile_title(this.benefitTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._benefitService.uploadFile(this.benefitFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._benefitService.uploadFile_banner(this.benefitBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._benefitService.uploadFile_mobile(this.benefitBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._benefitService.addbenefit(this.benefitTitle, this.benefitType, this.pdfFilePath, this.titleImagepath,
                    userId, this.benefitStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                      this.router.navigate(['/back-office/benefit']);

                    })
                })
              })
            })
          })


        }


      }
      else if (this.benefitType === 'vdo') {
        //==================== Title Image ================================
        this._benefitService.uploadFile_title(this.benefitTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._benefitService.uploadFile_banner(this.benefitBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._benefitService.uploadFile_mobile(this.benefitBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._benefitService.addbenefit(this.benefitTitle, this.benefitType, this.vdo_id, this.titleImagepath,
                userId, this.benefitStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/benefit']);

                })

            })
          })
        })



      }

      this.ngxService.stop();

    }





  }

  Back() {
    this.router.navigate(['/back-office/benefit']);
  }




}
