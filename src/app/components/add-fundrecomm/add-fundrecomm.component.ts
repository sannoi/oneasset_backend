import { Component, OnInit } from '@angular/core';
import { FundrecommendService } from 'src/app/services/fundrecommend.service';
import { Router } from '@angular/router';
import { Alert } from 'selenium-webdriver';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-fundrecomm',
  templateUrl: './add-fundrecomm.component.html',
  styleUrls: ['./add-fundrecomm.component.scss']
})
export class AddFundrecommComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  fundRecomType: string;
  typefundRecom: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  fundRecomStatus: string;
  statusfundRecom: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add fundRecom
  fundRecomTitle: string
  fundRecomTitleEN: string
  fundRecomTtitleImage: any
  fundRecomBannerImage: any
  imageTitleName: any
  fundRecomContentImage: any
  imageContentName: any
  fundRecomFiles: any
  fundRecomFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any
  vdo_id: any

  fundRecomBannerImage_mobile: any;
  BannerImagepath_mobile: any;

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private _fundrecommService: FundrecommendService, private router: Router,
    private ngxService: NgxUiLoaderService) { }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  ngOnInit() {
    // TODO: Public Date
    flatpickr.localize(Thai);
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.fundRecomType = 'text';
    this.fundRecomStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setfundRecomType(code) {
    // console.log("choice: ", code);
    this.fundRecomType = code;
  }

  setfundRecomTitle(title: string) {
    this.fundRecomTitle = title
  }

  setfundRecomTitleEN(title: string) {
    this.fundRecomTitleEN = title
  }

  setfundRecomStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.fundRecomStatus = '1'
    } else if (code === 'Unpublish ') {
      this.fundRecomStatus = '0'
    }
  }

  setfundRecomFiles(event) {
    if (event.target.id === 'fundRecomTitleImage') {
      this.fundRecomTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'fundRecomContentImage') {
      this.fundRecomContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'fundRecomFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.fundRecomFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }

    }
  }

  setfundRecomBannerFiles(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.fundRecomBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setfundRecomBannerFiles_mobile(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.fundRecomBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addfundRecom() {
    let userId = sessionStorage.getItem('userId')
    if (this.fundRecomTitle == undefined || this.fundRecomTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else if (this.fundRecomTtitleImage == undefined || this.fundRecomTtitleImage == "") {
      alert("กรุณาเลือก Title Image !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();

      if (this.fundRecomType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._fundrecommService.uploadFile_title(this.fundRecomTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._fundrecommService.uploadFile_banner(this.fundRecomBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._fundrecommService.uploadFile_mobile(this.fundRecomBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._fundrecommService.addfundRecom(this.fundRecomTitle, this.fundRecomType, editorHtmlTag, this.titleImagepath,
                userId, userId, this.fundRecomStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/fundrecommend']);
                })
            })

          })
        })





      }
      else if (this.fundRecomType === 'image') {
        //==================== Title Image ================================
        this._fundrecommService.uploadFile_title(this.fundRecomTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();


          //==================== File Download ================================
          this._fundrecommService.uploadFile(this.fundRecomContentImage).subscribe(response => {
            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._fundrecommService.uploadFile_banner(this.fundRecomBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();


              this._fundrecommService.uploadFile_mobile(this.fundRecomBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();


                this._fundrecommService.addfundRecom(this.fundRecomTitle, this.fundRecomType, this.contentImagePath, this.titleImagepath,
                  userId, userId, this.fundRecomStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/fundrecommend']);

                  })

              })
            })
          })
        })





      } else if (this.fundRecomType === 'pdf') {
        if (this.fundRecomFileName == undefined || this.fundRecomFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._fundrecommService.uploadFile_title(this.fundRecomTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._fundrecommService.uploadFile(this.fundRecomFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._fundrecommService.uploadFile_banner(this.fundRecomBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._fundrecommService.uploadFile_mobile(this.fundRecomBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._fundrecommService.addfundRecom(this.fundRecomTitle, this.fundRecomType, this.pdfFilePath, this.titleImagepath,
                    userId, userId, this.fundRecomStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/fundrecommend']);

                    })
                })
              })
            })

          })


        }







      }
      else if (this.fundRecomType === 'vdo') {
        //==================== Title Image ================================
        this._fundrecommService.uploadFile_title(this.fundRecomTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._fundrecommService.uploadFile_banner(this.fundRecomBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._fundrecommService.uploadFile_mobile(this.fundRecomBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._fundrecommService.addfundRecom(this.fundRecomTitle, this.fundRecomType, this.vdo_id, this.titleImagepath,
                userId, userId, this.fundRecomStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/fundrecommend']);

                })
            })
          })
        })






      }
      this.ngxService.stop();
    }

  }

  Back() {
    this.router.navigate(['/back-office/fundrecommend']);
  }








}
