import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user.service'

@Component({
  selector: 'app-add-user-list',
  templateUrl: './add-user-list.component.html',
  styleUrls: ['./add-user-list.component.scss']
})
export class AddUserListComponent implements OnInit {
  data: Array<any> = []
  groupList: any
  groupId: any
  userStatus: any[] = [
    { val: '1', name: 'Active' },
    { val: '0', name: 'Not Active' },

  ]

  username: any
  password: any
  firstName: any
  lastName: any
  status: any
  email: any

  constructor(private router: Router, private ngxService: NgxUiLoaderService,
    private userService: UserService) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m11')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this.ngxService.start()
    this.userService.getUserGroupList('', 'y').subscribe(response => {
      // console.log(response);
      this.groupId = response[0].Group_id
      this.groupList = response
      // console.log('grouplist',this.groupList);

      this.status = '1'
      this.groupList.forEach(element => {
        this.data.push({
          groupName: element.GroupName,
          Group_id: element.Group_id
        })
      });
      this.ngxService.stop();
    })
  }

  setGroupName(inpGroupId) {
    this.groupId = inpGroupId
  }

  setUserName(inpUsername) {
    this.username = inpUsername
  }

  setPassword(inpPassword) {
    this.password = inpPassword
  }

  setFname(inpFName) {
    this.firstName = inpFName
  }

  setLname(inpLname) {
    this.lastName = inpLname
  }

  setUserStatus(inpStatus) {
    this.status = inpStatus
  }

  setEmail(inpEmail) {
    this.email = inpEmail
  }

  addUser() {
    let userId = sessionStorage.getItem('userId')
    if (this.username === undefined || this.username == '') {
      alert('please enter username')
    } else if (this.password === undefined || this.password == '') {
      alert('please enter password')
    } else if (this.firstName === undefined || this.firstName == '') {
      alert('please enter firstName')
    } else if (this.lastName === undefined || this.lastName == '') {
      alert('please enter lastName')
    } else if (this.email === undefined || this.email == '') {
      alert('please enter email')
    } else {
      this.userService.addUser(this.username, this.password, this.firstName,
        this.lastName, userId, this.status, this.email, this.groupId).subscribe(response => {
          this.router.navigate(['/back-office/user-list'])
        })
    }

  }

  Back() {
    this.router.navigate(['/back-office/user-list'])
  }

}
