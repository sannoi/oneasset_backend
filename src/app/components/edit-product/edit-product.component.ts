import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  private sub: any;
  isTh: boolean
  isEn: boolean
  id: string;
  ProductType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  Product: any;

  ProductTtitleImage: any;
  ProductBannerImage: any
  ProductBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  ProductFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typeProduct: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusProduct: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editProductForm: FormGroup;

  old_productType: string;
  old_productDetail: string;
  isPDF_old: any;

  vdo_id: any

      // TODO: Public Date
      publicDate: any;
      public_date: any;


  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _productService: ProductService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.isTh = true
    this.isEn = false

    this._productService.getproductByID(this.id).subscribe(response => {

      this.Product = response

      this.ProductType = response[0]['product_type']
      this.title = response[0]['title']
      this.title_image = response[0]['product_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['product_img_Thum']

      if (response[0]['product_type'] != "text") {
        this.FilePath = response[0]['detail']
      }


      if (response[0]['product_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_productType = response[0]['product_type']
      this.old_productDetail = response[0]['detail']

      if (response[0]['product_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }

   // TODO: Public Date
   if (response[0]['publicDate']) {
    this.public_date = response[0]['publicDate'];
    this.publicDate  = response[0]['publicDate'];
  } else {
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = new Date().toISOString().slice(0,10);
  }
      //=============== Binding Info ==================//
      this.editProductForm = this.fb.group({
        ProductTypeControl: this.ProductType,
        ProductTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date : this.public_date
      });
      //==============================================//
    })
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setProductType(code) {
    this.ProductType = code;
    if (this.ProductType == this.old_productType) {
      //=============== Binding Info ==================//
      this.editProductForm = this.fb.group({
        ProductTypeControl: this.ProductType,
        ProductTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_productDetail,
        vdo_id: this.old_productDetail,
        public_date : this.public_date
      });
      this.detail = this.old_productDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editProductForm = this.fb.group({
        ProductTypeControl: this.ProductType,
        ProductTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date : this.public_date
      });
      this.detail = "";
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setProductTitle(title) { this.title = title; }
  setProductFiles(event) {
    if (event.target.id === 'ProductTitleImage') {
      this.ProductTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'ProductFile') {
      this.ProductFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setProductStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/product']);
  }

  deleteBanner() {
    this._productService.delBannerproduct(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/product/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._productService.delBannerproduct_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/product/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = "";

  }

  setProductBannerFiles(event) {
    var file_true = false;
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.ProductBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setProductBannerFiles_mobile(event) {
    var file_true = false;
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.ProductBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editProduct() {
    this.ngxService.start();
    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.ProductType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._productService.uploadFile_title(this.ProductTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._productService.uploadFile_mobile(this.ProductBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._productService.uploadFile_banner(this.ProductBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._productService.updateproduct(this.title, this.ProductType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/product']);
                  })
              })

            })

          } else if (this.status === '0') {
            this._productService.delBannerproduct(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._productService.updateproduct(this.title, this.ProductType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/product']);
                })
            })
          }



        })



      } else if (this.ProductType === 'image') {

        //==================== Title Image ================================
        this._productService.uploadFile_title(this.ProductTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._productService.uploadFile(this.ProductFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._productService.uploadFile_mobile(this.ProductBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._productService.uploadFile_banner(this.ProductBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._productService.updateproduct(this.title, this.ProductType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {

                      this.router.navigate(['/back-office/product']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._productService.delBannerproduct(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._productService.updateproduct(this.title, this.ProductType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {

                    this.router.navigate(['/back-office/product']);
                  })
              })
            }


          })




        })




      } else if (this.ProductType === 'pdf') {

        //==================== Title Image ================================
        this._productService.uploadFile_title(this.ProductTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._productService.uploadFile(this.ProductFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._productService.uploadFile_mobile(this.ProductBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._productService.uploadFile_banner(this.ProductBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._productService.updateproduct(this.title, this.ProductType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/product']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._productService.delBannerproduct(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._productService.updateproduct(this.title, this.ProductType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/product']);
                  })
              })
            }





          })




        })





      }

      else if (this.ProductType === 'vdo') {

        //==================== Title Image ================================
        this._productService.uploadFile_title(this.ProductTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== Banner Image ================================
          if (this.status === '1') {
            this._productService.uploadFile_mobile(this.ProductBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._productService.uploadFile_banner(this.ProductBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._productService.updateproduct(this.title, this.ProductType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/product']);
                  })
              })


            })

          } else if (this.status === '0') {
            this._productService.delBannerproduct(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._productService.updateproduct(this.title, this.ProductType, this.vdo_id, this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/product']);
                })
            })
          }




        })





      }



    }


    this.ngxService.stop();

  }

}
