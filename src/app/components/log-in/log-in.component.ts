import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserLoginService } from '../../services/user-login.service'
import { NgxUiLoaderService } from 'ngx-ui-loader'

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  username: string = ''
  password: string = ''
  alertDesc: string = ''
  showLoginAlert: boolean = false

  constructor(private router: Router,
    private loginService: UserLoginService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() { }

  clickLogin() {
    this.ngxService.start();
    event.preventDefault();
    this.loginService.getUserLogin(this.username, this.password).subscribe(response => {
      if (response['access_token'] && response['flag_reset'] !== 'y') {
        sessionStorage.setItem('userId', response['userid']);
        sessionStorage.setItem('loginUsername', this.username);
        localStorage.setItem('token', response['access_token']);
        this.ngxService.stop();
        this.router.navigate(['/back-office']);
      } else if (response['desc'] === 'success' && response['flag_reset'] === 'y') {
        sessionStorage.setItem('userId', response['userid']);
        localStorage.setItem('token', response['access_token']);
        this.ngxService.stop();
        this.router.navigate(['/back-office/reset-password']);
      } else {
        console.log('Not Correct');
        this.alertDesc = 'Username or Password incorrect';
        this.showLoginAlert = true;
        this.ngxService.stop();
        this.router.navigate(['/back-office/login']);
      }
    });
  }

  keyDownFunction(event, search: string) {
    if (event.keyCode == 13) {
      this.password = search

      this.ngxService.start();
      event.preventDefault()
      this.loginService.getUserLogin(this.username, this.password).subscribe(response => {
        if (response['access_token']) {
          console.log('Correct');
          localStorage.setItem('token', response['access_token']);
          this.ngxService.stop();
          this.router.navigate(['/back-office']);
        } else {
          this.alertDesc = 'Username or Password incorrect';
          this.showLoginAlert = true;
          this.ngxService.stop();
          this.router.navigate(['/back-office/login']);
        }
      })
    }
  }

  setValuePassword(password: string){
    this.password = password
  }





}
