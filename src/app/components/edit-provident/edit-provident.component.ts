import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-edit-provident',
  templateUrl: './edit-provident.component.html',
  styleUrls: ['./edit-provident.component.scss']
})
export class EditProvidentComponent implements OnInit {

  editProvidentForm: FormGroup
  resProvident: any
  provident: any

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _documentService: DocumentService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    this._documentService.getMaintain("Provident").subscribe(response => {
      console.log(response)
      // console.log(response);

      // this.r1 = response["r1"];
      this.resProvident = response["r1"];

      // this.r2 = response["r2"];
      // this.r3 = response["r3"];
      // this.r4 = response["r4"];
      // this.r5 = response["r5"];

      //=============== Binding Info ==================//
      this.editProvidentForm = this.fb.group({
        providentControl: this.resProvident
        // inflation2_control : this.r2,
        // inflation3_control : this.r3,
        // inflation4_control : this.r4,
        // inflation5_control : this.r5,
      });
      //==============================================//

    })

  }

  setProvident(providentChange) {
    this.provident = providentChange
  }

  editProvident() {

    this._documentService.UpdateMaintain("Provident", this.provident, "0", "0", "0", "0").subscribe(response => {
      this.router.navigate(['back-office/maintain'])
    })

  }

  Back() {
    this.router.navigate(['back-office/maintain'])
  }

}
