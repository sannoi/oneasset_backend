import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HoldingInvestmentUnitService } from 'src/app/services/holding-investment-unit.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-holding-investment-unit',
  templateUrl: './edit-holding-investment-unit.component.html',
  styleUrls: ['./edit-holding-investment-unit.component.scss']
})
export class EditHoldingInvestmentUnitComponent implements OnInit {

  private sub: any;
  isTh: boolean
  isEn: boolean
  id: string;
  HoldingInvestUnitType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  HoldingInvestUnit: any;

  HoldingInvestUnitTtitleImage: any;
  HoldingInvestUnitBannerImage: any
  HoldingInvestUnitBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  HoldingInvestUnitFiles: any;
  FilePath: string;


  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  typeHoldingInvestUnit: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusHoldingInvestUnit: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editHoldingInvestUnitForm: FormGroup;


  old_holdingInvestUnitType: string;
  old_holdingInvestUnitDetail: string;
  isPDF_old: any;

  vdo_id: any
  // TODO: Public Date
  publicDate: any;
  public_date: any;
  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _holdingInvestUnitService: HoldingInvestmentUnitService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
  // TODO: Public Date
  flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

    this._holdingInvestUnitService.getholdingInvestUnitByID(this.id).subscribe(response => {

      this.HoldingInvestUnit = response

      this.HoldingInvestUnitType = response[0]['holdingInvestUnit_type']
      this.title = response[0]['title']
      this.title_image = response[0]['holdingInvestUnit_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img']
      this.BannerImagepath_mobile = response[0]['banner_img']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['holdingInvestUnit_img_Thum']

      if (response[0]['holdingInvestUnit_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

      if (response[0]['holdingInvestUnit_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_holdingInvestUnitType = response[0]['holdingInvestUnit_type']
      this.old_holdingInvestUnitDetail = response[0]['detail']

      if (response[0]['holdingInvestUnit_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }
   // TODO: Public Date
   if (response[0]['publicDate']) {
    this.public_date = response[0]['publicDate'];
    this.publicDate  = response[0]['publicDate'];
  } else {
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = new Date().toISOString().slice(0,10);
  }
      //=============== Binding Info ==================//
      this.editHoldingInvestUnitForm = this.fb.group({
        HoldingInvestUnitTypeControl: this.HoldingInvestUnitType,
        HoldingInvestUnitTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }


  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setHoldingInvestUnitType(code) {
    this.HoldingInvestUnitType = code;
    if (this.HoldingInvestUnitType == this.old_holdingInvestUnitType) {
      //=============== Binding Info ==================//
      this.editHoldingInvestUnitForm = this.fb.group({
        HoldingInvestUnitTypeControl: this.HoldingInvestUnitType,
        HoldingInvestUnitTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_holdingInvestUnitDetail,
        public_date: this.public_date
      });
      this.detail = this.old_holdingInvestUnitDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editHoldingInvestUnitForm = this.fb.group({
        HoldingInvestUnitTypeControl: this.HoldingInvestUnitType,
        HoldingInvestUnitTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }
  setHoldingInvestUnitTitle(title) { this.title = title; }

  setHoldingInvestUnitFiles(event) {
    if (event.target.id === 'HoldingInvestUnitTitleImage') {
      this.HoldingInvestUnitTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'HoldingInvestUnitFile') {
      this.HoldingInvestUnitFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setHoldingInvestUnitStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/holding-investment-unit']);
  }

  deleteBanner() {
    this._holdingInvestUnitService.delBannerholdingInvestUnit(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/holding-investment-unit/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._holdingInvestUnitService.delBannerholdingInvestUnit(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/holding-investment-unit/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = "";

  }

  setHoldingInvestUnitBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.HoldingInvestUnitBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;

        }

      };
    }
  }

  setHoldingInvestUnitBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.HoldingInvestUnitBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editHoldingInvestUnit() {
    this.ngxService.start();

    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.HoldingInvestUnitType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._holdingInvestUnitService.uploadFile_title(this.HoldingInvestUnitTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== Banner Image ================================
          if (this.status === '1') {
            this._holdingInvestUnitService.uploadFile_mobile(this.HoldingInvestUnitBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._holdingInvestUnitService.uploadFile_banner(this.HoldingInvestUnitBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._holdingInvestUnitService.updateholdingInvestUnit(this.title, this.HoldingInvestUnitType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/holding-investment-unit']);
                  })

              })

            })

          } else if (this.status === '0') {
            this._holdingInvestUnitService.delBannerholdingInvestUnit(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._holdingInvestUnitService.updateholdingInvestUnit(this.title, this.HoldingInvestUnitType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/holding-investment-unit']);
                })

            })
          }


        })




      } else if (this.HoldingInvestUnitType === 'image') {

        //==================== Title Image ================================
        this._holdingInvestUnitService.uploadFile_title(this.HoldingInvestUnitTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._holdingInvestUnitService.uploadFile(this.HoldingInvestUnitFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._holdingInvestUnitService.uploadFile_mobile(this.HoldingInvestUnitBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._holdingInvestUnitService.uploadFile_banner(this.HoldingInvestUnitBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._holdingInvestUnitService.updateholdingInvestUnit(this.title, this.HoldingInvestUnitType,
                    this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                      this.router.navigate(['/back-office/holding-investment-unit']);

                    })

                })
              })

            } else if (this.status === '0') {
              this._holdingInvestUnitService.delBannerholdingInvestUnit(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._holdingInvestUnitService.updateholdingInvestUnit(this.title, this.HoldingInvestUnitType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                    this.router.navigate(['/back-office/holding-investment-unit']);

                  })

              })
            }



          })



        })




      } else if (this.HoldingInvestUnitType === 'pdf') {

        //==================== Title Image ================================
        this._holdingInvestUnitService.uploadFile_title(this.HoldingInvestUnitTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== File Download ================================
          this._holdingInvestUnitService.uploadFile(this.HoldingInvestUnitFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._holdingInvestUnitService.uploadFile_mobile(this.HoldingInvestUnitBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._holdingInvestUnitService.uploadFile_banner(this.HoldingInvestUnitBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._holdingInvestUnitService.updateholdingInvestUnit(this.title, this.HoldingInvestUnitType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/holding-investment-unit']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._holdingInvestUnitService.delBannerholdingInvestUnit(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._holdingInvestUnitService.updateholdingInvestUnit(this.title, this.HoldingInvestUnitType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/holding-investment-unit']);
                  })

              })
            }






          })


        })





      } else if (this.HoldingInvestUnitType === 'vdo') {

        //==================== Title Image ================================
        this._holdingInvestUnitService.uploadFile_title(this.HoldingInvestUnitTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== Banner Image ================================
          if (this.status === '1') {
            this._holdingInvestUnitService.uploadFile_mobile(this.HoldingInvestUnitBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._holdingInvestUnitService.uploadFile_banner(this.HoldingInvestUnitBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._holdingInvestUnitService.updateholdingInvestUnit(this.title, this.HoldingInvestUnitType, this.vdo_id,
                  this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/holding-investment-unit']);
                  })

              })

            })

          } else if (this.status === '0') {
            this._holdingInvestUnitService.delBannerholdingInvestUnit(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._holdingInvestUnitService.updateholdingInvestUnit(this.title, this.HoldingInvestUnitType, this.vdo_id,
                this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/holding-investment-unit']);
                })
            })
          }



        })





      }

    }



  }

}
