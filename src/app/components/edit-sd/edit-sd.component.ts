import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-edit-sd',
  templateUrl: './edit-sd.component.html',
  styleUrls: ['./edit-sd.component.scss']
})
export class EditSdComponent implements OnInit {

  editSDForm: FormGroup
  //response Expected Return
  resSDlvl1: any
  resSDlvl2: any
  resSDlvl3: any
  resSDlvl4: any
  resSDlvl5: any

  // edit expected return
  sdlvl1: any
  sdlvl2: any
  sdlvl3: any
  sdlvl4: any
  sdlvl5: any

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _documentService: DocumentService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    
    this._documentService.getMaintain("SD").subscribe(response => {
      this.resSDlvl1 = response["r1"];
      this.resSDlvl2 = response["r2"];
      this.resSDlvl3 = response["r3"];
      this.resSDlvl4 = response["r4"];
      this.resSDlvl5 = response["r5"];

      this.editSDForm = this.fb.group({
        lvl1Control: this.resSDlvl1,
        lvl2Control: this.resSDlvl2,
        lvl3Control: this.resSDlvl3,
        lvl4Control: this.resSDlvl4,
        lvl5Control: this.resSDlvl5,
      })

    })


  }

  setSDLvl1(value) {
    this.resSDlvl1 = value
  }
  setSDLvl2(value) {
    this.resSDlvl2 = value
  }
  setSDLvl3(value) {
    this.resSDlvl3 = value
  }
  setSDLvl4(value) {
    this.resSDlvl4 = value
  }
  setSDLvl5(value) {
    this.resSDlvl5 = value
  }

  editSD() {

    this._documentService.UpdateMaintain("SD", this.resSDlvl1, this.resSDlvl2, this.resSDlvl3,
    this.resSDlvl4, this.resSDlvl5).subscribe(response => {
      this.router.navigate(['back-office/maintain'])
    })

  }

  Back() {
    this.router.navigate(['/back-office/maintain'])
  }


}
