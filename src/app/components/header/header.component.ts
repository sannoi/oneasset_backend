import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserLoginService } from '../../services/user-login.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  username: any

  constructor(private router: Router, private userService: UserLoginService) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    let token  = localStorage.getItem('token');
    if (userId == null || token == null) {
      this.router.navigate(['/back-office/login'])
    }

    this.userService.checkTokenUserById(userId).subscribe(
      res => this.username = res,
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(['/back-office/login']);
          }
        }
      }
    );
  }

  userLogout() {
    sessionStorage.removeItem('userId');
    sessionStorage.removeItem('loginUsername');
    localStorage.removeItem('token');
    this.router.navigate(['/back-office/login']);
  }

  change_password() {

      this.router.navigate(['/back-office/reset-password'])

  }
}
