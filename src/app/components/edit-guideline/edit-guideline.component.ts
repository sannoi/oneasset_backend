import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GuidelineService } from 'src/app/services/guideline.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-guideline',
  templateUrl: './edit-guideline.component.html',
  styleUrls: ['./edit-guideline.component.scss']
})
export class EditGuidelineComponent implements OnInit {

  private sub: any;
  isTh: boolean
  isEn: boolean
  id: string;
  GuidelineType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  Guideline: any;

  GuidelineTtitleImage: any;
  GuidelineBannerImage: any
  GuidelineBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  GuidelineFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typeGuideline: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusGuideline: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editGuidelineForm: FormGroup;

  old_guidelineType: string;
  old_guidelineDetail: string;
  isPDF_old: any;

  vdo_id: any


    // TODO: Public Date
    publicDate: any;
    public_date: any;

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _guidelineService: GuidelineService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

    this._guidelineService.getguidelineByID(this.id).subscribe(response => {

      this.Guideline = response

      this.GuidelineType = response[0]['guideline_type']
      this.title = response[0]['title']
      this.title_image = response[0]['guideline_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      console.log(response[0]['banner_img_mobile']);

      if (response[0]['banner_img_mobile'] != null) {
        this.banner_img_mobile = response[0]['banner_img_mobile']
        this.BannerImagepath_mobile = response[0]['banner_img_mobile']
      }


      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['guideline_img_Thum']

      if (response[0]['guideline_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

         // TODO: Public Date
         if (response[0]['publicDate']) {
          this.public_date = response[0]['publicDate'];
          this.publicDate  = response[0]['publicDate'];
        } else {
          this.public_date = new Date().toISOString().slice(0,10);
          this.publicDate  = new Date().toISOString().slice(0,10);
        }

      if (response[0]['guideline_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_guidelineType = response[0]['guideline_type']
      this.old_guidelineDetail = response[0]['detail']

      if (response[0]['guideline_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }


      //=============== Binding Info ==================//
      this.editGuidelineForm = this.fb.group({
        GuidelineTypeControl: this.GuidelineType,
        GuidelineTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setGuidelineType(code) {
    this.GuidelineType = code;
    console.log(this.GuidelineType)
    if (this.GuidelineType == this.old_guidelineType) {
      //=============== Binding Info ==================//
      this.editGuidelineForm = this.fb.group({
        GuidelineTypeControl: this.GuidelineType,
        GuidelineTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_guidelineDetail,
        vdo_id: this.old_guidelineDetail,
        public_date: this.public_date
      });
      this.detail = this.old_guidelineDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editGuidelineForm = this.fb.group({
        GuidelineTypeControl: this.GuidelineType,
        GuidelineTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }
  setGuidelineTitle(title) { this.title = title; }
  setGuidelineFiles(event) {
    if (event.target.id === 'GuidelineTitleImage') {
      this.GuidelineTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'GuidelineFile') {
      this.GuidelineFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setGuidelineStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/guideline']);
  }

  deleteBanner() {
    this._guidelineService.delBannerguideline(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/guideline/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._guidelineService.delBannerguideline_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/guideline/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = "";


  }

  setGuidelineBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.GuidelineBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }


  setGuidelineBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.GuidelineBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }



  editGuideline() {
    this.ngxService.start();


    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      if (this.GuidelineType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._guidelineService.uploadFile_title(this.GuidelineTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._guidelineService.uploadFile_mobile(this.GuidelineBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._guidelineService.uploadFile_banner(this.GuidelineBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._guidelineService.updateguideline(this.title, this.GuidelineType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/guideline']);
                  })


              })

            })

          } else if (this.status === '0') {
            this._guidelineService.delBannerguideline(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._guidelineService.updateguideline(this.title, this.GuidelineType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/guideline']);
                })


            })
          }



        })



      } else if (this.GuidelineType === 'image') {

        //==================== Title Image ================================
        this._guidelineService.uploadFile_title(this.GuidelineTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== File Download ================================
          this._guidelineService.uploadFile(this.GuidelineFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._guidelineService.uploadFile_mobile(this.GuidelineBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._guidelineService.uploadFile_banner(this.GuidelineBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._guidelineService.updateguideline(this.title, this.GuidelineType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/guideline']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._guidelineService.delBannerguideline(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._guidelineService.updateguideline(this.title, this.GuidelineType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/guideline']);
                  })
              })
            }


          })



        })




      } else if (this.GuidelineType === 'pdf') {

        //==================== Title Image ================================
        this._guidelineService.uploadFile_title(this.GuidelineTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._guidelineService.uploadFile(this.GuidelineFiles).subscribe(response => {

            if (response.toString() != "") {
              this.FilePath = response.toString();
            }


            //==================== Banner Image ================================
            if (this.status === '1') {
              this._guidelineService.uploadFile_mobile(this.GuidelineBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._guidelineService.uploadFile_banner(this.GuidelineBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._guidelineService.updateguideline(this.title, this.GuidelineType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/guideline']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._guidelineService.delBannerguideline(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._guidelineService.updateguideline(this.title, this.GuidelineType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/guideline']);
                  })
              })
            }



          })






        })





      } else if (this.GuidelineType === 'vdo') {

        //==================== Title Image ================================
        this._guidelineService.uploadFile_title(this.GuidelineTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._guidelineService.uploadFile_mobile(this.GuidelineBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._guidelineService.uploadFile_banner(this.GuidelineBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._guidelineService.updateguideline(this.title, this.GuidelineType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/guideline']);
                  })

              })

            })

          } else if (this.status === '0') {
            this._guidelineService.delBannerguideline(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._guidelineService.updateguideline(this.title, this.GuidelineType, this.vdo_id, this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/guideline']);
                })
            })
          }




        })






      }

    }



  }

}
