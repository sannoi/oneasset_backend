import { Component, OnInit } from '@angular/core';
import { PagerService } from 'src/app/_services';
import { Router } from '@angular/router';
import { FundRecomMappService } from 'src/app/services/fund-recom-mapp.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-fund-recom-mapp',
  templateUrl: './fund-recom-mapp.component.html',
  styleUrls: ['./fund-recom-mapp.component.scss']
})
export class FundRecomMappComponent implements OnInit {

  constructor(private router: Router, private _Service: FundRecomMappService, private pagerService: PagerService,
    private ngxService: NgxUiLoaderService) { }

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];

  announceDate: any
  announces: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m7')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._Service.get_fundRecomMappList().subscribe(response => {
      this.announces = response
      this.allItems = response

      this.setPage(1);
      this.ngxService.stop();

    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  getNewsFromSerch() {
    this.ngxService.start();
    this._Service.get_fundRecomMappListByName(this.topicSearch).subscribe(response => {
      this.announces = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.announceDate = this._Service.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {
    console.log(xxx)
    this.ngxService.start();
    this._Service.Update_FundRecommStatus(xxx, "n").subscribe(response => {

      this._Service.get_fundRecomMappListByName(this.topicSearch).subscribe(response => {
        this.announces = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })



    })


    // this.router.navigate(['/back-office/fundRecom_map/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickmapping() {
    event.preventDefault()
    this.router.navigate(['/back-office/fundRecom_map/add'])

  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.announceDate = this._Service.date_thai(date)
    }
  }

  date_convert(date, short) {
    if (date === '') {
      return null
    } else {
      return this.announceDate = this._Service.date_convert(date, short)
    }
  }





}
