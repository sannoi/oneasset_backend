import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CorruptionService } from 'src/app/services/corruption.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';


@Component({
  selector: 'app-add-corruption',
  templateUrl: './add-corruption.component.html',
  styleUrls: ['./add-corruption.component.scss']
})
export class AddCorruptionComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };

  isTh: boolean
  isEn: boolean
  corruptionType: string;
  typecorruption: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  corruptionStatus: string;
  statuscorruption: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add corruption
  corruptionTitle: string
  corruptionTitleEN: string
  corruptionTtitleImage: any
  corruptionBannerImage: any
  imageTitleName: any
  corruptionContentImage: any
  imageContentName: any
  corruptionFiles: any
  corruptionFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any


  corruptionBannerImage_mobile: any
  BannerImagepath_mobile: any

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _corruptionService: CorruptionService, private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }

  ngOnInit() {
 // TODO: Public Date
 flatpickr.localize(Thai);
 this.public_date = new Date().toISOString().slice(0,10);
 this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.corruptionType = 'text';
    this.corruptionStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false

  }
  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }
  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setCorruptionType(code) {
    // console.log("choice: ", code);
    this.corruptionType = code;
  }

  setCorruptionTitle(title: string) {
    this.corruptionTitle = title
  }

  setCorruptionTitleEN(title: string) {
    this.corruptionTitleEN = title
  }

  setCorruptionStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.corruptionStatus = '1'
    } else if (code === 'Unpublish ') {
      this.corruptionStatus = '0'
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setCorruptionFiles(event) {
    if (event.target.id === 'CorruptionTitleImage') {
      this.corruptionTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name

    } else if (event.target.id === 'CorruptionContentImage') {
      this.corruptionContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name

    } else if (event.target.id === 'CorruptionFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.corruptionFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }


    }
  }

  setCorruptionBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.corruptionBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setCorruptionBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.corruptionBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addCorruption() {
    let userId = sessionStorage.getItem('userId')

    if (this.corruptionTitle == undefined || this.corruptionTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();

      if (this.corruptionType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._corruptionService.uploadFile_title(this.corruptionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();
          //==================== Banner Image ================================
          this._corruptionService.uploadFile_banner(this.corruptionBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            //==================== Banner Image ================================
            this._corruptionService.uploadFile_mobile(this.corruptionBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._corruptionService.addcorruption(this.corruptionTitle, this.corruptionType, editorHtmlTag, this.titleImagepath, userId,
                this.corruptionStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/corruption']);
                })
            })
          })
        })







      }
      else if (this.corruptionType === 'image') {
        //==================== Title Image ================================
        this._corruptionService.uploadFile_title(this.corruptionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._corruptionService.uploadFile(this.corruptionContentImage).subscribe(response => {

            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._corruptionService.uploadFile_banner(this.corruptionBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();

              this._corruptionService.uploadFile_mobile(this.corruptionBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._corruptionService.addcorruption(this.corruptionTitle, this.corruptionType, this.contentImagePath,
                  this.titleImagepath,
                  userId, this.corruptionStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/corruption']);

                  })
              })
            })
          })
        })



      } else if (this.corruptionType === 'pdf') {

        if (this.corruptionFileName == undefined || this.corruptionFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._corruptionService.uploadFile_title(this.corruptionTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._corruptionService.uploadFile(this.corruptionFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._corruptionService.uploadFile_banner(this.corruptionBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._corruptionService.uploadFile_mobile(this.corruptionBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._corruptionService.addcorruption(this.corruptionTitle, this.corruptionType, this.pdfFilePath, this.titleImagepath,
                    userId, this.corruptionStatus, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/corruption']);

                    })
                })
              })
            })
          })


        }








      }

      else if (this.corruptionType === 'vdo') {
        //==================== Title Image ================================
        this._corruptionService.uploadFile_title(this.corruptionTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._corruptionService.uploadFile_banner(this.corruptionBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._corruptionService.uploadFile_mobile(this.corruptionBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._corruptionService.addcorruption(this.corruptionTitle, this.corruptionType, this.vdo_id, this.titleImagepath,
                userId, this.corruptionStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/corruption']);

                })
            })
          })
        })





      }
      this.ngxService.stop();

    }


  }

  Back() {
    this.router.navigate(['/back-office/corruption']);
  }
}
