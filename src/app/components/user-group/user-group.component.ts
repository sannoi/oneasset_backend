import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user.service'

@Component({
  selector: 'app-user-group',
  templateUrl: './user-group.component.html',
  styleUrls: ['./user-group.component.scss']
})
export class UserGroupComponent implements OnInit {

  data: any[] = [
    { val: 'y', name: 'Active' },
    { val: 'n', name: 'Not Active' },
  ];

  topicSearch: any
  selectedLevel: any;
  userGroupList: any
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any = [];
  //============================================================

  constructor(private router: Router, private pagerService: PagerService, private ngxService: NgxUiLoaderService,
    private userService: UserService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m11')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.selectedLevel = 'y'
    this.ngxService.start();
    this.userService.getUserGroupList('', 'y').subscribe(response => {
      console.log(response);

      this.userGroupList = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })

  }

  setStatus(status) {
    console.log(status);
    this.selectedLevel = status
  }

  setSearch(value) {
    this.topicSearch = value
  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    console.log('keydown topic', this.topicSearch);
    console.log('keydown ', this.selectedLevel);

    if (event.keyCode == 13) {
      this.ngxService.start();

      this.userService.getUserGroupList(this.topicSearch, this.selectedLevel).subscribe(response => {
        this.userGroupList = response
        console.log(this.userGroupList)
        this.allItems = response
        console.log('keydown', response);

        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  getUserGoupFromSerch() {
    console.log('get from search title', this.topicSearch);
    console.log('get from search status ', this.selectedLevel);

    this.ngxService.start();
    this.userService.getUserGroupList(this.topicSearch, this.selectedLevel).subscribe(response => {

      console.log(response);

      this.userGroupList = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();

    })

  }

  setPage(page: number) {

    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    console.log('setpage pagedItems', this.pagedItems);
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

  }

  clickEdit(groupId) {
    this.router.navigate(['/back-office/user-group/edit'], { queryParams: { id: groupId } })
  }


  clickDel(groupId) {

    this.userService.delUserGroup(groupId).subscribe(response => {
      console.log(response);
      if (response != "success") {
        alert(response);
      }
      else {
        this.getUserGoupFromSerch();
      }
    })

  }

  clickAddUserGroup() {
    this.router.navigate(['/back-office/user-group/add'])
  }

  clickMapping(groupName, groupId) {
    this.router.navigate(['/back-office/user-group/menu-mapping'], { queryParams: { groupName: groupName, id: groupId } })
  }

}
