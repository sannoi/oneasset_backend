import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FundrecommendService } from 'src/app/services/fundrecommend.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-fundrecomm',
  templateUrl: './edit-fundrecomm.component.html',
  styleUrls: ['./edit-fundrecomm.component.scss']
})
export class EditFundrecommComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _fundRecomService: FundrecommendService, private ngxService: NgxUiLoaderService) { }

  private sub: any;

  isTh: boolean
  isEn: boolean
  id: string;
  fundRecomType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  fundRecom: any;

  fundRecomTtitleImage: any;
  fundRecomBannerImage: any
  fundRecomBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  fundRecomFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typefundRecom: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusfundRecom: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editfundRecomForm: FormGroup;

  old_fundRecomType: string;
  old_fundRecomDetail: string;
  isPDF_old: any;

  vdo_id: any

      // TODO: Public Date
      publicDate: any;
      public_date: any;

  ngOnInit() {
    // TODO: Public Date
    flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

    this._fundRecomService.getfundRecomByID(this.id).subscribe(response => {

      this.fundRecom = response

      console.log(response)

      this.fundRecomType = response[0]['fundRecom_type']
      this.title = response[0]['title']
      this.title_image = response[0]['fundRecom_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['fundRecom_img_Thum']

      if (response[0]['fundRecom_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

  // TODO: Public Date
        // TODO: Public Date
        if (response[0]['publicDate']) {
          this.public_date = response[0]['publicDate'];
          this.publicDate  = response[0]['publicDate'];
        } else {
          this.public_date = new Date().toISOString().slice(0,10);
          this.publicDate  = new Date().toISOString().slice(0,10);
        }
      if (response[0]['fundRecom_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_fundRecomType = response[0]['fundRecom_type']
      this.old_fundRecomDetail = response[0]['detail']

      if (response[0]['fundRecom_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }


      //=============== Binding Info ==================//
      this.editfundRecomForm = this.fb.group({
        fundRecomTypeControl: this.fundRecomType,
        fundRecomTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }


    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setfundRecomType(code) {
    this.fundRecomType = code;
    if (this.fundRecomType == this.old_fundRecomType) {
      //=============== Binding Info ==================//
      this.editfundRecomForm = this.fb.group({
        fundRecomTypeControl: this.fundRecomType,
        fundRecomTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_fundRecomDetail,
        vdo_id: this.old_fundRecomDetail,
        public_date: this.public_date
      });
      this.detail = this.old_fundRecomDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editfundRecomForm = this.fb.group({
        fundRecomTypeControl: this.fundRecomType,
        fundRecomTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }
  setfundRecomTitle(title) { this.title = title; }
  setfundRecomFiles(event) {
    if (event.target.id === 'fundRecomTitleImage') {
      this.fundRecomTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'fundRecomFile') {
      this.fundRecomFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setfundRecomStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/fundrecommend']);
  }

  deleteBanner() {
    this._fundRecomService.delBannerfundRecom(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/fundrecommend/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._fundRecomService.delBannerfundRecom_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/fundrecommend/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = ""

  }


  setfundRecomBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.fundRecomBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setfundRecomBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.fundRecomBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editfundRecom() {
    let userId = sessionStorage.getItem('userId')

    this.ngxService.start();

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {
      if (this.fundRecomType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._fundRecomService.uploadFile_title(this.fundRecomTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._fundRecomService.uploadFile_mobile(this.fundRecomBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._fundRecomService.uploadFile_banner(this.fundRecomBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._fundRecomService.updatefundRecom(this.title, this.fundRecomType, editorHtmlTag, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/fundrecommend']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._fundRecomService.delBannerfundRecom(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._fundRecomService.updatefundRecom(this.title, this.fundRecomType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/fundrecommend']);
                  })

              })
            }
          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }


        })




      } else if (this.fundRecomType === 'image') {

        //==================== Title Image ================================
        this._fundRecomService.uploadFile_title(this.fundRecomTtitleImage).subscribe(response => {


          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== File Download ================================
            this._fundRecomService.uploadFile(this.fundRecomFiles).subscribe(response => {

              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              //==================== Banner Image ================================
              if (this.status === '1') {
                this._fundRecomService.uploadFile_mobile(this.fundRecomBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._fundRecomService.uploadFile_banner(this.fundRecomBannerImage).subscribe(response => {

                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._fundRecomService.updatefundRecom(this.title, this.fundRecomType, this.FilePath, this.titleImagepath, userId,
                      this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/fundrecommend']);
                      })

                  })

                })

              } else if (this.status === '0') {
                this._fundRecomService.delBannerfundRecom(this.id).subscribe(response => {

                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";

                  this._fundRecomService.updatefundRecom(this.title, this.fundRecomType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/fundrecommend']);
                    })

                })
              }



            })




          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }



        })




      } else if (this.fundRecomType === 'pdf') {

        //==================== Title Image ================================
        this._fundRecomService.uploadFile_title(this.fundRecomTtitleImage).subscribe(response => {


          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== File Download ================================
            this._fundRecomService.uploadFile(this.fundRecomFiles).subscribe(response => {

              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              //==================== Banner Image ================================
              if (this.status === '1') {
                this._fundRecomService.uploadFile_mobile(this.fundRecomBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._fundRecomService.uploadFile_banner(this.fundRecomBannerImage).subscribe(response => {

                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._fundRecomService.updatefundRecom(this.title, this.fundRecomType, this.FilePath, this.titleImagepath,
                      userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/fundrecommend']);
                      })
                  })

                })

              } else if (this.status === '0') {
                this._fundRecomService.delBannerfundRecom(this.id).subscribe(response => {

                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";

                  this._fundRecomService.updatefundRecom(this.title, this.fundRecomType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/fundrecommend']);
                    })
                })
              }


            })
          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }




        })





      } else if (this.fundRecomType === 'vdo') {

        //==================== Title Image ================================
        this._fundRecomService.uploadFile_title(this.fundRecomTtitleImage).subscribe(response => {


          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._fundRecomService.uploadFile_mobile(this.fundRecomBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._fundRecomService.uploadFile_banner(this.fundRecomBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._fundRecomService.updatefundRecom(this.title, this.fundRecomType, this.vdo_id, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/fundrecommend']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._fundRecomService.delBannerfundRecom(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._fundRecomService.updatefundRecom(this.title, this.fundRecomType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/fundrecommend']);
                  })

              })
            }

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }




        })





      }
    }



    this.ngxService.stop();

  }






}
