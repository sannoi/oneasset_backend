import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NewsService } from 'src/app/services/news.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.scss']
})

export class EditNewsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _newsService: NewsService, private ngxService: NgxUiLoaderService) { }

  private sub: any;

  isTh: boolean
  isEn: boolean
  id: string;
  newsType: string;
  title: string;
  title_image: string;
  title_image_mobile: string
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  detail_mobile: string
  news: any;

  newsTtitleImage: any;
  newsTtitleImage_mobile: any;
  newsBannerImage: any;
  newsBannerImage_mobile: any;
  titleImagepath: string;
  titleImagepath_mobile: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  newsFiles: any;
  newsFiles_mobile: any;
  FilePath: string;
  FilePath_mobile: string;

  old_newsType: string;
  old_newsDetail: string;
  isPDF_old: any;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  typenews: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusnews: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editNewsForm: FormGroup;

  vdo_id: any

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  ngOnInit() {
    // TODO: Public Date
    flatpickr.localize(Thai);

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == 'false') {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.isTh = true
    this.isEn = false

    this._newsService.getNewsByID(this.id).subscribe(response => {

      this.news = response
      console.log(this.news);
      this.newsType = response[0]['news_type']
      this.old_newsType = response[0]['news_type']

      this.title = response[0]['tItle'];
      this.title_image = response[0]['title_img']
      this.title_image_mobile = response[0]['title_img_mobile']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']
      this.detail_mobile = response[0]['Detail_mobile']
      this.old_newsDetail = response[0]['detail']

      this.titleImagepath = response[0]['title_img']

      // TODO: Public Date
      if (response[0]['publicDate']) {
        this.public_date = response[0]['publicDate'];
        this.publicDate  = response[0]['publicDate'];
      } else {
        this.public_date = new Date().toISOString().slice(0,10);
        this.publicDate  = new Date().toISOString().slice(0,10);
      }

      if (response[0]['news_type'] != 'text') {
        this.FilePath = response[0]['detail']
      }

      if (response[0]['news_type'] == 'vdo') {
        this.vdo_id = response[0]['detail']
      }

      if (response[0]['news_type'] == 'pdf') {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }


      //=============== Binding Info ==================//
      this.editNewsForm = this.fb.group({
        NewsTypeControl: this.newsType,
        newsTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });

      //==============================================//

    })

  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setNewsTitleTH(title: string) {
    this.title = title
  }

  setNewsTitleEN(title: string) {
    this.title = title
  }

  setNewsType(code) {
    this.newsType = code;
    if (this.newsType == this.old_newsType) {
      //=============== Binding Info ==================//
      this.editNewsForm = this.fb.group({
        NewsTypeControl: this.newsType,
        newsTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_newsDetail,
        vdo_id: this.old_newsDetail,
        public_date: this.public_date
      });
      this.detail = this.old_newsDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editNewsForm = this.fb.group({
        NewsTypeControl: this.newsType,
        newsTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: '',
        vdo_id: '',
        public_date: this.public_date
      });
      this.detail = '';
    }
  }

  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace('https://www.youtube.com/watch?v=', '');
  }

  setNewsFiles(event) {
    if (event.target.id === 'newsTitleImage') {
      this.newsTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'newsTitleImageMobile') {
      this.newsTtitleImage_mobile = <File>event.target.files[0]
      this.titleImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name
    } else if (event.target.id === 'newsFile_mobile') {
      this.newsFiles_mobile = <File>event.target.files[0]
      this.FilePath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name
    } else if (event.target.id === 'newsFile') {
      this.newsFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setNewsStatus(code) { this.status = code }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  Back() {
    this.router.navigate(['/back-office/news']);
  }

  deletetitleImg() {

    this.title_image = '';
    this.titleImagepath = '';

  }

  deleteBanner() {
    this._newsService.delBannerNews(this.id).subscribe(response => {
      this.banner_img = '';
      this.BannerImagepath = '';
      this.router.navigate(['/back-office/news/edit'], { queryParams: { id: this.id } });
    })
  }

  deleteBanner_mobile() {
    this._newsService.delBannerNews_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = '';
      this.BannerImagepath_mobile = '';
      this.router.navigate(['/back-office/news/edit'], { queryParams: { id: this.id } });
    })
  }

  setNewsBannerFiles(event) {

    var file_true = false;
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert('ภาพ Banner ต้องมีขนาด 2880 x 1102 px')
        }
        else if (img.height != 1102) {
          alert('ภาพ Banner ต้องมีขนาด 2880 x 1102 px')
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.newsBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setNewsBannerFiles_mobile(event) {

    var file_true = false;
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.newsBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editNews() {
    this.ngxService.start();
    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == '') {
      alert('กรุณากรอก Title !!');
      this.ngxService.stop();

    }
    else {


      if (this.newsType === 'text') {

        let e = (document.getElementsByClassName('note-editable')) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._newsService.uploadFile_title(this.newsTtitleImage).subscribe(response => {

          if (response.toString() != '' || this.title_image != '') {
            if (response.toString() == '') {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== Banner Image ================================
            if (this.status === '1') {
              this._newsService.uploadFile_mobile(this.newsBannerImage_mobile).subscribe(response => {

                if (response.toString() != '') {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._newsService.uploadFile_banner(this.newsBannerImage).subscribe(response => {

                  if (response.toString() != '') {
                    this.BannerImagepath = response.toString();
                  }

                  this._newsService.updateNews(this.title, this.newsType, editorHtmlTag, '', this.titleImagepath,
                    this.titleImagepath_mobile, userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.isPDF_old = false
                      this.router.navigate(['/back-office/news']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._newsService.delBannerNews(this.id).subscribe(response => {

                this.banner_img = '';
                this.BannerImagepath = '';

                this.banner_img_mobile = '';
                this.BannerImagepath_mobile = '';

                this._newsService.updateNews(this.title, this.newsType, editorHtmlTag, '', this.titleImagepath,
                  this.titleImagepath_mobile, userId, this.status, this.id, this.BannerImagepath,
                  this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.isPDF_old = false
                    this.router.navigate(['/back-office/news']);
                  })

              })

            }


          }
          else {
            alert('กรุณาเลือก Title Image !!');
            this.ngxService.stop();

          }

        })




      } else if (this.newsType === 'image') {

        //==================== Title Image ================================
        this._newsService.uploadFile_title(this.newsTtitleImage).subscribe(response => {

          console.log(this.title_image)

          if (response.toString() != '' || this.title_image != '') {
            if (response.toString() == '') {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== File Download ================================
            this._newsService.uploadFile(this.newsFiles).subscribe(response => {


              if (response.toString() != '') {
                this.FilePath = response.toString();
              }



              //==================== Banner Image ================================
              if (this.status === '1') {
                this._newsService.uploadFile_mobile(this.newsBannerImage_mobile).subscribe(response => {

                  if (response.toString() != '') {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._newsService.uploadFile_banner(this.newsBannerImage).subscribe(response => {

                    if (response.toString() != '') {
                      this.BannerImagepath = response.toString();
                    }

                    this._newsService.updateNews(this.title, this.newsType, this.FilePath, this.FilePath_mobile,
                      this.titleImagepath, this.titleImagepath_mobile, userId, this.status, this.id, this.BannerImagepath,
                      this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.isPDF_old = false
                        this.router.navigate(['/back-office/news']);
                      })

                  })

                })

              } else if (this.status === '0') {
                this._newsService.delBannerNews(this.id).subscribe(response => {

                  this.banner_img = '';
                  this.BannerImagepath = '';

                  this.banner_img_mobile = '';
                  this.BannerImagepath_mobile = '';

                  this._newsService.updateNews(this.title, this.newsType, this.FilePath, this.FilePath_mobile,
                    this.titleImagepath, this.titleImagepath_mobile, userId, this.status, this.id,
                    this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.isPDF_old = false
                      this.router.navigate(['/back-office/news']);
                    })

                })
              }

            })


          }
          else {
            alert('กรุณาเลือก Title Image !!');
            this.ngxService.stop();

          }







        })

      } else if (this.newsType === 'pdf') {

        //==================== Title Image ================================
        this._newsService.uploadFile_title(this.newsTtitleImage).subscribe(response => {

          if (response.toString() != '' || this.title_image != '') {
            if (response.toString() == '') {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== File Download ================================
            this._newsService.uploadFile(this.newsFiles).subscribe(response => {

              if (response.toString() != '') {
                this.FilePath = response.toString();
              }

              //==================== Banner Image ================================
              if (this.status === '1') {
                this._newsService.uploadFile_mobile(this.newsBannerImage_mobile).subscribe(response => {

                  if (response.toString() != '') {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._newsService.uploadFile_banner(this.newsBannerImage).subscribe(response => {

                    if (response.toString() != '') {
                      this.BannerImagepath = response.toString();
                    }

                    this._newsService.updateNews(this.title, this.newsType, this.FilePath, '', this.titleImagepath,
                      this.titleImagepath_mobile, userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.isPDF_old = true
                        this.router.navigate(['/back-office/news']);
                      })
                  })

                })

              } else if (this.status === '0') {
                this._newsService.delBannerNews(this.id).subscribe(response => {

                  this.banner_img = '';
                  this.BannerImagepath = '';

                  this.banner_img_mobile = '';
                  this.BannerImagepath_mobile = '';

                  this._newsService.updateNews(this.title, this.newsType, this.FilePath, '', this.titleImagepath,
                    this.titleImagepath_mobile, userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.isPDF_old = true
                      this.router.navigate(['/back-office/news']);
                    })

                })
              }

            })


          }
          else {
            alert('กรุณาเลือก Title Image !!');
            this.ngxService.stop();

          }


        })





      }
      else if (this.newsType === 'vdo') {

        //==================== Title Image ================================
        this._newsService.uploadFile_title(this.newsTtitleImage).subscribe(response => {

          if (response.toString() != '' || this.title_image != '') {
            if (response.toString() == '') {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }



            //==================== Banner Image ================================
            if (this.status === '1') {
              this._newsService.uploadFile_mobile(this.newsBannerImage_mobile).subscribe(response => {

                if (response.toString() != '') {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._newsService.uploadFile_banner(this.newsBannerImage).subscribe(response => {

                  if (response.toString() != '') {
                    this.BannerImagepath = response.toString();
                  }

                  this._newsService.updateNews(this.title, this.newsType, this.vdo_id, '', this.titleImagepath,
                    this.titleImagepath_mobile, userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.isPDF_old = false
                      this.router.navigate(['/back-office/news']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._newsService.delBannerNews(this.id).subscribe(response => {

                this.banner_img = '';
                this.BannerImagepath = '';

                this.banner_img_mobile = '';
                this.BannerImagepath_mobile = '';

                this._newsService.updateNews(this.title, this.newsType, this.vdo_id, '', this.titleImagepath,
                  this.titleImagepath_mobile, userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.isPDF_old = false
                    this.router.navigate(['/back-office/news']);
                  })

              })
            }




          }
          else {
            alert('กรุณาเลือก Title Image !!');
            this.ngxService.stop();

          }


        })



      }


    }

    this.ngxService.stop();


  }



}
