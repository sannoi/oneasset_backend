import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-sd',
  templateUrl: './add-sd.component.html',
  styleUrls: ['./add-sd.component.scss']
})
export class AddSdComponent implements OnInit {

  // SD
  sdlvl1: any
  sdlvl2: any
  sdlvl3: any
  sdlvl4: any
  sdlvl5: any

  constructor(private router: Router) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
  }

  addSD() {
    console.log(parseFloat(this.sdlvl1),
      parseFloat(this.sdlvl2), parseFloat(this.sdlvl3),
      parseFloat(this.sdlvl4), parseFloat(this.sdlvl5));
    this.router.navigate(['/back-office/maintain'])
  }

  Back() {
    this.router.navigate(['/back-office/maintain'])
  }

}
