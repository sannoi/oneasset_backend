import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DocumentService } from 'src/app/services/document.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-add-fund-document',
  templateUrl: './add-fund-document.component.html',
  styleUrls: ['./add-fund-document.component.scss']
})
export class AddFundDocumentComponent implements OnInit {

  docNameTH: any
  docNameEN: any

  constructor(private router: Router, private docService: DocumentService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m9')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this.ngxService.start()
    this.ngxService.stop()
  }

  setDocNameTH(inpDocNameTH) {
    this.docNameTH = inpDocNameTH
  }

  setDocNameEN(inpDocNameEN) {
    this.docNameEN = inpDocNameEN
  }

  addDocName() {
    if (this.docNameTH == undefined || this.docNameTH == '') {
      alert('กรุณากรอกชื่อไฟล์ภาษาไทย')
    } else if (this.docNameEN == undefined || this.docNameEN == '') {
      alert('กรุณากรอกชื่อไฟล์ภาษาอังกฤษ')
    }
    else {
      this.docService.addFundDocName(this.docNameTH, this.docNameEN).subscribe(resposne => {
        this.router.navigate(['/back-office/fund-detail/document'])
      })
    }
  }

  Back() {
    this.router.navigate(['/back-office/fund-detail/document'])
  }

}
