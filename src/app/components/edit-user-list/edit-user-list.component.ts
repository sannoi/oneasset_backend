import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-user-list',
  templateUrl: './edit-user-list.component.html',
  styleUrls: ['./edit-user-list.component.scss']
})
export class EditUserListComponent implements OnInit {
  private sub: any;
  user: any
  userId: any
  userObject: any
  userGroupId: any
  // username: any
  password: any
  firstName: any
  lastName: any
  status: any
  email: any

  groupList: any
  data: Array<any> = []
  userStatus: any[] = [
    { val: '1', name: 'Active' },
    { val: '0', name: 'Not Active' },
  ]

  resetPwd: boolean

  editUserForm: FormGroup

  constructor(private router: Router, private ngxService: NgxUiLoaderService,
    private userService: UserService, private fb: FormBuilder, private route: ActivatedRoute) {
    this.resetPwd = false
  }

  ngOnInit() {
    
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m11')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start()
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.userId = params['id'] || 0;
      });

    this.userService.getUserGroupList('', 'y').subscribe(response => {
      // console.log(response);
      this.groupList = response
      this.status = '1'

      // bind data to drop down
      this.groupList.forEach(element => {
        this.data.push({
          groupName: element.GroupName,
          groupId: element.Group_id
        })
      });

      this.userService.getUserById(this.userId).subscribe(response => {
        this.user = response
        this.userGroupId = response['group_id']
        this.status = response['IsActive']
        this.firstName = response['Fname']
        this.lastName = response['Lname']
        this.email = response['Email']
        // this.username = 
        this.editUserForm = this.fb.group({
          groupCtrl: this.userGroupId,
          passwordCtrl: this.password,
          fnameCtrl: this.firstName,
          lnameCtrl: this.lastName,
          statusCtrl: this.status,
          emailCtrl: this.email
        })

      })
      this.ngxService.stop();
    })
  }

  resetPass() {
    this.resetPwd = true
  }

  setGroupId(inpGroupId) {
    this.userGroupId = inpGroupId
  }

  setPassword(inpPassword) {
    this.password = inpPassword
  }

  setFname(inpFName) {
    this.firstName = inpFName
  }

  setLname(inpLname) {
    this.lastName = inpLname
  }

  setUserStatus(inpStatus) {
    this.status = inpStatus
  }

  setEmail(inpEmail) {
    this.email = inpEmail
  }

  editUser() {
    let loginId = sessionStorage.getItem('userId')
    // call update user service
    this.userService.updateUser(this.userId, this.firstName,
      this.lastName, loginId, this.status, this.email, this.userGroupId).subscribe(response => {
        if (this.resetPwd === true) {
          this.userService.resetPassword(this.userId, this.password).subscribe(resposne => {
            this.router.navigate(['/back-office/user-list'])
          })
        } else {
          this.router.navigate(['/back-office/user-list'])
        }
      })
    // this.ngxService.stop()
  }

  Back() {
    this.router.navigate(['/back-office/user-list'])
  }

}
