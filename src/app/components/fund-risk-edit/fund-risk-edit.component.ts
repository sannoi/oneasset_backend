import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { FundRiskService } from 'src/app/services/fund-risk.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-fund-risk-edit',
  templateUrl: './fund-risk-edit.component.html',
  styleUrls: ['./fund-risk-edit.component.scss']
})
export class FundRiskEditComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _Service: FundRiskService, private ngxService: NgxUiLoaderService) { }

  fundSelectList: Array<any> = []
  delSelectList: Array<any> = []

  private sub: any;
  id: string;
  fundRisk: any;
  title: any;
  editRiskForm: any;
  riskDetail1_1: any;
  riskDetail1_1P: any;
  riskDetail1_2: any;
  riskDetail1_2P: any;
  riskLevelName: any;
  riskDetail2p: any;

  riskDetail3_1: any;
  riskDetail3_1P: any;
  riskDetail3_2: any;
  riskDetail3_2P: any;

  p1: any;
  p2: any;
  p3: any;
  p4: any;
  p5: any;
  p6: any;

  choiceFromSelect: any;
  Image: any;

  fundNotMapp1: any;
  fundNotMapp2: any;
  fundNotMapp3: any;
  fundNotMapp4: any;
  fundNotMapp5: any;
  fundNotMapp6: any;

  fundMapp1: any;
  fundMapp2: any;
  fundMapp3: any;
  fundMapp4: any;
  fundMapp5: any;
  fundMapp6: any;

  fundName1: any;
  fundName2: any;
  fundName3: any;
  fundName4: any;
  fundName5: any;
  fundName6: any;

  riskTitleImage: any;
  titleImagepath: any;

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m8')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    this.choiceFromSelect = "display-form";

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this._Service.get_riskLevelByRiskID(this.id).subscribe(response => {

      console.log(response)
      this.fundRisk = response
      this.title = response[0]['Title']

      this.riskDetail1_1 = response[0]['riskDetail1_1']
      this.riskDetail1_1P = response[0]['riskDetail1_1p']

      this.riskDetail1_2 = response[0]['riskDetail1_2']
      this.riskDetail1_2P = response[0]['riskDetail1_2p']

      this.riskDetail3_1 = response[0]['riskDetail3_1']
      this.riskDetail3_1P = response[0]['riskDetail3_1p']

      this.riskDetail3_2 = response[0]['riskDetail3_2']
      this.riskDetail3_2P = response[0]['riskDetail3_2p']

      this.riskLevelName = response[0]['riskLevelName']
      this.riskDetail2p = response[0]['riskDetail2p']

      this.Image = response[0]['Image']
      this.titleImagepath = response[0]['Image']

      this.p1 = response[0]['p1']
      this.p2 = response[0]['p2']
      this.p3 = response[0]['p3']
      this.p4 = response[0]['p4']
      this.p5 = response[0]['p5']
      this.p6 = response[0]['p6']


      //=============== Binding Info ==================//
      this.editRiskForm = this.fb.group({
        riskTitleControl: this.title,
        detail1_1Control: this.riskDetail1_1,
        detail1_1PControl: this.riskDetail1_1P,
        detail1_2Control: this.riskDetail1_2,
        detail1_2PControl: this.riskDetail1_2P,
        detail2pControl: this.riskDetail2p,
        detail3_1Control: this.riskDetail3_1,
        detail3_1PControl: this.riskDetail3_1P,
        detail3_2Control: this.riskDetail3_2,
        detail3_2PControl: this.riskDetail3_2P,


        p1Control: this.p1,
        p2Control: this.p2,
        p3Control: this.p3,
        p4Control: this.p4,
        p5Control: this.p5,
        p6Control: this.p6,

      });
      //==============================================//

      this._Service.getFundRiskMapp("1", this.id).subscribe(response => {
        this.fundMapp1 = response;
        console.log(this.fundMapp1);

      })

      this._Service.getFundRiskMapp("2", this.id).subscribe(response => {
        this.fundMapp2 = response;
      })

      this._Service.getFundRiskMapp("3", this.id).subscribe(response => {
        this.fundMapp3 = response;
      })

      this._Service.getFundRiskMapp("4", this.id).subscribe(response => {
        this.fundMapp4 = response;
      })

      this._Service.getFundRiskMapp("5", this.id).subscribe(response => {
        this.fundMapp5 = response;
      })

      this._Service.getFundRiskMapp("6", this.id).subscribe(response => {
        this.fundMapp6 = response;
      })

    })
  }

  //============================= Set P1 ================================================
  addP1() {
    this.ngxService.start();
    this.choiceFromSelect = "display-p1";

    this._Service.get_fundByTypeNotMapping("1", "", this.id).subscribe(response => {
      this.fundNotMapp1 = response;
    })

    this._Service.getFundRiskMapp("1", this.id).subscribe(response => {
      this.fundMapp1 = response;
    })
    this.ngxService.stop();
  }

  clickbackP1() {
    this.choiceFromSelect = "display-form";
  }
  setFundName1(name) {
    this.fundName1 = name;
  }

  clickSearchp1() {
    this._Service.get_fundByTypeNotMapping("1", this.fundName1, this.id).subscribe(response => {
      this.fundNotMapp1 = response;
    })

    this._Service.getFundRiskMapp("1", this.id).subscribe(response => {
      this.fundMapp1 = response;
    })
  }

  onSaveUsernameChanged1(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      console.log('delete fund 1');

      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }

  onDeleteUsernameChanged1(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
    }
    else {

      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectP1() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this._Service.Insert_FundRisk_recommend(this.id, "1", this.fundSelectList[i], "y", "1", "1").subscribe(response => {
          this._Service.getFundRiskMapp("1", this.id).subscribe(response => {
            this.fundMapp1 = response;

            this._Service.get_fundByTypeNotMapping("1", this.fundName1, this.id).subscribe(response => {
              this.fundNotMapp1 = response;
            })

          })
        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();

    }
  }


  deleteP1(id) {
    this.ngxService.start();
    this._Service.Delete_FundRisk_recommend(id).subscribe(response => {
      this._Service.getFundRiskMapp("1", this.id).subscribe(response => {
        this.fundMapp1 = response;

        this._Service.get_fundByTypeNotMapping("1", this.fundName1, this.id).subscribe(response => {
          this.fundNotMapp1 = response;
        })
        this.ngxService.stop();


      })
    })

  }


  delP1() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {

        this._Service.Delete_FundRisk_recommend(this.delSelectList[i]).subscribe(response => {
          this._Service.getFundRiskMapp("1", this.id).subscribe(response => {
            this.fundMapp1 = response;
            this._Service.get_fundByTypeNotMapping("1", this.fundName1, this.id).subscribe(response => {
              this.fundNotMapp1 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    }
  }


  //============================= Set P2 ================================================

  addp2() {
    this.ngxService.start();
    this.choiceFromSelect = "display-p2";

    this._Service.get_fundByTypeNotMapping("2", "", this.id).subscribe(response => {
      this.fundNotMapp2 = response;
    })

    this._Service.getFundRiskMapp("2", this.id).subscribe(response => {
      this.fundMapp2 = response;
    })
    this.ngxService.stop();

  }

  clickbackp2() {
    this.choiceFromSelect = "display-form";
  }
  setFundName2(name) {
    this.fundName2 = name;
  }
  clickSearchp2() {
    this._Service.get_fundByTypeNotMapping("2", this.fundName2, this.id).subscribe(response => {
      this.fundNotMapp2 = response;
    })

    this._Service.getFundRiskMapp("2", this.id).subscribe(response => {
      this.fundMapp2 = response;
    })
  }
  onSaveUsernameChanged2(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }

  onDeleteUsernameChanged2(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
      console.log('delete id list', this.delSelectList);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectp2() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this._Service.Insert_FundRisk_recommend(this.id, "2", this.fundSelectList[i], "y", "1", "1").subscribe(response => {

          this._Service.get_fundByTypeNotMapping("2", this.fundName2, this.id).subscribe(response => {
            this.fundNotMapp2 = response;

            this._Service.getFundRiskMapp("2", this.id).subscribe(response => {
              this.fundMapp2 = response;
            })

          })

        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();

    }
  }

  deleteP2(id) {
    this.ngxService.start();
    this._Service.Delete_FundRisk_recommend(id).subscribe(response => {
      this._Service.getFundRiskMapp("2", this.id).subscribe(response => {
        this.fundMapp2 = response;

        this._Service.get_fundByTypeNotMapping("2", this.fundName1, this.id).subscribe(response => {
          this.fundNotMapp2 = response;
          this.ngxService.stop();

        })

      })
    })

  }
  delp2() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this._Service.Delete_FundRisk_recommend(this.delSelectList[i]).subscribe(response => {
          this._Service.getFundRiskMapp("2", this.id).subscribe(response => {
            this.fundMapp2 = response;

            this._Service.get_fundByTypeNotMapping("2", this.fundName2, this.id).subscribe(response => {
              this.fundNotMapp2 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    }
  }

  //============================= Set P3 ================================================

  addp3() {
    this.ngxService.start();
    this.choiceFromSelect = "display-p3";

    this._Service.getFundRiskMapp("3", this.id).subscribe(response => {
      this.fundMapp3 = response;
    })

    this._Service.get_fundByTypeNotMapping("3", "", this.id).subscribe(response => {
      this.fundNotMapp3 = response;
    })
    this.ngxService.stop();
  }

  clickbackp3() {
    this.choiceFromSelect = "display-form";
  }
  setFundName3(name) {
    this.fundName3 = name;
  }
  clickSearchp3() {
    this._Service.get_fundByTypeNotMapping("3", this.fundName3, this.id).subscribe(response => {
      this.fundNotMapp3 = response;
    })

    this._Service.getFundRiskMapp("3", this.id).subscribe(response => {
      this.fundMapp3 = response;
    })
  }
  onSaveUsernameChanged3(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }
  onDeleteUsernameChanged3(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
      console.log('delete id list', this.delSelectList);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }
  selectp3() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this._Service.Insert_FundRisk_recommend(this.id, "3", this.fundSelectList[i], "y", "1", "1").subscribe(response => {

          this._Service.get_fundByTypeNotMapping("3", this.fundName3, this.id).subscribe(response => {
            this.fundNotMapp3 = response;

            this._Service.getFundRiskMapp("3", this.id).subscribe(response => {
              this.fundMapp3 = response;
            })

          })
        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();
    }
  }
  deleteP3(id) {
    this.ngxService.start();
    this._Service.Delete_FundRisk_recommend(id).subscribe(response => {
      this._Service.getFundRiskMapp("3", this.id).subscribe(response => {
        this.fundMapp3 = response;

        this._Service.get_fundByTypeNotMapping("3", this.fundName1, this.id).subscribe(response => {
          this.fundNotMapp3 = response;
        })
        this.ngxService.stop();


      })
    })

  }
  delp3() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this._Service.Delete_FundRisk_recommend(this.delSelectList[i]).subscribe(response => {
          this._Service.getFundRiskMapp("3", this.id).subscribe(response => {
            this.fundMapp3 = response;

            this._Service.get_fundByTypeNotMapping("3", this.fundName3, this.id).subscribe(response => {
              this.fundNotMapp3 = response;
            })

          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    }
  }

  //============================= Set P4 ================================================

  addp4() {
    this.ngxService.start();
    this.choiceFromSelect = "display-p4";

    this._Service.get_fundByTypeNotMapping("4", "", this.id).subscribe(response => {
      this.fundNotMapp4 = response;
    })

    this._Service.getFundRiskMapp("4", this.id).subscribe(response => {
      this.fundMapp4 = response;
    })
    this.ngxService.stop();
  }

  clickbackp4() {
    this.choiceFromSelect = "display-form";
  }
  setFundName4(name) {
    this.fundName4 = name;
  }
  clickSearchp4() {
    this._Service.get_fundByTypeNotMapping("4", this.fundName4, this.id).subscribe(response => {
      this.fundNotMapp4 = response;
    })

    this._Service.getFundRiskMapp("4", this.id).subscribe(response => {
      this.fundMapp4 = response;
    })
  }
  onSaveUsernameChanged4(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }
  onDeleteUsernameChanged4(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
      console.log('delete id list', this.delSelectList);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectp4() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this._Service.Insert_FundRisk_recommend(this.id, "4", this.fundSelectList[i], "y", "1", "1").subscribe(response => {

          this._Service.get_fundByTypeNotMapping("4", this.fundName4, this.id).subscribe(response => {
            this.fundNotMapp4 = response;

            this._Service.getFundRiskMapp("4", this.id).subscribe(response => {
              this.fundMapp4 = response;
            })

          })

        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();
    }
  }

  deleteP4(id) {
    this.ngxService.start();
    this._Service.Delete_FundRisk_recommend(id).subscribe(response => {
      this._Service.getFundRiskMapp("4", this.id).subscribe(response => {
        this.fundMapp4 = response;

        this._Service.get_fundByTypeNotMapping("4", this.fundName4, this.id).subscribe(response => {
          this.fundNotMapp4 = response;

          this.ngxService.stop();
        })
      })
    })

  }

  delp4() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this._Service.Delete_FundRisk_recommend(this.delSelectList[i]).subscribe(response => {
          this._Service.getFundRiskMapp("4", this.id).subscribe(response => {
            this.fundMapp4 = response;
            this._Service.get_fundByTypeNotMapping("4", this.fundName4, this.id).subscribe(response => {
              this.fundNotMapp4 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    }
  }

  //============================= Set P5 ================================================

  addp5() {
    this.ngxService.start();
    this.choiceFromSelect = "display-p5";

    this._Service.get_fundByTypeNotMapping("5", "", this.id).subscribe(response => {
      this.fundNotMapp5 = response;
    })

    this._Service.getFundRiskMapp("5", this.id).subscribe(response => {
      this.fundMapp5 = response;
    })
    this.ngxService.stop();
  }


  clickbackp5() {
    this.choiceFromSelect = "display-form";
  }
  setFundName5(name) {
    this.fundName5 = name;
  }
  clickSearchp5() {
    this._Service.get_fundByTypeNotMapping("5", this.fundName5, this.id).subscribe(response => {
      this.fundNotMapp5 = response;

      console.log("TEST SEARCH" + response);
    })

    this._Service.getFundRiskMapp("5", this.id).subscribe(response => {
      this.fundMapp5 = response;
    })
  }
  onSaveUsernameChanged5(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }
  onDeleteUsernameChanged5(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
      console.log('delete id list', this.delSelectList);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectp5() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this._Service.Insert_FundRisk_recommend(this.id, "5", this.fundSelectList[i], "y", "1", "1").subscribe(response => {

          this._Service.get_fundByTypeNotMapping("5", this.fundName5, this.id).subscribe(response => {
            this.fundNotMapp5 = response;
          })
          this._Service.getFundRiskMapp("5", this.id).subscribe(response => {
            this.fundMapp5 = response;
          })

        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();

    }
  }

  deleteP5(id) {
    this.ngxService.start();
    this._Service.Delete_FundRisk_recommend(id).subscribe(response => {
      this._Service.getFundRiskMapp("5", this.id).subscribe(response => {

        this.fundMapp5 = response;
        this._Service.get_fundByTypeNotMapping("5", this.fundName5, this.id).subscribe(response => {
          this.fundNotMapp5 = response;
        })
        this.ngxService.stop();

      })
    })

  }

  delp5() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this._Service.Delete_FundRisk_recommend(this.delSelectList[i]).subscribe(response => {
          this._Service.getFundRiskMapp("5", this.id).subscribe(response => {
            this.fundMapp5 = response;
            this._Service.get_fundByTypeNotMapping("5", this.fundName5, this.id).subscribe(response => {
              this.fundNotMapp5 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    }
  }

  //============================= Set P6 ================================================

  addp6() {
    this.ngxService.start();
    this.choiceFromSelect = "display-p6";

    this._Service.get_fundByTypeNotMapping("6", "", this.id).subscribe(response => {
      this.fundNotMapp6 = response;
    })

    this._Service.getFundRiskMapp("6", this.id).subscribe(response => {
      this.fundMapp6 = response;
    })
    this.ngxService.stop();
  }

  clickbackp6() {
    this.choiceFromSelect = "display-form";
  }
  setFundName6(name) {
    this.fundName6 = name;
  }
  clickSearchp6() {
    this._Service.get_fundByTypeNotMapping("6", this.fundName6, this.id).subscribe(response => {
      this.fundNotMapp6 = response;
    })

    this._Service.getFundRiskMapp("6", this.id).subscribe(response => {
      this.fundMapp6 = response;
    })
  }
  onSaveUsernameChanged6(value: boolean, x) {
    if (value == true) {
      this.fundSelectList.push(x);
    }
    else {
      const index: number = this.fundSelectList.indexOf(x);
      if (index !== -1) {
        this.fundSelectList.splice(index, 1);
      }
    }
  }
  onDeleteUsernameChanged6(value: boolean, x) {
    if (value == true) {
      this.delSelectList.push(x);
      console.log('delete id list', this.delSelectList);
    }
    else {
      const index: number = this.delSelectList.indexOf(x);
      if (index !== -1) {
        this.delSelectList.splice(index, 1);
      }
    }
  }

  selectp6() {
    this.ngxService.start();
    if (this.fundSelectList.length != 0) {
      var num: number = 5;
      var i: number;
      for (i = 0; i < this.fundSelectList.length; i++) {
        this._Service.Insert_FundRisk_recommend(this.id, "6", this.fundSelectList[i], "y", "1", "1").subscribe(response => {
          this._Service.get_fundByTypeNotMapping("6", this.fundName6, this.id).subscribe(response => {
            this.fundNotMapp6 = response;

            this._Service.getFundRiskMapp("6", this.id).subscribe(response => {
              this.fundMapp6 = response;
            })

          })

        })
      }
      this.fundSelectList = [];
      this.ngxService.stop();

    }
  }

  deleteP6(id) {
    this.ngxService.start();
    this._Service.Delete_FundRisk_recommend(id).subscribe(response => {
      this._Service.getFundRiskMapp("6", this.id).subscribe(response => {
        this.fundMapp6 = response;
        this._Service.get_fundByTypeNotMapping("6", this.fundName6, this.id).subscribe(response => {
          this.fundNotMapp6 = response;
        })
        this.ngxService.stop();
      })
    })

  }

  delp6() {
    this.ngxService.start();
    if (this.delSelectList.length != 0) {
      var i: number
      for (i = 0; i < this.delSelectList.length; i++) {
        this._Service.Delete_FundRisk_recommend(this.delSelectList[i]).subscribe(response => {
          this._Service.getFundRiskMapp("6", this.id).subscribe(response => {
            this.fundMapp6 = response;
            this._Service.get_fundByTypeNotMapping("6", this.fundName6, this.id).subscribe(response => {
              this.fundNotMapp6 = response;
            })
          })
        })
      }
      this.delSelectList = [];
      this.ngxService.stop();

    }
  }


  //======================================================================

  Back() {
    this.router.navigate(['/back-office/fund-risk']);
  }

  // setRiskFiles(event) {
  //   this.riskTitleImage = <File>event.target.files[0]
  //   this.titleImagepath = linkApi + '/FileUpload/' + <File>event.target.files[0].name

  // }

  save_info() {
    this.ngxService.start();

    this._Service.uploadFile(this.riskTitleImage).subscribe(response => {

      if (response.toString() != "") {
        this.titleImagepath = response.toString();
      }


      this._Service.Update_FundRisk_recommend(this.title, this.titleImagepath,
        this.riskDetail1_1, this.riskDetail1_1P, this.riskDetail1_2, this.riskDetail1_2P,
        this.riskDetail2p, this.riskDetail3_1, this.riskDetail3_1P, this.riskDetail3_2,
        this.riskDetail3_2P, this.p1, this.p2, this.p3, this.p4, this.p5, this.p6, this.id).subscribe(response => {
          this.ngxService.stop();

          this.router.navigate(['/back-office/fund-risk']);
        })
    })




  }

  setRiskTitle(values) { this.title = values; }
  setRisk_Detail1(values) { this.riskDetail1_1 = values; }
  setRisk_Detail1P(values) { this.riskDetail1_1P = values; }
  setRisk_Detail2(values) { this.riskDetail1_2 = values; }
  setRisk_Detail1_2P(values) {
    this.riskDetail1_2P = values;
    console.log(this.riskDetail1_2P)
  }
  setRisk_Detail2p(values) { this.riskDetail2p = values; }
  setRisk_Detail3(values) { this.riskDetail3_1 = values; }
  setRisk_Detail3P(values) { this.riskDetail3_1P = values; }
  setRisk_Detail3_2(values) { this.riskDetail3_2 = values; }
  setRisk_Detail3_2P(values) { this.riskDetail3_2P = values; }


  setRisk_p1(values) {
    this.p1 = values;
    // console.log(this.p1);
    // let p1Value = this.p1
    if (isNaN(this.p6)) {
      let p1Num = (parseInt(values))
      let p2Num = (parseInt(this.p2))
      let p3Num = (parseInt(this.p3))
      let p4Num = (parseInt(this.p4))
      let p5Num = (parseInt(this.p5))
      let p6Num = (parseInt(this.p6))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num + p6Num)
      // console.log('p1', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนตลาดเงิน')
      }
    } else {
      let p1Num = (parseInt(values))
      let p2Num = (parseInt(this.p2))
      let p3Num = (parseInt(this.p3))
      let p4Num = (parseInt(this.p4))
      let p5Num = (parseInt(this.p5))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num)
      // console.log('p1', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนตลาดเงิน')
      }
    }
  }

  setRisk_p2(values) {
    this.p2 = values;
    if (isNaN(this.p6)) {
      let p1Num = (parseInt(this.p1))
      let p2Num = (parseInt(values))
      let p3Num = (parseInt(this.p3))
      let p4Num = (parseInt(this.p4))
      let p5Num = (parseInt(this.p5))
      let p6Num = (parseInt(this.p6))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num + p6Num)
      // console.log('p2', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนตราสารหนี้')

      }
    } else {
      let p1Num = (parseInt(this.p1))
      let p2Num = (parseInt(values))
      let p3Num = (parseInt(this.p3))
      let p4Num = (parseInt(this.p4))
      let p5Num = (parseInt(this.p5))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num)
      // console.log('p2', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนตราสารหนี้')

      }
    }
  }

  setRisk_p3(values) {
    this.p3 = values;
    if (isNaN(this.p6)) {
      let p1Num = (parseInt(this.p1))
      let p2Num = (parseInt(this.p2))
      let p3Num = (parseInt(values))
      let p4Num = (parseInt(this.p4))
      let p5Num = (parseInt(this.p5))
      let p6Num = (parseInt(this.p6))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num + p6Num)
      // console.log('p3', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนผสม')
      }
    } else {
      let p1Num = (parseInt(this.p1))
      let p2Num = (parseInt(this.p2))
      let p3Num = (parseInt(values))
      let p4Num = (parseInt(this.p4))
      let p5Num = (parseInt(this.p5))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num)
      // console.log('p3', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนผสม')

      }
    }
  }

  setRisk_p4(values) {
    this.p4 = values;
    if (isNaN(this.p6)) {
      let p1Num = (parseInt(this.p1))
      let p2Num = (parseInt(this.p2))
      let p3Num = (parseInt(this.p3))
      let p4Num = (parseInt(values))
      let p5Num = (parseInt(this.p5))
      let p6Num = (parseInt(this.p6))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num + p6Num)
      // console.log('p4', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนตราสารทุน')
        // this.editRiskForm.p2Control = this.p2
      }
    } else {
      let p1Num = (parseInt(this.p1))
      let p2Num = (parseInt(this.p2))
      let p3Num = (parseInt(this.p3))
      let p4Num = (parseInt(values))
      let p5Num = (parseInt(this.p5))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num)
      // console.log('p4', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนตราสารทุน')
      }
    }
  }

  setRisk_p5(values) {
    this.p5 = values;
    if (isNaN(this.p6)) {
      let p1Num = (parseInt(this.p1))
      let p2Num = (parseInt(this.p2))
      let p3Num = (parseInt(this.p3))
      let p4Num = (parseInt(this.p4))
      let p5Num = (parseInt(values))
      let p6Num = (parseInt(this.p6))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num + p6Num)
      // console.log('p5', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนอสังหาริมทรัพย์')
        // this.editRiskForm.p2Control = this.p2
      }
    } else {
      let p1Num = (parseInt(this.p1))
      let p2Num = (parseInt(this.p2))
      let p3Num = (parseInt(this.p3))
      let p4Num = (parseInt(this.p4))
      let p5Num = (parseInt(values))
      let total = (p1Num + p2Num + p3Num + p4Num + p5Num)
      // console.log('p5', total);
      if (total > 100) {
        alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนอสังหาริมทรัพย์')
        // this.editRiskForm.p2Control = this.p2
      }
    }
  }

  setRisk_p6(values) {
    this.p6 = values;
    let p1Num = (parseInt(this.p1))
    let p2Num = (parseInt(this.p2))
    let p3Num = (parseInt(this.p3))
    let p4Num = (parseInt(this.p4))
    let p5Num = (parseInt(this.p5))
    let p6Num = (parseInt(values))
    let total = (p1Num + p2Num + p3Num + p4Num + p5Num + p6Num)
    // console.log('p6', total);
    if (total > 100) {
      alert('สัดส่วนรวมขณะนี้คือ ' + total + '% กรุณาลดสัดส่วนกองทุนตราสารทุนต่างประเทศ')
    }
  }

}
