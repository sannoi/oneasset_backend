import { Component, OnInit } from '@angular/core';
import { PromotionService } from 'src/app/services/promotion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';


@Component({
  selector: 'app-edit-promotion',
  templateUrl: './edit-promotion.component.html',
  styleUrls: ['./edit-promotion.component.scss']
})
export class EditPromotionComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _promotionService: PromotionService, private ngxService: NgxUiLoaderService) { }

  private sub: any;

  isTh: boolean
  isEn: boolean
  id: string;
  promotionType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  promotion: any;

  promotionTtitleImage: any;
  promotionBannerImage: any
  promotionBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  promotionFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typepromotion: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statuspromotion: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editpromotionForm: FormGroup;

  old_promotionType: string;
  old_promotionDetail: string;
  isPDF_old: any;

  vdo_id: any

   // TODO: Public Date
   publicDate: any;
   public_date: any;

  ngOnInit() {
      // TODO: Public Date
    flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m2')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.isTh = true
    this.isEn = false

    this._promotionService.getpromotionByID(this.id).subscribe(response => {

      this.promotion = response

      this.promotionType = response[0]['promotion_type']
      this.title = response[0]['title']
      this.title_image = response[0]['promotion_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['promotion_img_Thum']

      if (response[0]['promotion_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

      // TODO: Public Date
      if (response[0]['publicDate']) {
        this.public_date = response[0]['publicDate'];
        this.publicDate  = response[0]['publicDate'];
      } else {
        this.public_date = new Date().toISOString().slice(0,10);
        this.publicDate  = new Date().toISOString().slice(0,10);
      }

      if (response[0]['promotion_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_promotionType = response[0]['promotion_type']
      this.old_promotionDetail = response[0]['detail']

      if (response[0]['promotion_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }


      //=============== Binding Info ==================//
      this.editpromotionForm = this.fb.group({
        promotionTypeControl: this.promotionType,
        promotionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date : this.public_date

      });
      //==============================================//
    })
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setpromotionType(code) {
    this.promotionType = code;
    if (this.promotionType == this.old_promotionType) {
      //=============== Binding Info ==================//
      this.editpromotionForm = this.fb.group({
        promotionTypeControl: this.promotionType,
        promotionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_promotionDetail,
        vdo_id: this.old_promotionDetail,
        public_date : this.public_date
      });
      this.detail = this.old_promotionDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editpromotionForm = this.fb.group({
        promotionTypeControl: this.promotionType,
        promotionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date : this.public_date
      });
      this.detail = "";
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setpromotionTitle(title) { this.title = title; }
  setpromotionFiles(event) {
    if (event.target.id === 'promotionTitleImage') {
      this.promotionTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'promotionFile') {
      this.promotionFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setpromotionStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/promotion']);
  }

  deleteBanner() {
    this._promotionService.delBannerpromotion(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/promotion/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = ""

  }

  deleteBanner_mobile() {
    this._promotionService.delBannerpromotion_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/promotion/edit'], { queryParams: { id: this.id } });
  }

  setpromotionBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.promotionBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setpromotionBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.promotionBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  editpromotion() {

    this.ngxService.start();

    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.promotionType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._promotionService.uploadFile_title(this.promotionTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== Banner Image ================================
            if (this.status === '1') {
              this._promotionService.uploadFile_mobile(this.promotionBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._promotionService.uploadFile_banner(this.promotionBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._promotionService.updatepromotion(this.title, this.promotionType, editorHtmlTag, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/promotion']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._promotionService.delBannerpromotion(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._promotionService.updatepromotion(this.title, this.promotionType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/promotion']);
                  })
              })
            }

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })




      } else if (this.promotionType === 'image') {

        //==================== Title Image ================================
        this._promotionService.uploadFile_title(this.promotionTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== File Download ================================
            this._promotionService.uploadFile(this.promotionFiles).subscribe(response => {
              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              //==================== Banner Image ================================
              if (this.status === '1') {
                this._promotionService.uploadFile_mobile(this.promotionBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._promotionService.uploadFile_banner(this.promotionBannerImage).subscribe(response => {

                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._promotionService.updatepromotion(this.title, this.promotionType, this.FilePath, this.titleImagepath, userId,
                      this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/promotion']);
                      })
                  })

                })

              } else if (this.status === '0') {
                this._promotionService.delBannerpromotion(this.id).subscribe(response => {

                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";

                  this._promotionService.updatepromotion(this.title, this.promotionType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/promotion']);
                    })
                })
              }



            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }



        })




      } else if (this.promotionType === 'pdf') {

        //==================== Title Image ================================
        this._promotionService.uploadFile_title(this.promotionTtitleImage).subscribe(response => {


          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== File Download ================================
            this._promotionService.uploadFile(this.promotionFiles).subscribe(response => {
              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              //==================== Banner Image ================================
              if (this.status === '1') {
                this._promotionService.uploadFile_mobile(this.promotionBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._promotionService.uploadFile_banner(this.promotionBannerImage).subscribe(response => {

                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._promotionService.updatepromotion(this.title, this.promotionType, this.FilePath, this.titleImagepath,
                      userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/promotion']);
                      })

                  })

                })

              } else if (this.status === '0') {
                this._promotionService.delBannerpromotion(this.id).subscribe(response => {

                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";

                  this._promotionService.updatepromotion(this.title, this.promotionType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/promotion']);
                    })
                })
              }



            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }


        })




      }
      else if (this.promotionType === 'vdo') {

        //==================== Title Image ================================
        this._promotionService.uploadFile_title(this.promotionTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== Banner Image ================================
            if (this.status === '1') {
              this._promotionService.uploadFile_mobile(this.promotionBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._promotionService.uploadFile_banner(this.promotionBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._promotionService.updatepromotion(this.title, this.promotionType, this.vdo_id, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/promotion']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._promotionService.delBannerpromotion(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._promotionService.updatepromotion(this.title, this.promotionType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/promotion']);
                  })
              })
            }


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })





      }


    }

    this.ngxService.stop();


  }










}
