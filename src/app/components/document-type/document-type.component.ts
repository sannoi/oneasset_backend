import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { DocumentService } from 'src/app/services/document.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-document-type',
  templateUrl: './document-type.component.html',
  styleUrls: ['./document-type.component.scss']
})
export class DocumentTypeComponent implements OnInit {

  data: any[] = [
    { val: 'y', name: 'Publish ' },
    { val: 'n', name: 'Unpublish  ' },
  ];

  docDate: any
  docType: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private docTypeList: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  constructor(private router: Router, private _documentService: DocumentService,
    private pagerService: PagerService,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m6')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._documentService.getDocumentType("", "y").subscribe(response => {
      this.docType = response
      console.log(this.docType)
      this.docTypeList = response
      this.setPage(1);
      this.selectedLevel = "y"
      // console.log(response);

      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.docDate = this._documentService.changeFormatDate(date)
    }
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.docTypeList.length, page);
    this.pagedItems = this.docTypeList.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  getNewsFromSerch() {
    this.ngxService.start();
    // console.log(this.topicSearch, this.selectedLevel);

    this._documentService.getDocumentType(this.topicSearch, this.selectedLevel).subscribe(response => {

      // console.log(response)
      this.docType = response
      this.docTypeList = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/document-type/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickDelete(typeIdDelete) {
    this.ngxService.start();
    this._documentService.deleteDocTypeById(typeIdDelete).subscribe(response => {
      this._documentService.getDocumentType("", "y").subscribe(response => {
        this.docType = response
        this.docTypeList = response
        this.setPage(1);
        this.selectedLevel = "y"
        // console.log(response);

        this.ngxService.stop();
      })
    })
  }

  clickAddDoctype() {
    event.preventDefault()
    this.router.navigate(['/back-office/document-type/add'])

  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.docDate = this._documentService.date_thai(date)
    }
  }

  date_convert(date, short) {
    if (date === '') {
      return null
    } else {
      return this.docDate = this._documentService.date_convert(date, short)
    }
  }

}
