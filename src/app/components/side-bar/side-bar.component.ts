import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NewsService } from 'src/app/services/news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  constructor(private ngxService: NgxUiLoaderService, private _newsService: NewsService, private router: Router) { }

  m1: any;
  m2: any;
  m3: any;
  m4: any;
  m5: any;
  m6: any;
  m7: any;
  m8: any;
  m9: any;
  m10: any;
  m11: any;

  ngOnInit() {

    this.ngxService.start();

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    this._newsService.getMenu(userId).subscribe(response => {
      this.m1 = response['m1']
      this.m2 = response['m2']
      this.m3 = response['m3']
      this.m4 = response['m4']
      this.m5 = response['m5']
      this.m6 = response['m6']
      this.m7 = response['m7']
      this.m8 = response['m8']
      this.m9 = response['m9']
      this.m10 = response['m10']
      this.m11 = response['m11']

      sessionStorage.setItem('m1', response['m1'])
      sessionStorage.setItem('m2', response['m2'])
      sessionStorage.setItem('m3', response['m3'])
      sessionStorage.setItem('m4', response['m4'])
      sessionStorage.setItem('m5', response['m5'])
      sessionStorage.setItem('m6', response['m6'])
      sessionStorage.setItem('m7', response['m7'])
      sessionStorage.setItem('m8', response['m8'])
      sessionStorage.setItem('m9', response['m9'])
      sessionStorage.setItem('m10', response['m10'])
      sessionStorage.setItem('m11', response['m11'])

      this.ngxService.stop();
    })
  }

}
