import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EarthupdateService } from 'src/app/services/earthupdate.service';
import { PagerService } from 'src/app/_services';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-earthupdate',
  templateUrl: './earthupdate.component.html',
  styleUrls: ['./earthupdate.component.scss']
})
export class EarthupdateComponent implements OnInit {

  constructor(private router: Router, private _earthupdateService: EarthupdateService,
    private pagerService: PagerService, private ngxService: NgxUiLoaderService) { }

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];

  earthupdateDate: any
  earthupdate: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();

    this._earthupdateService.getEarthUpdate(1, "").subscribe(response => {
      this.earthupdate = response
      this.allItems = response
      this.setPage(1);
      this.selectedLevel = 1
      this.ngxService.stop();
    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();

      this._earthupdateService.getEarthUpdate(this.selectedLevel, this.topicSearch).subscribe(response => {
        this.earthupdate = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }



  getEarthUpdateFromSerch() {
    this.ngxService.start();
    this._earthupdateService.getEarthUpdate(this.selectedLevel, this.topicSearch).subscribe(response => {
      this.earthupdate = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.earthupdateDate = this._earthupdateService.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/earthupdate/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickAddEarthUpdate() {
    event.preventDefault()
    this.router.navigate(['/back-office/earthupdate/add'])

  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.earthupdateDate = this._earthupdateService.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.earthupdateDate = this._earthupdateService.date_convert(date)
    }
  }





}
