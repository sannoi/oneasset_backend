import { Component, OnInit } from '@angular/core';
import { VdoService } from 'src/app/services/vdo.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-vdo-add',
  templateUrl: './vdo-add.component.html',
  styleUrls: ['./vdo-add.component.scss']
})
export class VdoAddComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  vdoType: string;
  typevdo: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  vdoStatus: string;
  statusvdo: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add vdo
  vdoTitle: string
  vdoTtitleImage: any
  vdoBannerImage: any
  imageTitleName: any
  vdoContentImage: any
  imageContentName: any
  vdoFiles: any
  vdoFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  vdoBannerImage_mobile: any
  BannerImagepath_mobile: any

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _vdoService: VdoService, private router: Router, private ngxService: NgxUiLoaderService) { }


  ngOnInit() {
// TODO: Public Date
flatpickr.localize(Thai);
this.public_date = new Date().toISOString().slice(0,10);
this.publicDate  = this.public_date;
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.vdoType = 'text';
    this.vdoStatus = '1';
    this.BannerImagepath = '';
    this.isTh = true
    this.isEn = false
  }

 // TODO: Public Date
 setPublicDate(public_date: string) {
  this.publicDate = public_date;
}
  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setvdoType(code) {
    // console.log("choice: ", code);
    this.vdoType = code;
  }

  setvdoTitle(title: string) {
    this.vdoTitle = title
  }

  setvdoStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.vdoStatus = '1'
    } else if (code === 'Unpublish ') {
      this.vdoStatus = '0'
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setvdoFiles(event) {
    if (event.target.id === 'vdoTitleImage') {
      this.vdoTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'vdoContentImage') {
      this.vdoContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'vdoFile') {
      this.vdoFileName = <File>event.target.files[0]
      this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }

  setvdoBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.vdoBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name
        }

      };
    }


  }

  setvdoBannerFiles_mobile(event) {
    this.vdoBannerImage_mobile = <File>event.target.files[0]
    this.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name
  }


  addvdo() {
    this.ngxService.start();

    let userId = sessionStorage.getItem('userId')

    if (this.vdoTitle == undefined || this.vdoTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.vdoType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._vdoService.uploadFile_title(this.vdoTtitleImage).subscribe(response => {

          if (response.toString() != "") {
            this.titleImagepath = response.toString();


            //==================== Banner Image ================================
            this._vdoService.uploadFile_banner(this.vdoBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();
              this._vdoService.uploadFile_mobile(this.vdoBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();
                this._vdoService.addvdo(this.vdoTitle, this.vdoType, editorHtmlTag, this.titleImagepath, userId,
                  this.vdoStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vdo']);
                  })
              })
            })

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }



        })

      }
      else if (this.vdoType === 'image') {
        //==================== Title Image ================================
        this._vdoService.uploadFile_title(this.vdoTtitleImage).subscribe(response => {

          if (response.toString() != "") {
            this.titleImagepath = response.toString();



            //==================== File Download ================================
            this._vdoService.uploadFile(this.vdoContentImage).subscribe(response => {
              this.contentImagePath = response.toString();
              //==================== Banner Image ================================
              this._vdoService.uploadFile_banner(this.vdoBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();
                this._vdoService.uploadFile_mobile(this.vdoBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();
                  this._vdoService.addvdo(this.vdoTitle, this.vdoType, this.contentImagePath, this.titleImagepath,
                    userId, this.vdoStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vdo']);

                    })
                })
              })
            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })










      } else if (this.vdoType === 'pdf') {
        //==================== Title Image ================================
        this._vdoService.uploadFile_title(this.vdoTtitleImage).subscribe(response => {
          if (response.toString() != "") {
            this.titleImagepath = response.toString();



            //==================== File Download ================================
            this._vdoService.uploadFile(this.vdoFileName).subscribe(response => {
              this.pdfFilePath = response.toString();
              //==================== Banner Image ================================
              this._vdoService.uploadFile_banner(this.vdoBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();
                this._vdoService.uploadFile_mobile(this.vdoBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();
                  this._vdoService.addvdo(this.vdoTitle, this.vdoType, this.pdfFilePath, this.titleImagepath,
                    userId, this.vdoStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/vdo']);

                    })
                })
              })
            })



          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })

      }

      else if (this.vdoType === 'vdo') {
        //==================== Title Image ================================
        this._vdoService.uploadFile_title(this.vdoTtitleImage).subscribe(response => {
          if (response.toString() != "") {
            this.titleImagepath = response.toString();



            //==================== Banner Image ================================
            this._vdoService.uploadFile_banner(this.vdoBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();
              this._vdoService.uploadFile_mobile(this.vdoBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();
                this._vdoService.addvdo(this.vdoTitle, this.vdoType, this.vdo_id, this.titleImagepath,
                  userId, this.vdoStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/vdo']);

                  })
              })
            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })







      }





    }

    this.ngxService.stop();


  }

  Back() {
    this.router.navigate(['/back-office/vdo']);
  }
























}
