import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-edit-investment-allocation',
  templateUrl: './edit-investment-allocation.component.html',
  styleUrls: ['./edit-investment-allocation.component.scss']
})
export class EditInvestmentAllocationComponent implements OnInit {

  editInvestAlloForm: FormGroup

  // Equity Investment Allocation
  reseqlvl1: any
  reseqlvl2: any
  reseqlvl3: any
  reseqlvl4: any
  reseqlvl5: any

  // Money Market Investment Allocation
  resmmlvl1: any
  resmmlvl2: any
  resmmlvl3: any
  resmmlvl4: any
  resmmlvl5: any

  // Fixed Income Investment Allocation
  resfilvl1: any
  resfilvl2: any
  resfilvl3: any
  resfilvl4: any
  resfilvl5: any

  // Commodity Investment Allocation
  rescommlvl1: any
  rescommlvl2: any
  rescommlvl3: any
  rescommlvl4: any
  rescommlvl5: any

  // Property Investment Allocation
  resproplvl1: any
  resproplvl2: any
  resproplvl3: any
  resproplvl4: any
  resproplvl5: any


  // Equity Investment Allocation
  eqlvl1: any
  eqlvl2: any
  eqlvl3: any
  eqlvl4: any
  eqlvl5: any

  // Money Market Investment Allocation
  mmlvl1: any
  mmlvl2: any
  mmlvl3: any
  mmlvl4: any
  mmlvl5: any

  // Fixed Income Investment Allocation
  filvl1: any
  filvl2: any
  filvl3: any
  filvl4: any
  filvl5: any

  // Commodity Investment Allocation
  commlvl1: any
  commlvl2: any
  commlvl3: any
  commlvl4: any
  commlvl5: any

  // Property Investment Allocation
  proplvl1: any
  proplvl2: any
  proplvl3: any
  proplvl4: any
  proplvl5: any

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _documentService: DocumentService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    
    let ress = sessionStorage.getItem('m10')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
    
    this._documentService.getMaintain("Property").subscribe(response1 => {

      this.resproplvl1 = response1["r1"];
      this.resproplvl2 = response1["r2"];
      this.resproplvl3 = response1["r3"];
      this.resproplvl4 = response1["r4"];
      this.resproplvl5 = response1["r5"];

      this._documentService.getMaintain("MoneyMarket").subscribe(response2 => {
        this.resmmlvl1 = response2["r1"];
        this.resmmlvl2 = response2["r2"];
        this.resmmlvl3 = response2["r3"];
        this.resmmlvl4 = response2["r4"];
        this.resmmlvl5 = response2["r5"];

        this._documentService.getMaintain("FixedIncome").subscribe(response3 => {
          this.resfilvl1 = response3["r1"];
          this.resfilvl2 = response3["r2"];
          this.resfilvl3 = response3["r3"];
          this.resfilvl4 = response3["r4"];
          this.resfilvl5 = response3["r5"];

          this._documentService.getMaintain("Commodity").subscribe(response4 => {
            this.rescommlvl1 = response4["r1"];
            this.rescommlvl2 = response4["r2"];
            this.rescommlvl3 = response4["r3"];
            this.rescommlvl4 = response4["r4"];
            this.rescommlvl5 = response4["r5"];

            this._documentService.getMaintain("Equity").subscribe(response5 => {

              this.reseqlvl1 = response5["r1"];
              this.reseqlvl2 = response5["r2"];
              this.reseqlvl3 = response5["r3"];
              this.reseqlvl4 = response5["r4"];
              this.reseqlvl5 = response5["r5"];

              this.editInvestAlloForm = this.fb.group({

                eqlvl1Ctrl: this.reseqlvl1,
                eqlvl2Ctrl: this.reseqlvl2,
                eqlvl3Ctrl: this.reseqlvl3,
                eqlvl4Ctrl: this.reseqlvl4,
                eqlvl5Ctrl: this.reseqlvl5,

                mmlvl1Ctrl: this.resmmlvl1,
                mmlvl2Ctrl: this.resmmlvl2,
                mmlvl3Ctrl: this.resmmlvl3,
                mmlvl4Ctrl: this.resmmlvl4,
                mmlvl5Ctrl: this.resmmlvl5,

                filvl1Ctrl: this.resfilvl1,
                filvl2Ctrl: this.resfilvl2,
                filvl3Ctrl: this.resfilvl3,
                filvl4Ctrl: this.resfilvl4,
                filvl5Ctrl: this.resfilvl5,

                commlvl1Ctrl: this.rescommlvl1,
                commlvl2Ctrl: this.rescommlvl2,
                commlvl3Ctrl: this.rescommlvl3,
                commlvl4Ctrl: this.rescommlvl4,
                commlvl5Ctrl: this.rescommlvl5,

                proplvl1Ctrl: this.resproplvl1,
                proplvl2Ctrl: this.resproplvl2,
                proplvl3Ctrl: this.resproplvl3,
                proplvl4Ctrl: this.resproplvl4,
                proplvl5Ctrl: this.resproplvl5,

              })


            })
          })
        })

      })

    })



  }

  setEqLvl1(value) {
    this.reseqlvl1 = value
  }
  setEqLvl2(value) {
    this.reseqlvl2 = value
  }
  setEqLvl3(value) {
    this.reseqlvl3 = value
  }
  setEqLvl4(value) {
    this.reseqlvl4 = value
  }
  setEqLvl5(value) {
    this.reseqlvl5 = value
  }

  setMmLvl1(value) {
    this.resmmlvl1 = value
  }
  setMmLvl2(value) {
    this.resmmlvl2 = value
  }
  setMmLvl3(value) {
    this.resmmlvl3 = value
  }
  setMmLvl4(value) {
    this.resmmlvl4 = value
  }
  setMmLvl5(value) {
    this.resmmlvl5 = value
  }

  setFiLvl1(value) {
    this.resfilvl1 = value
  }
  setFiLvl2(value) {
    this.resfilvl2 = value
  }
  setFiLvl3(value) {
    this.resfilvl3 = value
  }
  setFiLvl4(value) {
    this.resfilvl4 = value
  }
  setFiLvl5(value) {
    this.resfilvl5 = value
  }

  setCommLvl1(value) {
    this.rescommlvl1 = value
  }
  setCommLvl2(value) {
    this.rescommlvl2 = value
  }
  setCommLvl3(value) {
    this.rescommlvl3 = value
  }
  setCommLvl4(value) {
    this.rescommlvl4 = value
  }
  setCommLvl5(value) {
    this.rescommlvl5 = value
  }

  setPropLvl1(value) {
    this.resproplvl1 = value
  }
  setPropLvl2(value) {
    this.resproplvl2 = value
  }
  setPropLvl3(value) {
    this.resproplvl3 = value
  }
  setPropLvl4(value) {
    this.resproplvl4 = value
  }
  setPropLvl5(value) {
    this.resproplvl5 = value
  }

  editInvestAllo() {

    this._documentService.UpdateMaintain("Equity", this.reseqlvl1, this.reseqlvl2, this.reseqlvl3, this.reseqlvl4,
      this.reseqlvl5).subscribe(response1 => {
        this._documentService.UpdateMaintain("MoneyMarket", this.resmmlvl1, this.resmmlvl2, this.resmmlvl3, this.resmmlvl4,
          this.resmmlvl5).subscribe(response2 => {
            this._documentService.UpdateMaintain("FixedIncome", this.resfilvl1, this.resfilvl2, this.resfilvl3, this.resfilvl4,
              this.resfilvl5).subscribe(response3 => {
                this._documentService.UpdateMaintain("Commodity", this.rescommlvl1, this.rescommlvl2, this.rescommlvl3, this.rescommlvl4,
                  this.rescommlvl5).subscribe(response4 => {
                    this._documentService.UpdateMaintain("Property", this.resproplvl1, this.resproplvl2, this.resproplvl3, this.resproplvl4,
                      this.resproplvl5).subscribe(response4 => {
                        this.router.navigate(['/back-office/maintain'])
                      })
                  })
              })
          })
      })

  }

  Back() {
    this.router.navigate(['/back-office/maintain'])
  }

}
