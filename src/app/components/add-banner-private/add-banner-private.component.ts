import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { BannerPrivateService } from 'src/app/services/banner-private.service';
import { linkApi } from '../../constants/constants';

@Component({
  selector: 'app-add-banner-private',
  templateUrl: './add-banner-private.component.html',
  styleUrls: ['./add-banner-private.component.scss']
})
export class AddBannerPrivateComponent implements OnInit {

  isTh: boolean
  isEn: boolean
  announceType: string;
  typeannounce: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  announceStatus: string;
  statusannounce: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };

  // for add announce
  bannerTitle: string
  bannerUrl: string

  // path response from server

  announceBannerImage: any;
  BannerImagepath: any

  announceBannerImage_mobile: any;

  vdo_id: any

  constructor(private _bannerService: BannerPrivateService, private router: Router,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.isTh = true
    this.BannerImagepath = '';
  }

  setBannerTitle(title: string) {
    this.bannerTitle = title
  }

  setAnnounceBannerFiles(event) {
    var file_true = false;
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 1440) {
          alert("ภาพ Banner ต้องมีขนาด 1440 x 380 px")
        }
        else if (img.height != 380) {
          alert("ภาพ Banner ต้องมีขนาด 1440 x 380 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.announceBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }


  addAnnounce() {
    let userId = sessionStorage.getItem('userId')

    if (this.bannerTitle == undefined || this.bannerTitle == "") {
      alert("กรุณากรอก Banner Name !!");
      this.ngxService.stop();
    }
    else if (this.announceBannerImage == undefined || this.announceBannerImage == "") {
      alert("กรุณาเลือก Banner !!");
      this.ngxService.stop();
    }
    else {
      this.ngxService.start();
      this._bannerService.uploadFile_banner(this.announceBannerImage).subscribe(response => {
        this.BannerImagepath = response.toString();

        if (this.announceBannerImage != undefined && this.announceBannerImage != "") {

          this._bannerService.addBanner(this.bannerTitle, this.BannerImagepath, userId, "1")
            .subscribe(response => {
              console.log(response)
              this.router.navigate(['/back-office/banner-private']);
          })
          this.ngxService.stop();
        }
        else {
          //==================== Title Image ================================
          this._bannerService.addBanner(this.bannerTitle, this.BannerImagepath, userId, "1")
            .subscribe(response => {
              console.log(response)
              this.router.navigate(['/back-office/banner-private']);

            })
          this.ngxService.stop();

        }

      })
    }
  }

  Back() {
    this.router.navigate(['/back-office/banner-private']);
  }


}
