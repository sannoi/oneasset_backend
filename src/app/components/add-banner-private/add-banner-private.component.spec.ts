import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBannerPrivateComponent } from './add-banner-private.component';

describe('AddBannerPrivateComponent', () => {
  let component: AddBannerPrivateComponent;
  let fixture: ComponentFixture<AddBannerPrivateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBannerPrivateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBannerPrivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
