import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManagerListService } from 'src/app/services/manager-list.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';


@Component({
  selector: 'app-add-manager-list',
  templateUrl: './add-manager-list.component.html',
  styleUrls: ['./add-manager-list.component.scss']
})
export class AddManagerListComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  managerListType: string;
  typemanagerList: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  managerListStatus: string;
  statusmanagerList: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  isTh: boolean
  isEn: boolean

  // for add managerList
  managerListTitle: string
  managerListTitleEN: string
  managerListTtitleImage: any
  managerListBannerImage: any
  imageTitleName: any
  managerListContentImage: any
  imageContentName: any
  managerListFiles: any
  managerListFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  managerListBannerImage_mobile: any;
  BannerImagepath_mobile: any;

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _managerListService: ManagerListService, private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }

  ngOnInit() {
      // TODO: Public Date
      flatpickr.localize(Thai);
      this.public_date = new Date().toISOString().slice(0,10);
      this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.managerListType = 'text';
    this.managerListStatus = '1';
    this.BannerImagepath = '';

    this.isTh = true
    this.isEn = false
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setManagerListType(code) {
    // console.log("choice: ", code);
    this.managerListType = code;
  }

  setManagerListTitle(title: string) {
    this.managerListTitle = title
  }

  setManagerListTitleEN(title: string) {
    this.managerListTitleEN = title
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setManagerListStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.managerListStatus = '1'
    } else if (code === 'Unpublish ') {
      this.managerListStatus = '0'
    }
  }

  setManagerListFiles(event) {
    if (event.target.id === 'ManagerListTitleImage') {
      this.managerListTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    } else if (event.target.id === 'ManagerListContentImage') {
      this.managerListContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    } else if (event.target.id === 'ManagerListFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.managerListFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }

    }
  }

  setManagerListBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.managerListBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setManagerListBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.managerListBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }


  addManagerList() {
    let userId = sessionStorage.getItem('userId')
    if (this.managerListTitle == undefined || this.managerListTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();

      if (this.managerListType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._managerListService.uploadFile_title(this.managerListTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._managerListService.uploadFile_banner(this.managerListBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();


            this._managerListService.uploadFile_mobile(this.managerListBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();


              this._managerListService.addmanagerList(this.managerListTitle, this.managerListType, editorHtmlTag, this.titleImagepath, userId,
                this.managerListStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/manager-list']);
                })
            })
          })
        })



      }
      else if (this.managerListType === 'image') {
        //==================== Title Image ================================
        this._managerListService.uploadFile_title(this.managerListTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();


          //==================== File Download ================================
          this._managerListService.uploadFile(this.managerListContentImage).subscribe(response => {

            this.contentImagePath = response.toString();
            //==================== Banner Image ================================
            this._managerListService.uploadFile_banner(this.managerListBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();


              this._managerListService.uploadFile_mobile(this.managerListBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._managerListService.addmanagerList(this.managerListTitle, this.managerListType, this.contentImagePath, this.titleImagepath,
                  userId, this.managerListStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/manager-list']);

                  })
              })
            })
          })
        })







      } else if (this.managerListType === 'pdf') {

        if (this.managerListFileName == undefined || this.managerListFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {


          //==================== Title Image ================================
          this._managerListService.uploadFile_title(this.managerListTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();
            //==================== File Download ================================
            this._managerListService.uploadFile(this.managerListFileName).subscribe(response => {

              this.pdfFilePath = response.toString();
              //==================== Banner Image ================================
              this._managerListService.uploadFile_banner(this.managerListBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._managerListService.uploadFile_mobile(this.managerListBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();


                  this._managerListService.addmanagerList(this.managerListTitle, this.managerListType, this.pdfFilePath, this.titleImagepath,
                    userId, this.managerListStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/manager-list']);

                    })
                })
              })
            })
          })



        }


      }
      else if (this.managerListType === 'vdo') {
        //==================== Title Image ================================
        this._managerListService.uploadFile_title(this.managerListTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();


          //==================== Banner Image ================================
          this._managerListService.uploadFile_banner(this.managerListBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();


            this._managerListService.uploadFile_mobile(this.managerListBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._managerListService.addmanagerList(this.managerListTitle, this.managerListType, this.vdo_id,
                this.titleImagepath,
                userId, this.managerListStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/manager-list']);

                })
            })

          })
        })





      }

      this.ngxService.stop();
    }


  }

  Back() {
    this.router.navigate(['/back-office/manager-list']);
  }

}
