import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-inflation',
  templateUrl: './add-inflation.component.html',
  styleUrls: ['./add-inflation.component.scss']
})
export class AddInflationComponent implements OnInit {

  inflation: any

  constructor(private router: Router) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m10')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }
  }

  addInflation() {
    let saveInflation = this.inflation * 0.01
    console.log('saveInflation', saveInflation);
    this.router.navigate(['/back-office/maintain'])
  }

  Back() {
    this.router.navigate(['/back-office/maintain'])
  }


}
