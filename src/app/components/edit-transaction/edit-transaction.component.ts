import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TransactionService } from 'src/app/services/transaction.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-transaction',
  templateUrl: './edit-transaction.component.html',
  styleUrls: ['./edit-transaction.component.scss']
})
export class EditTransactionComponent implements OnInit {

  private sub: any;

  id: string;
  TransactionType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  Transaction: any;

  TransactionTtitleImage: any;
  TransactionBannerImage: any
  TransactionBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  TransactionFiles: any;
  FilePath: string;

  isTh: boolean
  isEn: boolean

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  typeTransaction: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusTransaction: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editTransactionForm: FormGroup;

  old_transactionType: string;
  old_transactionDetail: string;
  isPDF_old: any;

  vdo_id: any

    // TODO: Public Date
    publicDate: any;
    public_date: any;


  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _transactionService: TransactionService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
      // TODO: Public Date
      flatpickr.localize(Thai);

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.isTh = true
    this.isEn = false

    this._transactionService.gettransactionByID(this.id).subscribe(response => {

      this.Transaction = response

      this.TransactionType = response[0]['transaction_type']
      this.title = response[0]['title']
      this.title_image = response[0]['transaction_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['transaction_img_Thum']

      if (response[0]['transaction_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

// TODO: Public Date
if (response[0]['publicDate']) {
  this.public_date = response[0]['publicDate'];
  this.publicDate  = response[0]['publicDate'];
} else {
  this.public_date = new Date().toISOString().slice(0,10);
  this.publicDate  = new Date().toISOString().slice(0,10);
}

      if (response[0]['transaction_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_transactionType = response[0]['transaction_type']
      this.old_transactionDetail = response[0]['detail']

      if (response[0]['transaction_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }

      //=============== Binding Info ==================//
      this.editTransactionForm = this.fb.group({
        TransactionTypeControl: this.TransactionType,
        TransactionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date : this.public_date
      });
      //==============================================//
    })
  }


  // TODO: Public Date
  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setTransactionType(code) {
    this.TransactionType = code;
    if (this.TransactionType == this.old_transactionType) {
      //=============== Binding Info ==================//
      this.editTransactionForm = this.fb.group({
        TransactionTypeControl: this.TransactionType,
        TransactionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_transactionDetail,
        vdo_id: this.old_transactionDetail,
        public_date : this.public_date
      });
      this.detail = this.old_transactionDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editTransactionForm = this.fb.group({
        TransactionTypeControl: this.TransactionType,
        TransactionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date : this.public_date
      });
      this.detail = "";
    }
  }


  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }



  setTransactionTitle(title) { this.title = title; }
  setTransactionFiles(event) {
    if (event.target.id === 'TransactionTitleImage') {
      this.TransactionTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'TransactionFile') {
      this.TransactionFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setTransactionStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/transaction']);
  }

  deleteBanner() {
    this._transactionService.delBannertransaction(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/transaction/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._transactionService.delBannertransaction_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/transaction/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {


    this.title_image = "";
    this.titleImagepath = ""


  }

  setTransactionBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.TransactionBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setTransactionBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.TransactionBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editTransaction() {
    this.ngxService.start();

    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.TransactionType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._transactionService.uploadFile_title(this.TransactionTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== Banner Image ================================
          if (this.status === '1') {
            this._transactionService.uploadFile_mobile(this.TransactionBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._transactionService.uploadFile_banner(this.TransactionBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._transactionService.updatetransaction(this.title, this.TransactionType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/transaction']);
                  })

              })


            })

          } else if (this.status === '0') {
            this._transactionService.delBannertransaction(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._transactionService.updatetransaction(this.title, this.TransactionType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/transaction']);
                })
            })
          }



        })




      } else if (this.TransactionType === 'image') {

        //==================== Title Image ================================
        this._transactionService.uploadFile_title(this.TransactionTtitleImage).subscribe(response => {



          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }



          //==================== File Download ================================
          this._transactionService.uploadFile(this.TransactionFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._transactionService.uploadFile_mobile(this.TransactionBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._transactionService.uploadFile_banner(this.TransactionBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._transactionService.updatetransaction(this.title, this.TransactionType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                      this.router.navigate(['/back-office/transaction']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._transactionService.delBannertransaction(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._transactionService.updatetransaction(this.title, this.TransactionType, this.FilePath, this.titleImagepath,
                  userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                    this.router.navigate(['/back-office/transaction']);
                  })

              })
            }



          })




        })



      } else if (this.TransactionType === 'pdf') {

        //==================== Title Image ================================
        this._transactionService.uploadFile_title(this.TransactionTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== File Download ================================
          this._transactionService.uploadFile(this.TransactionFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._transactionService.uploadFile_mobile(this.TransactionBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._transactionService.uploadFile_banner(this.TransactionBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._transactionService.updatetransaction(this.title, this.TransactionType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                      this.router.navigate(['/back-office/transaction']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._transactionService.delBannertransaction(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._transactionService.updatetransaction(this.title, this.TransactionType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                    this.router.navigate(['/back-office/transaction']);
                  })
              })
            }



          })




        })


      }
      else if (this.TransactionType === 'vdo') {

        //==================== Title Image ================================
        this._transactionService.uploadFile_title(this.TransactionTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          if (this.status === '1') {
            this._transactionService.uploadFile_mobile(this.TransactionBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._transactionService.uploadFile_banner(this.TransactionBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._transactionService.updatetransaction(this.title, this.TransactionType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/transaction']);
                  })
              })

            })

          } else if (this.status === '0') {
            this._transactionService.delBannertransaction(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._transactionService.updatetransaction(this.title, this.TransactionType, this.vdo_id,
                this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/transaction']);
                })

            })
          }

        })






      }



    }

    this.ngxService.stop();


  }

}
