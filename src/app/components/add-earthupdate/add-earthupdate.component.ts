import { Component, OnInit } from '@angular/core';
import { EarthupdateService } from 'src/app/services/earthupdate.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-add-earthupdate',
  templateUrl: './add-earthupdate.component.html',
  styleUrls: ['./add-earthupdate.component.scss']
})
export class AddEarthupdateComponent implements OnInit {

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  isTh: boolean
  isEn: boolean
  earthupdateType: string;
  typeearthupdate: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  earthupdateStatus: string;
  statusearthupdate: any[] = [
    { code: 'Publish', chooes: 'Publish' },
    { code: 'Unpublish ', chooes: 'Unpublish ' },
  ];

  // for add earthupdate
  earthupdateTitle: string
  earthupdateTitleEN: string
  earthupdateTtitleImage: any
  earthupdateBannerImage: any
  imageTitleName: any
  earthupdateContentImage: any
  imageContentName: any
  earthupdateFiles: any
  earthupdateFileName: any

  // path response from server
  titleImagepath: any
  BannerImagepath: any
  contentImagePath: any
  pdfFilePath: any

  vdo_id: any

  earthupdateBannerImage_mobile: any
  BannerImagepath_mobile: any

  // TODO: Public Date
  publicDate: any;
  public_date: any;

  constructor(private _earthupdateService: EarthupdateService, private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }


  ngOnInit() {
    flatpickr.localize(Thai);
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = this.public_date;

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.isTh = true
    this.isEn = false
    this.earthupdateType = 'text';
    this.earthupdateStatus = '1';
    this.BannerImagepath = '';
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setEarthUpdateType(code) {
    // console.log("choice: ", code);
    this.earthupdateType = code;
  }

  setEarthUpdateTitle(title: string) {
    this.earthupdateTitle = title
  }

  setEarthUpdateTitleEN(title: string) {
    this.earthupdateTitleEN = title
  }

  setPublicDate(public_date: string) {
    this.publicDate = public_date;
  }

  setEarthUpdateStatus(code) {
    console.log(code);
    if (code === 'Publish') {
      this.earthupdateStatus = '1'
    } else if (code === 'Unpublish ') {
      this.earthupdateStatus = '0'
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  setEarthUpdateFiles(event) {
    if (event.target.id === 'EarthUpdateTitleImage') {
      this.earthupdateTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name

    } else if (event.target.id === 'EarthUpdateContentImage') {
      this.earthupdateContentImage = <File>event.target.files[0]
      this.contentImagePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name

    } else if (event.target.id === 'EarthUpdateFile') {


      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.earthupdateFileName = <File>event.target.files[0]
        this.pdfFilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }

    }
  }

  setEarthUpdateBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.earthupdateBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setEarthUpdateBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.earthupdateBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  addEarthUpdate() {
    let userId = sessionStorage.getItem('userId')

    if (this.earthupdateTitle == undefined || this.earthupdateTitle == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      this.ngxService.start();

      if (this.earthupdateType === 'text') {
        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML

        //==================== Title Image ================================
        this._earthupdateService.uploadFile_title(this.earthupdateTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._earthupdateService.uploadFile_banner(this.earthupdateBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            //==================== Banner Image ================================
            this._earthupdateService.uploadFile_mobile(this.earthupdateBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._earthupdateService.addEarthUpdate(this.earthupdateTitle, this.earthupdateType, editorHtmlTag, this.titleImagepath, userId,
                this.earthupdateStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/earthupdate']);
                })
            })
          })
        })






      }
      else if (this.earthupdateType === 'image') {
        //==================== Title Image ================================
        this._earthupdateService.uploadFile_title(this.earthupdateTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== File Download ================================
          this._earthupdateService.uploadFile(this.earthupdateContentImage).subscribe(response => {

            this.contentImagePath = response.toString();

            //==================== Banner Image ================================
            this._earthupdateService.uploadFile_banner(this.earthupdateBannerImage).subscribe(response => {
              this.BannerImagepath = response.toString();

              this._earthupdateService.uploadFile_mobile(this.earthupdateBannerImage_mobile).subscribe(response => {
                this.BannerImagepath_mobile = response.toString();

                this._earthupdateService.addEarthUpdate(this.earthupdateTitle, this.earthupdateType, this.contentImagePath, this.titleImagepath,
                  userId, this.earthupdateStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/earthupdate']);

                  })
              })

            })
          })

        })


      } else if (this.earthupdateType === 'pdf') {

        if (this.earthupdateFileName == undefined || this.earthupdateFileName == '') {
          alert("กรุณาเลือกไฟล์ PDF");
          this.ngxService.stop();
        }
        else {

          //==================== Title Image ================================
          this._earthupdateService.uploadFile_title(this.earthupdateTtitleImage).subscribe(response => {
            this.titleImagepath = response.toString();

            //==================== File Download ================================
            this._earthupdateService.uploadFile(this.earthupdateFileName).subscribe(response => {
              this.pdfFilePath = response.toString();

              //==================== Banner Image ================================
              this._earthupdateService.uploadFile_banner(this.earthupdateBannerImage).subscribe(response => {
                this.BannerImagepath = response.toString();

                this._earthupdateService.uploadFile_mobile(this.earthupdateBannerImage_mobile).subscribe(response => {
                  this.BannerImagepath_mobile = response.toString();

                  this._earthupdateService.addEarthUpdate(this.earthupdateTitle, this.earthupdateType, this.pdfFilePath,
                    this.titleImagepath,
                    userId, this.earthupdateStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/earthupdate']);

                    })
                })
              })
            })
          })


        }


      }
      else if (this.earthupdateType === 'vdo') {
        //==================== Title Image ================================
        this._earthupdateService.uploadFile_title(this.earthupdateTtitleImage).subscribe(response => {
          this.titleImagepath = response.toString();

          //==================== Banner Image ================================
          this._earthupdateService.uploadFile_banner(this.earthupdateBannerImage).subscribe(response => {
            this.BannerImagepath = response.toString();

            this._earthupdateService.uploadFile_mobile(this.earthupdateBannerImage_mobile).subscribe(response => {
              this.BannerImagepath_mobile = response.toString();

              this._earthupdateService.addEarthUpdate(this.earthupdateTitle, this.earthupdateType, this.vdo_id,
                this.titleImagepath,
                userId, this.earthupdateStatus, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/earthupdate']);

                })
            })
          })
        })


      }
      this.ngxService.stop();


    }


  }

  Back() {
    this.router.navigate(['/back-office/earthupdate']);
  }






}
