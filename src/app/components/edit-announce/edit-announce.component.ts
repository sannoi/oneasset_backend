import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AnnounceService } from 'src/app/services/announce.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-announce',
  templateUrl: './edit-announce.component.html',
  styleUrls: ['./edit-announce.component.scss']
})
export class EditAnnounceComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _announceService: AnnounceService, private ngxService: NgxUiLoaderService) { }

  private sub: any;


  isTh: boolean
  isEn: boolean
  id: string;
  AnnounceType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  Announce: any;

  AnnounceTtitleImage: any;
  AnnounceBannerImage: any
  AnnounceBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  AnnounceFiles: any;
  FilePath: string;


  typeAnnounce: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusAnnounce: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  editAnnounceForm: FormGroup;

  old_AnnounceType: string;
  old_AnnounceDetail: string;
  isPDF_old: any;

  vdo_id: any

    // TODO: Public Date
    publicDate: any;
    public_date: any;

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m1')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }


    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

      console.log(this.id)
    this._announceService.getAnnounceByID(this.id).subscribe(response => {

      this.Announce = response
      console.log(this.Announce);
      this.AnnounceType = response[0]['announce_type']

      this.title = response[0]['title'];
      this.title_image = response[0]['announce_img_Thum']
      this.titleImagepath = response[0]['announce_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']


      if (response[0]['announce_type'] != "text") {
        this.FilePath = response[0]['detail']
      }


     // TODO: Public Date
     if (response[0]['publicDate']) {
      this.public_date = response[0]['publicDate'];
      this.publicDate  = response[0]['publicDate'];
    } else {
      this.public_date = new Date().toISOString().slice(0,10);
      this.publicDate  = new Date().toISOString().slice(0,10);
    }

      if (response[0]['announce_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_AnnounceType = response[0]['announce_type']
      this.old_AnnounceDetail = response[0]['detail']

      if (response[0]['announce_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }


      //=============== Binding Info ==================//
      this.editAnnounceForm = this.fb.group({
        AnnounceTypeControl: this.AnnounceType,
        AnnounceTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }

      // TODO: Public Date
      setPublicDate(public_date: string) {
        this.publicDate = public_date;
      }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setAnnounceType(code) {
    this.AnnounceType = code;

    if (this.AnnounceType == this.old_AnnounceType) {
      //=============== Binding Info ==================//
      this.editAnnounceForm = this.fb.group({
        AnnounceTypeControl: this.AnnounceType,
        AnnounceTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_AnnounceDetail,
        vdo_id: this.old_AnnounceDetail
      });
      this.detail = this.old_AnnounceDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editAnnounceForm = this.fb.group({
        AnnounceTypeControl: this.AnnounceType,
        AnnounceTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
      });
      this.detail = "";
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  setAnnounceTitle(title) { this.title = title; }
  setAnnounceFiles(event) {
    if (event.target.id === 'AnnounceTitleImage') {
      this.AnnounceTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'AnnounceFile') {

      var a = '' + <File>event.target.files[0].name.split('.')[1]
      console.log(a)

      if (a == 'pdf' || a == 'PDF') {

        this.AnnounceFiles = <File>event.target.files[0]
        this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
      }
      else {
        alert('ต้องเป้นไฟล์ PDF เท่านั้น');
      }


    }
  }
  setAnnounceStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/announce']);
  }

  deleteBanner() {
    this._announceService.delBannerAnnounce(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/announce/edit'], { queryParams: { id: this.id } });
  }


  deleteBanner_mobile() {
    this._announceService.delBannerAnnounce_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/announce/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = "";


  }

  setAnnounceBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.AnnounceBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setAnnounceBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.AnnounceBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editAnnounce() {
    let userId = sessionStorage.getItem('userId')
    this.ngxService.start();

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      if (this.AnnounceType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._announceService.uploadFile_title(this.AnnounceTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== Banner Image ================================
          if (this.status === '1') {

            this._announceService.uploadFile_banner(this.AnnounceBannerImage).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath = response.toString();
              }

              this._announceService.uploadFile_mobile(this.AnnounceBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._announceService.updateAnnounce(this.title, this.AnnounceType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/announce']);
                  })

              })


            })

          } else if (this.status === '0') {
            this._announceService.delBannerAnnounce(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._announceService.updateAnnounce(this.title, this.AnnounceType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/announce']);
                })
            })
          }




        })




      } else if (this.AnnounceType === 'image') {

        //==================== Title Image ================================
        this._announceService.uploadFile_title(this.AnnounceTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._announceService.uploadFile(this.AnnounceFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._announceService.uploadFile_mobile(this.AnnounceBannerImage_mobile).subscribe(response => {
                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._announceService.uploadFile_banner(this.AnnounceBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._announceService.updateAnnounce(this.title, this.AnnounceType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/announce']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._announceService.delBannerAnnounce(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._announceService.updateAnnounce(this.title, this.AnnounceType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/announce']);
                  })
              })
            }


          })


        })





      } else if (this.AnnounceType === 'pdf') {

        //==================== Title Image ================================
        this._announceService.uploadFile_title(this.AnnounceTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._announceService.uploadFile(this.AnnounceFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._announceService.uploadFile_mobile(this.AnnounceBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._announceService.uploadFile_banner(this.AnnounceBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._announceService.updateAnnounce(this.title, this.AnnounceType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/announce']);
                    })
                })


              })

            } else if (this.status === '0') {
              this._announceService.delBannerAnnounce(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._announceService.updateAnnounce(this.title, this.AnnounceType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/announce']);
                  })
              })
            }


          })




        })


      }

      else if (this.AnnounceType === 'vdo') {

        //==================== Title Image ================================
        this._announceService.uploadFile_title(this.AnnounceTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._announceService.uploadFile_mobile(this.AnnounceBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._announceService.uploadFile_banner(this.AnnounceBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._announceService.updateAnnounce(this.title, this.AnnounceType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/announce']);
                  })
              })

            })

          } else if (this.status === '0') {
            this._announceService.delBannerAnnounce(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._announceService.updateAnnounce(this.title, this.AnnounceType, this.vdo_id, this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/announce']);
                })
            })
          }


        })



      }
    }


    this.ngxService.stop();

  }















}
