import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GovernenceService } from 'src/app/services/governence.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-good-governence',
  templateUrl: './edit-good-governence.component.html',
  styleUrls: ['./edit-good-governence.component.scss']
})
export class EditGoodGovernenceComponent implements OnInit {

  private sub: any;

  isTh: boolean
  isEn: boolean
  id: string;
  GovernenceType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  Governence: any;

  GovernenceTtitleImage: any;
  GovernenceBannerImage: any
  GovernenceBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  GovernenceFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typeGovernence: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusGovernence: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editGovernenceForm: FormGroup;

  old_governenceType: string;
  old_governenceDetail: string;
  isPDF_old: any;

  vdo_id: any

      // TODO: Public Date
      publicDate: any;
      public_date: any;

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _governenceService: GovernenceService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
  // TODO: Public Date
  flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.isTh = true
    this.isEn = false

    this._governenceService.getgovernenceByID(this.id).subscribe(response => {

      this.Governence = response

      console.log(response)

      this.GovernenceType = response[0]['governence_type']
      this.title = response[0]['title']
      this.title_image = response[0]['governence_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['governence_img_Thum']

      if (response[0]['governence_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

          // TODO: Public Date
          if (response[0]['publicDate']) {
            this.public_date = response[0]['publicDate'];
            this.publicDate  = response[0]['publicDate'];
          } else {
            this.public_date = new Date().toISOString().slice(0,10);
            this.publicDate  = new Date().toISOString().slice(0,10);
          }

      if (response[0]['governence_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_governenceType = response[0]['governence_type']
      this.old_governenceDetail = response[0]['detail']

      if (response[0]['governence_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }


      //=============== Binding Info ==================//
      this.editGovernenceForm = this.fb.group({
        GovernenceTypeControl: this.GovernenceType,
        GovernenceTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setGovernenceTitle(title) { this.title = title; }
  setGovernenceFiles(event) {
    if (event.target.id === 'GovernenceTitleImage') {
      this.GovernenceTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'GovernenceFile') {
      this.GovernenceFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setGovernenceStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/governence']);
  }

  deleteBanner() {
    this._governenceService.delBannergovernence(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/governence/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._governenceService.delBannergovernence_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/governence/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = "";

  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }


  setGovernenceBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.GovernenceBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setGovernenceType(code) {
    this.GovernenceType = code;
    if (this.GovernenceType == this.old_governenceType) {
      //=============== Binding Info ==================//
      this.editGovernenceForm = this.fb.group({

        GovernenceTypeControl: this.GovernenceType,
        GovernenceTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_governenceDetail,
        vdo_id: this.old_governenceDetail,
        public_date: this.public_date
      });
      this.detail = this.old_governenceDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editGovernenceForm = this.fb.group({
        GovernenceTypeControl: this.GovernenceType,
        GovernenceTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }

  setGovernenceBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.GovernenceBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editGovernence() {
    let userId = sessionStorage.getItem('userId')
    this.ngxService.start();


    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {


      if (this.GovernenceType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._governenceService.uploadFile_title(this.GovernenceTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== Banner Image ================================
          if (this.status === '1') {
            this._governenceService.uploadFile_mobile(this.GovernenceBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._governenceService.uploadFile_banner(this.GovernenceBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._governenceService.updategovernence(this.title, this.GovernenceType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/governence']);
                  })

              })

            })

          } else if (this.status === '0') {
            this._governenceService.delBannergovernence(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._governenceService.updategovernence(this.title, this.GovernenceType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/governence']);
                })

            })
          }



        })




      } else if (this.GovernenceType === 'image') {

        //==================== Title Image ================================
        this._governenceService.uploadFile_title(this.GovernenceTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._governenceService.uploadFile(this.GovernenceFiles).subscribe(response => {

            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._governenceService.uploadFile_mobile(this.GovernenceBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._governenceService.uploadFile_banner(this.GovernenceBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._governenceService.updategovernence(this.title, this.GovernenceType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/governence']);
                    })


                })

              })

            } else if (this.status === '0') {
              this._governenceService.delBannergovernence(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._governenceService.updategovernence(this.title, this.GovernenceType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/governence']);
                  })

              })
            }



          })



        })


      } else if (this.GovernenceType === 'pdf') {
        //==================== Title Image ================================
        this._governenceService.uploadFile_title(this.GovernenceTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._governenceService.uploadFile(this.GovernenceFiles).subscribe(response => {
            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._governenceService.uploadFile_mobile(this.GovernenceBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._governenceService.uploadFile_banner(this.GovernenceBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._governenceService.updategovernence(this.title, this.GovernenceType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/governence']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._governenceService.delBannergovernence(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._governenceService.updategovernence(this.title, this.GovernenceType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/governence']);
                  })

              })
            }



          })



        })




      }
      else if (this.GovernenceType === 'vdo') {

        //==================== Title Image ================================
        this._governenceService.uploadFile_title(this.GovernenceTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._governenceService.uploadFile_mobile(this.GovernenceBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._governenceService.uploadFile_banner(this.GovernenceBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._governenceService.updategovernence(this.title, this.GovernenceType, this.vdo_id,
                  this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/governence']);
                  })

              })

            })

          } else if (this.status === '0') {
            this._governenceService.delBannergovernence(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._governenceService.updategovernence(this.title, this.GovernenceType, this.vdo_id,
                this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/governence']);
                })
            })
          }



        })





      }



    }

    this.ngxService.stop();



  }
}
