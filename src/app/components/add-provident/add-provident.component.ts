import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-provident',
  templateUrl: './add-provident.component.html',
  styleUrls: ['./add-provident.component.scss']
})
export class AddProvidentComponent implements OnInit {

  provident: any

  constructor(private router: Router) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
  }

  addProvident() {
    let saveProvident = this.provident * 0.01
    console.log('saveProvident', saveProvident);
    this.router.navigate(['/back-office/maintain'])
  }

  Back() {
    this.router.navigate(['/back-office/maintain'])
  }

}
