import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FundDetailService } from 'src/app/services/fund-detail.service';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

@Component({
  selector: 'app-edit-fund-detail',
  templateUrl: './edit-fund-detail.component.html',
  styleUrls: ['./edit-fund-detail.component.scss']
})
export class EditFundDetailComponent implements OnInit {

  private sub: any;
  id: string;
  fund: any

  morningStarLevel: any[] = [
    { code: '1', level: '1 Star' },
    { code: '2', level: '2 Star' },
    { code: '3', level: '3 Star' },
    { code: '4', level: '4 Star' },
    { code: '5', level: '5 Star' },
  ];

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };

  data: any[] = [
    { val: 'n', name: 'Unpublish' },
    { val: 'w', name: 'Publish On Web' },
    { val: 'm', name: 'Publish On Mobile' },
    { val: 'b', name: 'Publish Both' }
  ];

  fundCode: any
  publishStatus: any
  fundAward: any
  investmentStyle: any
  investorType: any
  investmentPolicy: any
  accountPeriod: any
  haveMorningStarAward: any
  morningStarAward: any
  MorningStarLevel: any
  awardRecieveDate: any
  editFundDetailForm: FormGroup
  showMorningStarMenu: boolean

  fundDocItem: any
  isActiveDoc: any
  docItemShow: any
  selDate: any

  morninstarDate: any
  morninstarMonth: any
  morninstarYear: any

  Files: any
  FileName: any
  FilesPath: any

  myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    disableSince: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate()
    },
    showClearDateBtn: false,
    editableDateField: false,
    openSelectorOnInputClick: true
  };

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _fundDetailService: FundDetailService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m9')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route.queryParams.subscribe(params => {
      this.id = params['id'] || 0;
    });

    this._fundDetailService.getFundByID(this.id).subscribe(response => {

      console.log(response);
      this.fund = response

      this.fundCode = response[0].FundCode
      if (response[0].cms_publish_status == '') {
        this.publishStatus = "n"
      }
      else {
        this.publishStatus = response[0].cms_publish_status
      }
      this.fundAward = response[0].cms_reward
      this.investmentStyle = response[0].cms_investment_stype
      this.investorType = response[0].cms_investment_type
      this.investmentPolicy = response[0].cms_investment_policy
      this.accountPeriod = response[0].cms_fund_cycle
      this.morningStarAward = response[0].cms_morningstar_award

      if (response[0].cms_morningstar_award === 'y') {
        this.showMorningStarMenu = true
        this.haveMorningStarAward = 'y'
      }
      else {
        this.haveMorningStarAward = 'n'
        this.showMorningStarMenu = false
      }

      this.MorningStarLevel = response[0].cms_morningstar_level

      console.log(response[0].cms_morningstar_date)
      if (response[0].cms_morningstar_date != '') {
        this.awardRecieveDate = response[0].cms_morningstar_date

        this.selDate = {
          year: new Date(this.awardRecieveDate).getFullYear(),
          month: new Date(this.awardRecieveDate).getMonth() + 1,
          day: new Date(this.awardRecieveDate).getDate()
        };

      }

      //=============== Binding Info ==================//
      this.editFundDetailForm = this.fb.group({
        fundCodeControl: this.fundCode,
        publishStatusControl: this.publishStatus,
        fundAwardControl: this.fundAward,
        haveMorningStar: this.showMorningStarMenu,
        awardLevelControl: this.MorningStarLevel,
        awardRecieveDateControl: this.awardRecieveDate,
        accountPeriodControl: this.accountPeriod,
        investmentPolicyControl: this.investmentPolicy,
        investorTypeControl: this.investorType,
        investmentStyleControl: this.investmentStyle
      });
      //==============================================//

      this._fundDetailService.getFundDoc(this.id).subscribe(response => {
        this.fundDocItem = response

        this.fundDocItem.forEach(element => {
          if (element.Isactive === 'y') {
            element.Isactive = 'checked'
          } else if (element.Isactive === 'n') {
            element.Isactive = ''
          }
        });

      })






    })


  }

  setPublishStatus(event) {
    this.publishStatus = event
  }

  setFundAward(value) {
    this.fundAward = value
  }

  setInvestmentStyle(value) {
    this.investmentStyle = value
  }

  setinvestorType(value) {
    this.investorType = value
  }

  setinvestmentPolicy(value) {
    this.investmentPolicy = value
  }

  setaccountPeriod(value) {
    this.accountPeriod = value
  }

  setMorningStarLevel(event) {
    console.log(event);

    this.MorningStarLevel = event
  }

  morningStarCheck(event) {
    if (event.target.checked === true) {
      this.showMorningStarMenu = true
      this.haveMorningStarAward = 'y'
    } else {
      this.showMorningStarMenu = false
      this.haveMorningStarAward = 'n'
      this.MorningStarLevel = ''
      this.awardRecieveDate = ''
    }
  }

  setFilenameTH(value) {
    this.FileName = value
  }

  setNewsFiles(event) {
    if (event.target.id === 'newsFile') {
      this.Files = <File>event.target.files[0]
      this.FilesPath = linkApi + '/doc/' + <File>event.target.files[0].name
    }
  }

  UploadFile() {
    if (this.FileName != "") {
      this._fundDetailService.uploadFile(this.Files).subscribe(response => {

        if (response.toString() != "") {
          this.FilesPath = response.toString();
        }

        this._fundDetailService.InsertFile_DOC(this.FileName, this.FilesPath, this.id, "TH").subscribe(response => {
          //==============================================//

          this._fundDetailService.getFundDoc(this.id).subscribe(response => {
            this.fundDocItem = response

            this.fundDocItem.forEach(element => {
              if (element.Isactive === 'y') {
                element.Isactive = 'checked'
              } else if (element.Isactive === 'n') {
                element.Isactive = ''
              }
            });

          })
        })

      })
    }
    else {
      alert("กรุณากรอกชื่อไฟล์");
    }
  }


  updateDoc(event) {
    let updateDocId = event.target.id
    if (event.target.checked) {
      this.isActiveDoc = 'y'
    } else {
      this.isActiveDoc = 'n'
    }
    console.log('update docId: ', updateDocId);
    console.log('doc status update: ', this.isActiveDoc);
    this._fundDetailService.updateDoc(this.isActiveDoc, updateDocId).subscribe(response => {
      console.log('update doc response: ', response);

    })
  }

  onDateChanged(event: IMyDateModel) {
    this.selDate = event.date
    this.awardRecieveDate = event.formatted
  }

  editDetail() {
    event.preventDefault();

    let userId = sessionStorage.getItem('userId')

    let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
    let editorHtmlTag = e.item(0).innerHTML

    if (this.showMorningStarMenu === true) {

      console.log(editorHtmlTag);
      this._fundDetailService.updateFundDetail(this.id, this.publishStatus, this.fundAward,
        this.investmentStyle, this.investorType, editorHtmlTag, this.accountPeriod,
        this.haveMorningStarAward, this.MorningStarLevel, this.awardRecieveDate, userId).subscribe(response => {

          this.router.navigate(['/back-office/fund-detail']);

        })
    } else if (this.showMorningStarMenu === false) {

      console.log(editorHtmlTag);
      this._fundDetailService.updateFundDetail(this.id, this.publishStatus, this.fundAward,
        this.investmentStyle, this.investorType, editorHtmlTag, this.accountPeriod,
        '', '', '', userId).subscribe(response => {

          this.router.navigate(['/back-office/fund-detail']);

        })
    }
    //
  }

  Back() {
    event.preventDefault()
    this.router.navigate(['/back-office/fund-detail']);
  }
}
