import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EthicService } from 'src/app/services/ethic.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-ethic',
  templateUrl: './edit-ethic.component.html',
  styleUrls: ['./edit-ethic.component.scss']
})
export class EditEthicComponent implements OnInit {

  private sub: any;
  isTh: boolean
  isEn: boolean
  id: string;
  EthicType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  Ethic: any;

  EthicTtitleImage: any;
  EthicBannerImage: any
  EthicBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  EthicFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };



  typeEthic: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusEthic: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editEthicForm: FormGroup;


  old_ethicType: string;
  old_ethicDetail: string;
  isPDF_old: any;

  vdo_id: any

      // TODO: Public Date
      publicDate: any;
      public_date: any;
  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _ethicService: EthicService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
  // TODO: Public Date
  flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.isTh = true
    this.isEn = false

    this._ethicService.getethicByID(this.id).subscribe(response => {

      this.Ethic = response

      this.EthicType = response[0]['ethic_type']
      this.title = response[0]['title']
      this.title_image = response[0]['ethic_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['ethic_img_Thum']

      if (response[0]['ethic_type'] != "text") {
        this.FilePath = response[0]['detail']
      }


      if (response[0]['ethic_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_ethicType = response[0]['ethic_type']
      this.old_ethicDetail = response[0]['detail']

      if (response[0]['ethic_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }

  // TODO: Public Date
    if (response[0]['publicDate']) {
      this.public_date = response[0]['publicDate'];
      this.publicDate  = response[0]['publicDate'];
    } else {
      this.public_date = new Date().toISOString().slice(0,10);
      this.publicDate  = new Date().toISOString().slice(0,10);
    }
      //=============== Binding Info ==================//
      this.editEthicForm = this.fb.group({
        EthicTypeControl: this.EthicType,
        EthicTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setEthicType(code) {
    this.EthicType = code;
    console.log(this.EthicType)
    if (this.EthicType == this.old_ethicType) {
      //=============== Binding Info ==================//
      this.editEthicForm = this.fb.group({
        EthicTypeControl: this.EthicType,
        EthicTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_ethicDetail,
        public_date: this.public_date
      });
      this.detail = this.old_ethicDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editEthicForm = this.fb.group({
        EthicTypeControl: this.EthicType,
        EthicTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }


  setEthicTitle(title) { this.title = title; }
  setEthicFiles(event) {
    if (event.target.id === 'EthicTitleImage') {
      this.EthicTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'EthicFile') {
      this.EthicFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setEthicStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/ethic']);
  }

  deleteBanner() {
    this._ethicService.delBannerethic(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/ethic/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._ethicService.delBannerethic_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/ethic/edit'], { queryParams: { id: this.id } });
  }

  deletetitleImg() {

    this.title_image = "";
    this.titleImagepath = ""

  }

  setEthicBannerFiles(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.EthicBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }


      };
    }
  }

  setEthicBannerFiles_mobile(event) {

    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.EthicBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editEthic() {
    let userId = sessionStorage.getItem('userId')

    this.ngxService.start();
    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      if (this.EthicType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._ethicService.uploadFile_title(this.EthicTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._ethicService.uploadFile_mobile(this.EthicBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._ethicService.uploadFile_banner(this.EthicBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._ethicService.updateethic(this.title, this.EthicType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                    console.log(response)
                    this.router.navigate(['/back-office/ethic']);
                  })
              })

            })

          } else if (this.status === '0') {
            this._ethicService.delBannerethic(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._ethicService.updateethic(this.title, this.EthicType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/ethic']);
                })
            })
          }





        })



      } else if (this.EthicType === 'image') {

        //==================== Title Image ================================
        this._ethicService.uploadFile_title(this.EthicTtitleImage).subscribe(response => {


          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._ethicService.uploadFile(this.EthicFiles).subscribe(response => {

            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._ethicService.uploadFile_mobile(this.EthicBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._ethicService.uploadFile_banner(this.EthicBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._ethicService.updateethic(this.title, this.EthicType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/ethic']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._ethicService.delBannerethic(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._ethicService.updateethic(this.title, this.EthicType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/ethic']);
                  })
              })
            }

          })





        })




      } else if (this.EthicType === 'pdf') {

        //==================== Title Image ================================
        this._ethicService.uploadFile_title(this.EthicTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._ethicService.uploadFile(this.EthicFiles).subscribe(response => {

            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._ethicService.uploadFile_mobile(this.EthicBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._ethicService.uploadFile_banner(this.EthicBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._ethicService.updateethic(this.title, this.EthicType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/ethic']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._ethicService.delBannerethic(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._ethicService.updateethic(this.title, this.EthicType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/ethic']);
                  })
              })
            }



          })



        })




      } else if (this.EthicType === 'vdo') {

        //==================== Title Image ================================
        this._ethicService.uploadFile_title(this.EthicTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }


          //==================== Banner Image ================================
          if (this.status === '1') {
            this._ethicService.uploadFile_mobile(this.EthicBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._ethicService.uploadFile_banner(this.EthicBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._ethicService.updateethic(this.
                  title, this.EthicType, this.vdo_id, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/ethic']);
                  })
              })

            })

          } else if (this.status === '0') {
            this._ethicService.delBannerethic(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._ethicService.updateethic(this.title, this.EthicType, this.vdo_id, this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/ethic']);
                })
            })
          }



        })





      }

    }



    this.ngxService.stop();


  }
}
