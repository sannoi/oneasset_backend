import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { HoldingInvestmentUnitService } from 'src/app/services/holding-investment-unit.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-holding-investment-unit',
  templateUrl: './holding-investment-unit.component.html',
  styleUrls: ['./holding-investment-unit.component.scss']
})
export class HoldingInvestmentUnitComponent implements OnInit {

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];

  holdingInvestUnitDate: any
  holdingInvestUnits: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  constructor(private router: Router, private _holdingInvestUnitService: HoldingInvestmentUnitService,
    private pagerService: PagerService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m5')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._holdingInvestUnitService.getholdingInvestUnit(1, "").subscribe(response => {
      this.holdingInvestUnits = response
      this.allItems = response
      this.setPage(1);
      this.selectedLevel = 1
      this.ngxService.stop();
    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();
      this._holdingInvestUnitService.getholdingInvestUnit(this.selectedLevel, this.topicSearch).subscribe(response => {
        this.holdingInvestUnits = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  getNewsFromSerch() {
    this.ngxService.start();
    this._holdingInvestUnitService.getholdingInvestUnit(this.selectedLevel, this.topicSearch).subscribe(response => {
      this.holdingInvestUnits = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.holdingInvestUnitDate = this._holdingInvestUnitService.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/holding-investment-unit/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickAddHoldingInvestUnit() {
    event.preventDefault()
    this.router.navigate(['/back-office/holding-investment-unit/add'])

  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.holdingInvestUnitDate = this._holdingInvestUnitService.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.holdingInvestUnitDate = this._holdingInvestUnitService.date_convert(date)
    }
  }

}
