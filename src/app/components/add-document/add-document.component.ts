import { Component, OnInit } from '@angular/core';
import { DocumentService } from 'src/app/services/document.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';


@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.scss']
})
export class AddDocumentComponent implements OnInit {

  responsedoctypeList: any
  docTypeList: any
  doctype: any
  docName: any
  docFile: any
  docFilePaht: any

  docStatus: any[] = [
    { code: 'y', chooes: 'Publish' },
    { code: 'n', chooes: 'Unpublish ' },
  ];

  selectDocStatus = 'y'

  constructor(private _documentService: DocumentService, private router: Router,
    private ngxService: NgxUiLoaderService
    ) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m6')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this._documentService.getDocumentType('', 'y').subscribe(response => {
      this.responsedoctypeList = response
      this.docTypeList = response
      this.doctype = this.docTypeList[0].id
    })
  }

  setDocTypeName(doctypeName) {
    this.doctype = doctypeName
  }

  setDocName(docName) {
    this.docName = docName
  }

  setDocFile(docFile) {
    this.docFile = <File>docFile.target.files[0]
    this.docFilePaht = linkApi + '/FileUpload/' + <File>docFile.target.files[0].name
  }

  setDocumentStatus(docStatus) {

    this.selectDocStatus = docStatus

  }

  addDocument() {
    let userId = sessionStorage.getItem('userId')
    console.log('add doc');

    console.log(this.docName);
    console.log(this.doctype);
    console.log(this.docFile);
    console.log(this.docFilePaht);
    console.log(this.selectDocStatus);

    this._documentService.uploadFile(this.docFile).subscribe(response => {
      this.docFilePaht = response.toString();

      this._documentService.addDocument(this.docName, this.doctype, this.docFilePaht, this.selectDocStatus, userId)
        .subscribe(response => {

          this.router.navigate(['/back-office/document'])

        })
    })

  }

  back() {
    this.router.navigate(['/back-office/document'])
  }

}
