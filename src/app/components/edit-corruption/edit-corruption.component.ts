import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CorruptionService } from 'src/app/services/corruption.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';
@Component({
  selector: 'app-edit-corruption',
  templateUrl: './edit-corruption.component.html',
  styleUrls: ['./edit-corruption.component.scss']
})
export class EditCorruptionComponent implements OnInit {

  private sub: any;
  isTh: boolean
  isEn: boolean
  id: string;
  CorruptionType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  Corruption: any;

  CorruptionTtitleImage: any;
  CorruptionBannerImage: any
  CorruptionBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  CorruptionFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  typeCorruption: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusCorruption: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editCorruptionForm: FormGroup;

  old_corruptionType: string;
  old_corruptionDetail: string;
  isPDF_old: any;

  vdo_id: any
   // TODO: Public Date
   publicDate: any;
   public_date: any;

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _corruptionService: CorruptionService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
 // TODO: Public Date
 flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });
    this.isTh = true
    this.isEn = false

    this._corruptionService.getcorruptionByID(this.id).subscribe(response => {

      this.Corruption = response

      this.CorruptionType = response[0]['corruption_type']
      this.title = response[0]['title']
      this.title_image = response[0]['corruption_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['corruption_img_Thum']

      if (response[0]['corruption_type'] != "text") {
        this.FilePath = response[0]['detail']
      }


      if (response[0]['corruption_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_corruptionType = response[0]['corruption_type']
      this.old_corruptionDetail = response[0]['detail']

      if (response[0]['corruption_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }

       // TODO: Public Date
       if (response[0]['publicDate']) {
        this.public_date = response[0]['publicDate'];
        this.publicDate  = response[0]['publicDate'];
      } else {
        this.public_date = new Date().toISOString().slice(0,10);
        this.publicDate  = new Date().toISOString().slice(0,10);
      }

      //=============== Binding Info ==================//
      this.editCorruptionForm = this.fb.group({
        CorruptionTypeControl: this.CorruptionType,
        CorruptionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date: this.public_date
      });
      //==============================================//
    })
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setCorruptionType(code) {
    this.CorruptionType = code;
    if (this.CorruptionType == this.old_corruptionType) {
      //=============== Binding Info ==================//
      this.editCorruptionForm = this.fb.group({
        CorruptionTypeControl: this.CorruptionType,
        CorruptionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_corruptionDetail,
        vdo_id: this.old_corruptionDetail,
        public_date: this.public_date
      });
      this.detail = this.old_corruptionDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editCorruptionForm = this.fb.group({
        CorruptionTypeControl: this.CorruptionType,
        CorruptionTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        vdo_id: "",
        public_date: this.public_date
      });
      this.detail = "";
    }
  }
  setCorruptionTitle(title) { this.title = title; }
  setCorruptionFiles(event) {
    if (event.target.id === 'CorruptionTitleImage') {
      this.CorruptionTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'CorruptionFile') {
      this.CorruptionFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setCorruptionStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/corruption']);
  }

  deleteBanner() {
    this._corruptionService.delBannercorruption(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/corruption/edit'], { queryParams: { id: this.id } });
  }

    // TODO: Public Date
    setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  deleteBanner_mobile() {
    this._corruptionService.delBannercorruption_mobile(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/corruption/edit'], { queryParams: { id: this.id } });
  }
  deletetitleImg() {
    // this._corruptionService.delTitlecorruption(this.id).subscribe(response => {

    //   this.router.navigate(['/back-office/corruption/edit'], { queryParams: { id: this.id } });
    // })

    this.title_image = "";
    this.titleImagepath = ""

  }

  setCorruptionBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {

          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.CorruptionBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;

        }


      };
    }
  }

  setCorruptionBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.CorruptionBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editCorruption() {
    let userId = sessionStorage.getItem('userId')
    this.ngxService.start();
    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {
      if (this.CorruptionType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._corruptionService.uploadFile_title(this.CorruptionTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._corruptionService.uploadFile_mobile(this.CorruptionBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._corruptionService.uploadFile_banner(this.CorruptionBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._corruptionService.updatecorruption(this.title, this.CorruptionType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/corruption']);
                  })
              })

            })

          } else if (this.status === '0') {
            this._corruptionService.delBannercorruption(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._corruptionService.updatecorruption(this.title, this.CorruptionType, editorHtmlTag, this.titleImagepath,
                userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/corruption']);
                })
            })
          }




        })




      } else if (this.CorruptionType === 'image') {

        //==================== Title Image ================================
        this._corruptionService.uploadFile_title(this.CorruptionTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._corruptionService.uploadFile(this.CorruptionFiles).subscribe(response => {

            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._corruptionService.uploadFile_mobile(this.CorruptionBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._corruptionService.uploadFile_banner(this.CorruptionBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._corruptionService.updatecorruption(this.title, this.CorruptionType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/corruption']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._corruptionService.delBannercorruption(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._corruptionService.updatecorruption(this.title, this.CorruptionType, this.FilePath, this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/corruption']);
                  })
              })
            }

          })




        })



      } else if (this.CorruptionType === 'pdf') {

        //==================== Title Image ================================
        this._corruptionService.uploadFile_title(this.CorruptionTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== File Download ================================
          this._corruptionService.uploadFile(this.CorruptionFiles).subscribe(response => {

            if (response.toString() != "") {
              this.FilePath = response.toString();
            }

            //==================== Banner Image ================================
            if (this.status === '1') {
              this._corruptionService.uploadFile_mobile(this.CorruptionBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._corruptionService.uploadFile_banner(this.CorruptionBannerImage).subscribe(response => {


                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._corruptionService.updatecorruption(this.title, this.CorruptionType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/corruption']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._corruptionService.delBannercorruption(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._corruptionService.updatecorruption(this.title, this.CorruptionType, this.FilePath, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/corruption']);
                  })

              })
            }


          })



        })




      } else if (this.CorruptionType === 'vdo') {

        //==================== Title Image ================================
        this._corruptionService.uploadFile_title(this.CorruptionTtitleImage).subscribe(response => {

          if (response.toString() == "") {
            this.titleImagepath = this.title_image;
          }
          else {
            this.titleImagepath = response.toString();
          }

          //==================== Banner Image ================================
          if (this.status === '1') {
            this._corruptionService.uploadFile_mobile(this.CorruptionBannerImage_mobile).subscribe(response => {

              if (response.toString() != "") {
                this.BannerImagepath_mobile = response.toString();
              }

              this._corruptionService.uploadFile_banner(this.CorruptionBannerImage).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath = response.toString();
                }

                this._corruptionService.updatecorruption(this.title, this.CorruptionType, this.vdo_id,
                  this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/corruption']);
                  })
              })


            })

          } else if (this.status === '0') {
            this._corruptionService.delBannercorruption(this.id).subscribe(response => {

              this.banner_img = "";
              this.BannerImagepath = "";

              this.banner_img_mobile = "";
              this.BannerImagepath_mobile = "";

              this._corruptionService.updatecorruption(this.title, this.CorruptionType, this.vdo_id,
                this.titleImagepath, userId,
                this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile , this.publicDate).subscribe(response => {
                  this.router.navigate(['/back-office/corruption']);
                })

            })
          }


        })






      }
    }

    this.ngxService.stop();

  }

}
