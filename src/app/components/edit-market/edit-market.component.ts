import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MarketService } from 'src/app/services/market.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { linkApi } from '../../constants/constants';

// TODO: Public Date
import flatpickr from 'flatpickr';
import { Thai } from 'flatpickr/dist/l10n/th';

@Component({
  selector: 'app-edit-market',
  templateUrl: './edit-market.component.html',
  styleUrls: ['./edit-market.component.scss']
})
export class EditMarketComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _marketService: MarketService, private ngxService: NgxUiLoaderService) { }

  private sub: any;

  isTh: boolean
  isEn: boolean
  id: string;
  marketType: string;
  title: string;
  title_image: string;
  banner_img: string;
  banner_img_mobile: string;
  status: string;
  detail: string;
  market: any;

  marketTtitleImage: any;
  marketBannerImage: any
  marketBannerImage_mobile: any
  titleImagepath: string;

  BannerImagepath: any
  BannerImagepath_mobile: any

  marketFiles: any;
  FilePath: string;

  config = {
    toolbar: [
      // [groupName, [list of button]]
      ['misc', ['codeview', 'undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']]
    ]
  };


  typemarket: any[] = [
    { code: 'text', chooes: 'Text' },
    { code: 'image', chooes: 'Image' },
    { code: 'pdf', chooes: 'PDF' },
    { code: 'vdo', chooes: 'VDO' },
  ];
  statusmarket: any[] = [
    { code: '1', chooes: 'Publish' },
    { code: '0', chooes: 'Unpublish ' },
  ];

  editmarketForm: FormGroup;

  old_marketType: string;
  old_marketDetail: string;
  isPDF_old: any;

  vdo_id: any

      // TODO: Public Date
      publicDate: any;
      public_date: any;

  ngOnInit() {
    // TODO: Public Date
    flatpickr.localize(Thai);
    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }

    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'] || 0;
      });

    this.isTh = true
    this.isEn = false

    this._marketService.getmarketByID(this.id).subscribe(response => {

      console.log(response);

      this.market = response
      console.log(this.market);
      this.marketType = response[0]['market_type']
      this.title = response[0]['title']
      this.title_image = response[0]['market_img_Thum']

      this.banner_img = response[0]['banner_img']
      this.BannerImagepath = response[0]['banner_img']

      this.banner_img_mobile = response[0]['banner_img_mobile']
      this.BannerImagepath_mobile = response[0]['banner_img_mobile']

      this.status = response[0]['status']
      this.detail = response[0]['detail']

      this.titleImagepath = response[0]['market_img_Thum']

      if (response[0]['market_type'] != "text") {
        this.FilePath = response[0]['detail']
      }

  // TODO: Public Date
  if (response[0]['publicDate']) {
    this.public_date = response[0]['publicDate'];
    this.publicDate  = response[0]['publicDate'];
  } else {
    this.public_date = new Date().toISOString().slice(0,10);
    this.publicDate  = new Date().toISOString().slice(0,10);
  }

      if (response[0]['market_type'] == "vdo") {
        this.vdo_id = response[0]['detail']
      }
      this.old_marketType = response[0]['market_type']
      this.old_marketDetail = response[0]['detail']

      if (response[0]['market_type'] == "pdf") {
        this.isPDF_old = true
      }
      else {
        this.isPDF_old = false
      }


      //=============== Binding Info ==================//
      this.editmarketForm = this.fb.group({
        marketTypeControl: this.marketType,
        marketTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.detail,
        vdo_id: this.detail,
        public_date:  this.public_date

      });
      //==============================================//
    })
  }

  setEn() {
    this.isEn = true;
    this.isTh = false;
  }

  setTh() {
    this.isTh = true;
    this.isEn = false
  }

  setVDOID(vdo_id: string) {
    this.vdo_id = vdo_id.replace("https://www.youtube.com/watch?v=", "");
  }

  setmarketType(code) {
    this.marketType = code;
    if (this.marketType == this.old_marketType) {
      //=============== Binding Info ==================//
      this.editmarketForm = this.fb.group({
        marketTypeControl: this.marketType,
        marketTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: this.old_marketDetail,
        public_date:  this.public_date
      });
      this.detail = this.old_marketDetail;
    }
    else {
      //=============== Binding Info ==================//
      this.editmarketForm = this.fb.group({
        marketTypeControl: this.marketType,
        marketTitleControl: this.title,
        statusControl: this.status,
        textEditorControl: "",
        public_date:  this.public_date
      });
      this.detail = "";
    }
  }
  setmarketTitle(title) { this.title = title; }
  setmarketFiles(event) {
    if (event.target.id === 'marketTitleImage') {
      this.marketTtitleImage = <File>event.target.files[0]
      this.titleImagepath = linkApi + '/img_title/' + <File>event.target.files[0].name
    }
    else if (event.target.id === 'marketFile') {
      this.marketFiles = <File>event.target.files[0]
      this.FilePath = linkApi + '/FileUpload/' + <File>event.target.files[0].name
    }
  }
  setmarketStatus(code) { this.status = code }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  Back() {
    this.router.navigate(['/back-office/market']);
  }

  deleteBanner() {

    this._marketService.delBannermarket(this.id).subscribe(response => {
      this.banner_img = "";
      this.BannerImagepath = "";
    })
    this.router.navigate(['/back-office/market/edit'], { queryParams: { id: this.id } });
  }

  deleteBanner_mobile() {
    this._marketService.delBannermarket(this.id).subscribe(response => {
      this.banner_img_mobile = "";
      this.BannerImagepath_mobile = "";
    })
    this.router.navigate(['/back-office/market/edit'], { queryParams: { id: this.id } });
  }

     // TODO: Public Date
     setPublicDate(public_date: string) {
      this.publicDate = public_date;
    }

  deletetitleimg() {

    this.title_image = "";
    this.titleImagepath = ""

  }

  setmarketBannerFiles(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {

        if (img.width != 2880) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else if (img.height != 1102) {
          alert("ภาพ Banner ต้องมีขนาด 2880 x 1102 px")
        }
        else {
          window.URL.revokeObjectURL(img.src);
          file_true = true;
          self.marketBannerImage = <File>event.target.files[0]
          self.BannerImagepath = linkApi + '/img_banner/' + <File>event.target.files[0].name;
        }

      };
    }
  }

  setmarketBannerFiles_mobile(event) {
    var file_true = false;

    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      const img = new Image();
      let self = this;
      img.src = window.URL.createObjectURL(file);
      img.onload = function () {
        window.URL.revokeObjectURL(img.src);
        file_true = true;
        self.marketBannerImage_mobile = <File>event.target.files[0]
        self.BannerImagepath_mobile = linkApi + '/img_mobile/' + <File>event.target.files[0].name;
      };
    }
  }

  editmarket() {






    let userId = sessionStorage.getItem('userId')

    if (this.title == undefined || this.title == "") {
      alert("กรุณากรอก Title !!");
      this.ngxService.stop();

    }
    else {

      if (this.marketType === 'text') {

        let e = (document.getElementsByClassName("note-editable")) as HTMLCollectionOf<HTMLDivElement>;
        let editorHtmlTag = e.item(0).innerHTML
        //==================== Title Image ================================
        this._marketService.uploadFile_title(this.marketTtitleImage).subscribe(response => {


          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== Banner Image ================================
            if (this.status === '1') {
              this._marketService.uploadFile_mobile(this.marketBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._marketService.uploadFile_banner(this.marketBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._marketService.updatemarket(this.title, this.marketType, editorHtmlTag, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/market']);
                    })
                })

              })

            } else if (this.status === '0') {
              this._marketService.delBannermarket(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._marketService.updatemarket(this.title, this.marketType, editorHtmlTag, this.titleImagepath,
                  userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/market']);
                  })
              })
            }


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }





        })




      } else if (this.marketType === 'image') {

        //==================== Title Image ================================
        this._marketService.uploadFile_title(this.marketTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }



            //==================== File Download ================================
            this._marketService.uploadFile(this.marketFiles).subscribe(response => {

              if (response.toString() != "") {
                this.FilePath = response.toString();
              }
              //==================== Banner Image ================================
              if (this.status === '1') {
                this._marketService.uploadFile_mobile(this.marketBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._marketService.uploadFile_banner(this.marketBannerImage).subscribe(response => {

                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._marketService.updatemarket(this.title, this.marketType, this.FilePath, this.titleImagepath, userId,
                      this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                        this.router.navigate(['/back-office/market']);
                      })

                  })

                })

              } else if (this.status === '0') {
                this._marketService.delBannermarket(this.id).subscribe(response => {

                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";

                  this._marketService.updatemarket(this.title, this.marketType, this.FilePath, this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/market']);
                    })
                })
              }



            })

          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }



        })




      } else if (this.marketType === 'pdf') {

        //==================== Title Image ================================
        this._marketService.uploadFile_title(this.marketTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== File Download ================================
            this._marketService.uploadFile(this.marketFiles).subscribe(response => {
              if (response.toString() != "") {
                this.FilePath = response.toString();
              }

              //==================== Banner Image ================================
              if (this.status === '1') {
                this._marketService.uploadFile_mobile(this.marketBannerImage_mobile).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath_mobile = response.toString();
                  }

                  this._marketService.uploadFile_banner(this.marketBannerImage).subscribe(response => {

                    if (response.toString() != "") {
                      this.BannerImagepath = response.toString();
                    }

                    this._marketService.updatemarket(this.title, this.marketType, this.FilePath, this.titleImagepath,
                      userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                        this.router.navigate(['/back-office/market']);
                      })
                  })

                })

              } else if (this.status === '0') {
                this._marketService.delBannermarket(this.id).subscribe(response => {

                  this.banner_img = "";
                  this.BannerImagepath = "";

                  this.banner_img_mobile = "";
                  this.BannerImagepath_mobile = "";

                  this._marketService.updatemarket(this.title, this.marketType, this.FilePath, this.titleImagepath,
                    userId, this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {

                      this.router.navigate(['/back-office/market']);
                    })

                })
              }


            })


          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }



        })





      }
      else if (this.marketType === 'vdo') {

        //==================== Title Image ================================
        this._marketService.uploadFile_title(this.marketTtitleImage).subscribe(response => {

          if (response.toString() != "" || this.title_image != "") {
            if (response.toString() == "") {
              this.titleImagepath = this.title_image;
            }
            else {
              this.titleImagepath = response.toString();
            }


            //==================== Banner Image ================================
            if (this.status === '1') {
              this._marketService.uploadFile_mobile(this.marketBannerImage_mobile).subscribe(response => {

                if (response.toString() != "") {
                  this.BannerImagepath_mobile = response.toString();
                }

                this._marketService.uploadFile_banner(this.marketBannerImage).subscribe(response => {

                  if (response.toString() != "") {
                    this.BannerImagepath = response.toString();
                  }

                  this._marketService.updatemarket(this.title, this.marketType, this.vdo_id,
                    this.titleImagepath, userId,
                    this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                      this.router.navigate(['/back-office/market']);
                    })

                })

              })

            } else if (this.status === '0') {
              this._marketService.delBannermarket(this.id).subscribe(response => {

                this.banner_img = "";
                this.BannerImagepath = "";

                this.banner_img_mobile = "";
                this.BannerImagepath_mobile = "";

                this._marketService.updatemarket(this.title, this.marketType, this.vdo_id,
                  this.titleImagepath, userId,
                  this.status, this.id, this.BannerImagepath, this.BannerImagepath_mobile, this.publicDate).subscribe(response => {
                    this.router.navigate(['/back-office/market']);
                  })

              })
            }





          }
          else {
            alert("กรุณาเลือก Title Image !!");
            this.ngxService.stop();

          }

        })






      }

    }

    this.ngxService.stop();




  }






}
