import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserLoginService } from '../../services/user-login.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  oldPassword: string
  newPassword: string
  userId: string
  confirmNewPassword: string
  showResetWarning: boolean = false
  warnignMessage: string = ''

  constructor(private router: Router, private userService: UserLoginService) { }

  ngOnInit() {
    let userId = sessionStorage.getItem('userId')
    let token  = localStorage.getItem('token');
    if (userId == null || token == null) {
      this.router.navigate(['/back-office/login'])
    }

    this.userId = sessionStorage.getItem('userId')

    this.userService.checkTokenUserById(this.userId).subscribe(
      res => console.log(res),
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(['/back-office/login']);
          }
        }
      }
    );
  }

  clickReset() {
    event.preventDefault()
    console.log('old password: ', this.oldPassword,
      ' new password: ', this.newPassword,
      ' confirm new password: ', this.confirmNewPassword,
      ' userid : ', this.userId);
    if (this.newPassword != this.confirmNewPassword) {
      this.showResetWarning = true
      console.log("Confirm password dose not correct");
      this.warnignMessage = "Confirm password dose not correct"
    } else {
      this.showResetWarning = false
      this.userService.chkExisting_password(this.userId, this.oldPassword).subscribe(response => {

        console.log(response)

        if (response['returncode'] == "1000") {
          this.userService.changePassword(this.userId, this.confirmNewPassword, this.oldPassword).subscribe(response => {

            console.log(response)

            sessionStorage.removeItem('userId')
            sessionStorage.removeItem('loginUsername')

            this.router.navigate(['/back-office/login'])

          })
        }
        else {
          this.showResetWarning = true
          this.warnignMessage = "Password เก่าไม่ถูกต้อง"

        }
      })

    }

  }
}
