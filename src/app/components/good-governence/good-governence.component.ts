import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagerService } from 'src/app/_services';
import { GovernenceService } from 'src/app/services/governence.service'
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-good-governence',
  templateUrl: './good-governence.component.html',
  styleUrls: ['./good-governence.component.scss']
})
export class GoodGovernenceComponent implements OnInit {

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];

  governenceDate: any
  governences: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  constructor(private router: Router, private ngxService: NgxUiLoaderService,
    private _governenceService: GovernenceService, private pagerService: PagerService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m4')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._governenceService.getgovernence(1, "").subscribe(response => {
      this.governences = response
      this.allItems = response
      this.setPage(1);
      this.selectedLevel = 1
      this.ngxService.stop();
    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }

  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();
      this._governenceService.getgovernence(this.selectedLevel, this.topicSearch).subscribe(response => {
        this.governences = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  getNewsFromSerch() {
    this.ngxService.start();
    this._governenceService.getgovernence(this.selectedLevel, this.topicSearch).subscribe(response => {
      this.governences = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.governenceDate = this._governenceService.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/governence/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickAddGovernence() {
    event.preventDefault()
    this.router.navigate(['/back-office/governence/add'])

  }

  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.governenceDate = this._governenceService.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.governenceDate = this._governenceService.date_convert(date)
    }
  }

}
