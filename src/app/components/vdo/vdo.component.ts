import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VdoService } from 'src/app/services/vdo.service';
import { PagerService } from 'src/app/_services';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-vdo',
  templateUrl: './vdo.component.html',
  styleUrls: ['./vdo.component.scss']
})
export class VdoComponent implements OnInit {

  constructor(private router: Router, private _Service: VdoService,
    private pagerService: PagerService, private ngxService: NgxUiLoaderService) { }

  data: any[] = [
    { val: '1', name: 'Publish ' },
    { val: '0', name: 'Unpublish  ' },
  ];

  vdoDate: any
  vdos: any;
  selectedLevel: any;
  topicSearch: any;
  //============================== Paging ======================
  private allItems: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  //============================================================

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    let ress = sessionStorage.getItem('m3')
    if (ress == "false") {
      this.router.navigate(['/back-office'])
    }

    this.ngxService.start();
    this._Service.getvdo(1, "").subscribe(response => {
      this.vdos = response
      this.allItems = response
      this.setPage(1);
      this.selectedLevel = 1
      this.ngxService.stop();
    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setStatus(val) {
    // console.log("choice: ", code);
    this.selectedLevel = val;
  }

  setSearch(searchValue) {
    this.topicSearch = searchValue
  }
  keyDownFunction(event, search: string) {
    this.topicSearch = search
    if (event.keyCode == 13) {
      this.ngxService.start();

      this._Service.getvdo(this.selectedLevel, this.topicSearch).subscribe(response => {
        this.vdos = response
        this.allItems = response
        this.setPage(1);
        this.ngxService.stop();
      })
    }
  }

  getNewsFromSerch() {
    this.ngxService.start();
    this._Service.getvdo(this.selectedLevel, this.topicSearch).subscribe(response => {
      this.vdos = response
      this.allItems = response
      this.setPage(1);
      this.ngxService.stop();
    })
  }

  changeFormatDate(date) {
    if (date === '') {
      return null
    } else {
      return this.vdoDate = this._Service.changeFormatDate(date)
    }
  }

  clickEdit(xxx) {
    this.router.navigate(['/back-office/vdo/edit'], { queryParams: { id: xxx } });
    // this.router.navigate(['/back-office/news/edit'])
  }

  clickAddvdo() {
    event.preventDefault()
    this.router.navigate(['/back-office/vdo/add'])

  }



  date_thai(date) {
    if (date === '') {
      return null
    } else {
      return this.vdoDate = this._Service.date_thai(date)
    }
  }

  date_convert(date) {
    if (date === '') {
      return null
    } else {
      return this.vdoDate = this._Service.date_convert(date)
    }
  }








}
