import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DocumentService } from 'src/app/services/document.service';

@Component({
  selector: 'app-edit-expected-return',
  templateUrl: './edit-expected-return.component.html',
  styleUrls: ['./edit-expected-return.component.scss']
})
export class EditExpectedReturnComponent implements OnInit {

  editExpectedReturnForm: FormGroup
  //response Expected Return
  resExtlvl1: any
  resExtlvl2: any
  resExtlvl3: any
  resExtlvl4: any
  resExtlvl5: any

  // edit expected return
  extlvl1: any
  extlvl2: any
  extlvl3: any
  extlvl4: any
  extlvl5: any

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private _documentService: DocumentService) { }

  ngOnInit() {

    let userId = sessionStorage.getItem('userId')
    if (userId == null) {
      this.router.navigate(['/back-office/login'])
    }
    
    this._documentService.getMaintain("Expected").subscribe(response => {
      console.log(response)

      this.resExtlvl1 = response["r1"];
      this.resExtlvl2 = response["r2"];
      this.resExtlvl3 = response["r3"];
      this.resExtlvl4 = response["r4"];
      this.resExtlvl5 = response["r5"];

      this.editExpectedReturnForm = this.fb.group({
        lvl1Control: this.resExtlvl1,
        lvl2Control: this.resExtlvl2,
        lvl3Control: this.resExtlvl3,
        lvl4Control: this.resExtlvl4,
        lvl5Control: this.resExtlvl5,
      })


    })


  }

  setExpectedReturnLvl1(value) {
    this.resExtlvl1 = value
  }
  setExpectedReturnLvl2(value) {
    this.resExtlvl2 = value
  }
  setExpectedReturnLvl3(value) {
    this.resExtlvl3 = value
  }
  setExpectedReturnLvl4(value) {
    this.resExtlvl4 = value
  }
  setExpectedReturnLvl5(value) {
    this.resExtlvl5 = value
  }

  editExpectedReturn() {

    this._documentService.UpdateMaintain("Expected", this.resExtlvl1, this.resExtlvl2, this.resExtlvl3,
      this.resExtlvl4, this.resExtlvl5).subscribe(response => {
        this.router.navigate(['back-office/maintain'])

      })

  }

  Back() {
    this.router.navigate(['/back-office/maintain'])
  }

}
